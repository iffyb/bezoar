& ./clean
if ($LastExitCode -ne 0) {
    throw $LastExitCode
}

& ./presubmit
if ($LastExitCode -ne 0) {
    throw $LastExitCode
}

& ./wheel
if ($LastExitCode -ne 0) {
    throw $LastExitCode
}

scp -r out/release/* iffy@iffy.pairserver.com:~/public_html/bezoar/
if ($LastExitCode -ne 0) {
    throw $LastExitCode
}
