"""
MarkDown Document Object Model

MDDOM is a simplified Object Model to represent documents such that they can be
rendered out to different targets (HTML, MarkDown, ANSI Terminal). They are
designed to be comfortable and readable to declare inline in code.

A document looks something like this:

Fragment:
  Block:
    Span:
      Image: url
      Text: content
      Span: italics
        Text: content
      Text: content
  Block:
    ...
  Block:
    ...

Fragments are sequences of Blocks.

Blocks are sequences of Spans or other Blocks.

Spans are sequences of Contents, or other Spans.

Content is the only node type that contains actual content.

Processor is an abstract base class that has hooks to process an MDDOM tree like
a SAX parser.
"""


from __future__ import annotations

from typing import Any, Optional, Sequence, Union

import bezoar.base.ansi as ansi
from bezoar.base.text import html_escape, words

_INDENT = "  "


class Node(object):
    """Base class for all MDDOM node types."""

    def __init__(self) -> None:
        super().__init__()

    def _prefix(self) -> str:
        return f"[{self.__class__.__name__}"

    def _suffix(self) -> str:
        return f"]"

    def to_pretty_string(self, level: int = 0) -> str:
        return _INDENT * level + self._prefix() + self._suffix() + "\n"

    def get_text(self) -> str:
        return ""

    def __repr__(self) -> str:
        return ""

    def __str__(self) -> str:
        return self.__repr__()


class Image(Node):
    """An image to be flowed at the specified location. An image can be made
    into a block by just placing it inside a Block."""

    def __init__(self, url: str) -> None:
        super().__init__()
        self.url = url

    def __repr__(self) -> str:
        return "<img>"

    def get_text(self) -> str:
        return "(image)"


class Text(Node):
    """Text to be flowed at the specified location."""

    def __init__(self, text: str) -> None:
        super().__init__()
        self.text = text

    def to_pretty_string(self, level: int = 0) -> str:
        return _INDENT * level + self._prefix() + " '" + self.text + "'" + self._suffix() + "\n"

    def get_text(self) -> str:
        return self.text

    def __repr__(self) -> str:
        return self.get_text()


Content = Union[Image, Text]
ContentOrString = Union[Content, str]


def _diff(destination: Optional[bool], source: Optional[bool]) -> Optional[bool]:
    """
     D | S | R
    ---|---|---
     T | T | N   # No Change
     T | F | T   # Turn on
     T | N | T   # Probably shouldn't happen
     F | T | F   # Turn off
     F | F | N   # No Change
     F | N | F   # Probably shouldn't happen
     N | T | ?   # Set to default, which is?
     N | F | ?   # Set to default, which is?
     N | N | N   # No Change
    """
    if destination is source:
        # No Change
        return None

    if destination is None:
        # TODO: Assuming default is False for now, but that seems wrong.
        return False

    return destination


_NO_REFERRANT: object = {}


class Span(Node):
    """Base class for all nodes that span content, or other spans, but don't
    break blocks."""

    def __init__(self) -> None:
        super().__init__()
        self.children: list[ContentOrSpan] = []
        self.bold: Optional[bool] = None
        self.color: Optional[str] = None
        self.font_name: Optional[str] = None
        self.font_size: Optional[int] = None
        self.italics: Optional[bool] = None
        self.line: Optional[bool] = None
        self.referrant: Optional[Any] = None
        self.sentence: Optional[bool] = None
        self.underline: Optional[bool] = None

    def _prefix(self) -> str:
        result = super()._prefix()
        if self.bold is not None:
            result += f" bold={self.bold}"
        if self.color is not None:
            result += f" color={self.color}"
        if self.font_name is not None:
            result += f" font_name={self.font_name}"
        if self.font_size is not None:
            result += f" font_size={self.font_size}"
        if self.italics is not None:
            result += f" italics={self.italics}"
        if self.line is not None:
            result += f" line={self.line}"
        if self.referrant is not None:
            result += f" referrant={self.referrant}"
        if self.sentence is not None:
            result += f" sentence={self.sentence}"
        if self.underline is not None:
            result += f" underline={self.underline}"
        return result

    def to_pretty_string(self, level: int = 0) -> str:
        result = _INDENT * level + self._prefix()

        if self.children:
            result += "\n"

        for c in self.children:
            result += c.to_pretty_string(level + 1)

        result += _INDENT * level + self._suffix() + "\n"
        return result

    def __repr__(self) -> str:
        result = ""
        if self.bold:
            result += "**"
        if self.italics:
            result += "*"
        if self.underline:
            result += f"__"
        if self.referrant is not None:
            result += f"["

        for c in self.children:
            result += repr(c)

        if self.referrant is not None:
            result += f"]({self.referrant})"
        if self.underline:
            result += f"__"
        if self.italics:
            result += "*"
        if self.bold:
            result += "**"
        return result


    def get_text(self) -> str:
        result = ""
        for c in self.children:
            result += c.get_text()
        return result

    def apply(self, span: Span) -> Span:
        """Applies the given span settings over this span's settings, returning
        a new span, leaving this and the specified span unmodified."""
        result = Span()
        if not span:
            span = Span()

        if span.bold is not None:
            result.bold = span.bold
        else:
            result.bold = self.bold

        if span.color is not None:
            result.color = span.color
        else:
            result.color = self.color

        if span.font_name is not None:
            result.font_name = span.font_name
        else:
            result.font_name = self.font_name

        if span.font_size is not None:
            result.font_size = span.font_size
        else:
            result.font_size = self.font_size

        result.line = span.line

        if span.italics is not None:
            result.italics = span.italics
        else:
            result.italics = self.italics

        result.sentence = span.sentence

        if span.underline is not None:
            result.underline = span.underline
        else:
            result.underline = self.underline

        if span.referrant is not None:
            result.referrant = span.referrant
        else:
            result.referrant = self.referrant

        return result

    def diff(self, span: Span) -> Span:
        """Returns the result of `self - span`, which unchanged properties set
        to None. In other words, the result will be the changes needed to make
        `span` become `self`."""

        result = Span()
        if not span:
            span = Span()

        result.bold = _diff(self.bold, span.bold)

        if self.color != span.color:
            result.color = self.color
        if self.font_name != span.font_name:
            result.font_name = self.font_name
        if self.font_size != span.font_size:
            result.font_size = self.font_size

        result.italics = _diff(self.italics, span.italics)
        result.sentence = self.sentence
        result.line = self.line
        result.underline = _diff(self.underline, span.underline)
        if self.referrant is not span.referrant:
            if not self.referrant:
                result.referrant = _NO_REFERRANT
            else:
                result.referrant = self.referrant
        return result


ContentOrSpan = Union[Content, Span]
ContentSpanOrString = Union[Content, Span, str]


class Block(Node):
    """A block can change indentation level, and will terminate vertically
    before any previous or subsequent content."""

    def __init__(self) -> None:
        super().__init__()
        self.children: list[BlockOrSpan] = []

        self.alignment: Optional[str] = None
        self.background_color: Optional[str] = None
        self.border: Optional[bool] = None
        self.indentation_level: Optional[int] = None
        self.header_level: Optional[int] = None
        self.paragraph: Optional[bool] = None

    def _prefix(self) -> str:
        result = super()._prefix()
        if self.alignment is not None:
            result += f" alignment={self.alignment}"
        if self.background_color is not None:
            result += f" background_color={self.background_color}"
        if self.border is not None:
            result += f" border={self.border}"
        if self.indentation_level is not None:
            result += f" indentation_level={self.indentation_level}"
        if self.header_level is not None:
            result += f" header_level={self.header_level}"
        if self.paragraph is not None:
            result += f" paragraph={self.paragraph}"
        return result

    def to_pretty_string(self, level: int = 0) -> str:
        result = _INDENT * level + self._prefix()

        if self.children:
            result += "\n"

        for c in self.children:
            result += c.to_pretty_string(level + 1)

        result += _INDENT * level + self._suffix() + "\n"
        return result

    def get_text(self) -> str:
        result = ""
        for c in self.children:
            result += c.get_text()
        return result

    def apply(self, block: Block) -> Block:
        """Applies the given block's settings over this block's settings,
        returning a new span, leaving this and the specified block
        unmodified."""
        if not block:
            return self
        result = Block()

        if block.alignment is not None:
            result.alignment = block.alignment
        else:
            result.alignment = self.alignment

        if block.background_color is not None:
            result.background_color = block.background_color
        else:
            result.background_color = self.background_color

        if block.border is not None:
            result.border = block.border
        else:
            result.border = self.border

        if block.indentation_level is not None:
            result.indentation_level = block.indentation_level
        else:
            result.indentation_level = self.indentation_level

        if block.header_level is not None:
            result.header_level = block.header_level

        if block.paragraph is not None:
            result.paragraph = block.paragraph

        return result

    def __repr__(self) -> str:
        result = "\n\n"
        for c in self.children:
            result += repr(c)
        return result


BlockOrSpan = Union[Block, Span]
ContentBlockSpanOrString = Union[Content, Block, Span, str]
Nodeish = Union[Node, str]
Spanish = Union[Span, Content, str]
Blockish = Union[Block, Span, Content, str]


class Fragment(Node):
    """A node used for grouping blocks."""

    def __init__(self) -> None:
        super().__init__()
        self.children: list[Block] = []

    def to_pretty_string(self, level: int = 0) -> str:
        result = _INDENT * level + self._prefix()

        if self.children:
            result += "\n"

        for c in self.children:
            result += c.to_pretty_string(level + 1)

        result += _INDENT * level + self._suffix() + "\n"
        return result

    def get_text(self) -> str:
        result = ""
        for c in self.children:
            result += c.get_text()
        return result

    def __repr__(self) -> str:
        result = ""
        for c in self.children:
            result += repr(c)
        return result


class Processor(object):
    """ABC for processing an MDDOM tree as an event stream."""

    def __init__(self) -> None:
        super().__init__()

    def _process(self, node: Node) -> None:
        if isinstance(node, Fragment):
            self.fragment_start(node)
            for fc in node.children:
                self._process(fc)
            self.fragment_end(node)
        elif isinstance(node, Block):
            self.block_start(node)
            for bc in node.children:
                self._process(bc)
            self.block_end(node)
        elif isinstance(node, Span):
            self.span_start(node)
            for sc in node.children:
                self._process(sc)
            self.span_end(node)
        elif isinstance(node, Text):
            self.text(node)
        elif isinstance(node, Image):
            self.image(node)

    def process(self, node: Node) -> None:
        self._process(node)

    def reset(self) -> None:
        pass

    def clear(self) -> None:
        pass

    @property
    def buffer(self) -> str:
        return ""

    def fragment_start(self, fragment: Fragment) -> None:
        pass

    def fragment_end(self, fragment: Fragment) -> None:
        pass

    def block_start(self, block: Block) -> None:
        pass

    def block_end(self, block: Block) -> None:
        pass

    def span_start(self, span: Span) -> None:
        pass

    def span_end(self, span: Span) -> None:
        pass

    def text(self, text: Text) -> None:
        pass

    def image(self, image_node: Image) -> None:
        pass


def _sew_block(destination: list[BlockOrSpan], args: Sequence[Optional[ContentSpanOrString]]) -> None:
    last_text: Optional[Text] = None
    for arg in args:
        if arg is None:
            continue

        if isinstance(arg, str):
            if not arg:
                continue
            if last_text:
                last_text.text += arg
                continue
            arg = Text(arg)
            last_text = arg

        if not isinstance(arg, Text):
            last_text = None

        if isinstance(arg, Image) or isinstance(arg, Text):
            span = Span()
            span.children.append(arg)
            arg = span

        destination.append(arg)


def _sew_span(destination: list[ContentOrSpan], args: Sequence[Optional[ContentSpanOrString]]) -> None:
    last_text: Optional[Text] = None
    for arg in args:
        if arg is None:
            continue

        if isinstance(arg, str):
            if not arg:
                continue
            if last_text:
                last_text.text += arg
                continue
            arg = Text(arg)

        if isinstance(arg, Text):
            last_text = arg
        else:
            last_text = None

        destination.append(arg)


def sp(*args: Optional[ContentSpanOrString]) -> Span:
    """Span."""
    result = Span()
    _sew_span(result.children, args)
    return result


def l(*args: Optional[ContentSpanOrString]) -> Span:
    """Line."""
    result = sp(*args)
    result.line = True
    return result


def s(*args: Optional[ContentSpanOrString]) -> Span:
    """Sentence."""
    result = sp(*args)
    result.sentence = True
    return result


def b(*args: Optional[ContentSpanOrString]) -> Span:
    """Bold."""
    result = sp(*args)
    result.bold = True
    return result


def bi(*args: Optional[ContentSpanOrString]) -> Span:
    """Bold Italics."""
    result = sp(*args)
    result.bold = True
    result.italics = True
    return result


def bu(*args: Optional[ContentSpanOrString]) -> Span:
    """Bold Underline."""
    result = sp(*args)
    result.bold = True
    result.underline = True
    return result


def biu(*args: Optional[ContentSpanOrString]) -> Span:
    """Bold Italics Underline."""
    result = sp(*args)
    result.bold = True
    result.italics = True
    result.underline = True
    return result


def i(*args: Optional[ContentSpanOrString]) -> Span:
    """Italics."""
    result = sp(*args)
    result.italics = True
    return result


def iu(*args: Optional[ContentSpanOrString]) -> Span:
    """Italics Underline."""
    result = sp(*args)
    result.italics = True
    result.underline = True
    return result


def u(*args: Optional[ContentSpanOrString]) -> Span:
    """Underline."""
    result = sp(*args)
    result.underline = True
    return result


ib = bi
ibu = biu
iub = biu
ub = bu
ubi = biu
ui = iu
uib = biu


def f(*args: Optional[Block]) -> Fragment:
    """Fragment."""
    result = Fragment()
    result.children = [x for x in args if x]
    return result


def bl(*args: Optional[Block]) -> Block:
    """Block."""
    result = Block()
    result.children = [x for x in args if x]
    return result


def p(*args: Optional[ContentSpanOrString]) -> Block:
    """Paragraph."""
    result = Block()
    result.paragraph = True
    _sew_block(result.children, args)
    return result


def h(level: int, *args: Optional[ContentSpanOrString]) -> Block:
    """Header."""
    result = Block()
    result.header_level = level
    _sew_block(result.children, args)
    return result


def h1(*args: Optional[ContentSpanOrString]) -> Block:
    """Header level 1."""
    return h(1, *args)


def h2(*args: Optional[ContentSpanOrString]) -> Block:
    """Header level 2."""
    return h(2, *args)


def h3(*args: Optional[ContentSpanOrString]) -> Block:
    """Header level 3."""
    return h(3, *args)


def h4(*args: Optional[ContentSpanOrString]) -> Block:
    """Header level 4."""
    return h(4, *args)


def h5(*args: Optional[ContentSpanOrString]) -> Block:
    """Header level 5."""
    return h(5, *args)


def _ensure_text_item_list(items: list[Optional[ContentSpanOrString]]) -> list[ContentOrSpan]:
    result: list[ContentOrSpan] = []
    for i in items:
        if i is None:
            continue

        if isinstance(i, str):
            i = i.lstrip()
            if i == "":
                continue
            result.append(Text(i))
            continue

        result.append(i)
    return result


def text_list(items: list[Optional[ContentSpanOrString]]) -> Span:
    comma_emitted = False
    use_comma = True
    use_and = True
    items2 = _ensure_text_item_list(items)
    count = len(items2)
    if count == 1:
        use_and = False
    if count <= 2:
        use_comma = False

    result = Span()
    for index, item in enumerate(items2):
        if use_comma:
            if not comma_emitted:
                comma_emitted = True
            else:
                result.children.append(Text(","))
        if use_and:
            if index == count - 1:
                result.children.append(Text(" and"))
        result.children.append(Text(" "))
        result.children.append(item)
    return result


class AnsiTerminalRenderer(Processor):
    def __init__(self) -> None:
        super().__init__()
        self.ansi = True
        self._line_max = 78
        self.reset()

    @property
    def buffer(self) -> str:
        return self._buffer

    def clear(self) -> None:
        self._buffer = ""

    def reset(self) -> None:
        self._buffer = ""
        self._characters = 0
        self._block_state = [Block()]
        self._span_state = [Span()]
        self._sentence_start_pending = False
        self._sentence_end_pending = False
        self._whitespace_pending = False
        self._block_newline_pending = False
        self._absorb_leading_whitespace = True

    def _get_block(self, depth=0) -> Block:
        return self._block_state[-(1 + depth)]

    def _get_span(self, depth=0) -> Span:
        return self._span_state[-(1 + depth)]

    def fragment_start(self, fragment: Fragment) -> None:
        if self.ansi:
            self._buffer += ansi.RESET

    def fragment_end(self, fragment: Fragment) -> None:
        if self.ansi:
            self._buffer += ansi.RESET

    def block_start(self, block: Block) -> None:
        self._absorb_leading_whitespace = True
        self._block_state.append(self._get_block().apply(block))
        if self._block_newline_pending:
            if self._characters > 0:
                self._buffer += "\n"
                self._characters = 0
            self._buffer += "\n"
            self._block_newline_pending = False
            self._whitespace_pending = False
        header_level = self._get_block().header_level
        if header_level:
            synthetic_span = Span()
            synthetic_span.bold = True
            self._span_state.append(self._get_span().apply(synthetic_span))
            top = self._get_span()
            diff = top.diff(self._get_span(1))
            self._apply_diff(diff, top)

    def block_end(self, block: Block) -> None:
        self._block_state.pop()
        self._block_newline_pending = True
        header_level = block.header_level
        if header_level:
            old = self._span_state.pop()
            top = self._get_span()
            diff = top.diff(old)
            self._apply_diff(diff, top)

    def _apply_raw(self, span: Span) -> None:
        if not self.ansi:
            return

        if span.bold is not None and span.bold:
            self._buffer += ansi.BOLD

        if span.italics is not None and span.italics:
            self._buffer += ansi.ITALIC

        if span.underline is not None and span.underline:
            self._buffer += ansi.UNDERLINE

    def _apply_diff(self, diff: Span, top: Span) -> None:
        if not self.ansi:
            return

        reset = False
        if ((diff.bold is not None and not diff.bold) or
            (diff.italics is not None and not diff.italics) or
                (diff.underline is not None and not diff.underline)):
            reset = True
            self._buffer += ansi.RESET

        if reset:
            self._apply_raw(top)
        else:
            self._apply_raw(diff)

    def span_start(self, span: Span) -> None:
        self._span_state.append(self._get_span().apply(span))
        top = self._get_span()
        if top.sentence:
            self._sentence_start_pending = True

        diff = top.diff(self._get_span(1))
        self._apply_diff(diff, top)

    def span_end(self, span: Span) -> None:
        state = self._span_state.pop()
        top = self._get_span()
        diff = top.diff(state)
        self._apply_diff(diff, top)
        if span.sentence and self._sentence_end_pending:
            self._buffer += "."
            self._characters += 1
            self._sentence_end_pending = False
            self._whitespace_pending = True

        if span.line:
            self._buffer += "\n"
            self._characters = 0
            self._whitespace_pending = False
            self._absorb_leading_whitespace = True

    def text(self, text: Text) -> None:
        if not text or not text.text:
            return

        word_list = words(text.text)
        if not word_list:
            # Whitespace
            self._whitespace_pending = True
            return

        if self._absorb_leading_whitespace:
            self._whitespace_pending = False
            self._absorb_leading_whitespace = False
        else:
            self._whitespace_pending |= len(
                text.text.lstrip()) < len(text.text)

        for w in word_list:
            wordlen = len(w)

            pre_space_len = 0
            if self._whitespace_pending:
                pre_space_len = 1

            if self._characters + wordlen + pre_space_len >= 78:
                self._buffer += "\n"
                self._characters = 0
                self._whitespace_pending = False

            if self._whitespace_pending:
                self._buffer += " "
                self._whitespace_pending = False
                self._characters += 1

            if self._sentence_start_pending:
                self._buffer += w[0].upper()
                w = w[1:]
                self._characters += 1
                self._sentence_start_pending = False
                self._sentence_end_pending = True

            self._buffer += w
            self._characters += wordlen
            self._whitespace_pending = True

        # We don't want to add trailing whitespace when it shouldn't be there.
        self._whitespace_pending = False

        if self._sentence_end_pending:
            if self._buffer[-1] in [".", "?", "!"]:
                self._sentence_end_pending = False
                self._whitespace_pending = True

        self._whitespace_pending |= len(text.text.rstrip()) < len(text.text)

    def image(self, image_node: Image) -> None:
        pass


class HtmlRenderer(Processor):
    def __init__(self) -> None:
        super().__init__()
        self.reset()

    @property
    def buffer(self) -> str:
        return self._buffer

    def clear(self) -> None:
        self._buffer = ""

    def reset(self) -> None:
        self._buffer = ""
        self._block_state = [Block()]
        self._span_state = [Span()]
        self._sentence_start_pending = False
        self._sentence_end_pending = False
        self._whitespace_pending = False
        self._absorb_leading_whitespace = True
        self._deferred_diff: Optional[Span] = None

    def _get_block(self, depth=0) -> Block:
        return self._block_state[- (1 + depth)]

    def _get_span(self, depth=0) -> Span:
        return self._span_state[- (1 + depth)]

    def fragment_start(self, fragment: Fragment) -> None:
        pass

    def fragment_end(self, fragment: Fragment) -> None:
        pass

    def block_start(self, block: Block) -> None:
        self._absorb_leading_whitespace = True
        self._block_state.append(self._get_block().apply(block))
        if self._get_block().paragraph:
            self._buffer += "<p>"
            self._whitespace_pending = False
        header_level = self._get_block().header_level
        if header_level:
            self._buffer += f"<h{header_level}>"

    def block_end(self, block: Block) -> None:
        state = self._block_state.pop()
        if state.paragraph:
            self._buffer += "</p>"
        header_level = state.header_level
        if header_level:
            self._buffer += f"</h{header_level}>"

    def _apply_diff(self, diff: Span) -> None:
        self._apply_diff_close(diff)
        self._apply_diff_open(diff)

    def _apply_diff_close(self, diff: Span) -> None:
        if diff.underline is not None and not diff.underline:
            self._buffer += "</u>"
        if diff.italics is not None and not diff.italics:
            self._buffer += "</i>"
        if diff.bold is not None and not diff.bold:
            self._buffer += "</b>"
        if diff.referrant is not None and diff.referrant is _NO_REFERRANT:
            self._buffer += "</span>"

    def _apply_diff_open(self, diff: Span) -> None:
        if diff.referrant is not None and diff.referrant is not _NO_REFERRANT:
            self._buffer += ("<span class=\"referrant\" " +
                             f"data-value=\"{html_escape(repr(diff.referrant))}\" " +
                             "onclick=\"window.handleReferrantClick(this);\" " +
                             ">")
        if diff.bold is not None and diff.bold:
            self._buffer += "<b>"
        if diff.italics is not None and diff.italics:
            self._buffer += "<i>"
        if diff.underline is not None and diff.underline:
            self._buffer += "<u>"

    def span_start(self, span: Span) -> None:
        self._span_state.append(self._get_span().apply(span))
        top = self._get_span()
        if top.sentence:
            self._sentence_start_pending = True

        diff = top.diff(self._get_span(1))
        if self._whitespace_pending:
            self._deferred_diff = diff
        else:
            self._apply_diff(diff)

    def span_end(self, span: Span) -> None:
        state = self._span_state.pop()
        diff = self._get_span().diff(state)
        if span.sentence and self._sentence_end_pending:
            self._buffer += "."
            self._sentence_end_pending = False
            self._whitespace_pending = True
        if state.line:
            self._buffer += "<br/>"
            self._whitespace_pending = False
            self._absorb_leading_whitespace = True
        if self._whitespace_pending:
            self._deferred_diff = diff
        else:
            self._apply_diff(diff)

    def text(self, text: Text) -> None:
        text_len = len(text.text)
        if text is None or text_len == 0:
            return

        word_list = words(html_escape(text.text))
        if not word_list:
            # Whitespace
            self._whitespace_pending = True
            return

        if self._absorb_leading_whitespace:
            self._whitespace_pending = False
            self._absorb_leading_whitespace = False
        else:
            self._whitespace_pending |= len(text.text.lstrip()) < text_len

        for w in word_list:
            if self._whitespace_pending:
                self._buffer += " "
                self._whitespace_pending = False

            if self._deferred_diff:
                diff = self._deferred_diff
                self._deferred_diff = None
                self._apply_diff(diff)

            if self._sentence_start_pending:
                self._buffer += w[0].upper()
                w = w[1:]
                self._sentence_start_pending = False
                self._sentence_end_pending = True

            self._buffer += w
            self._whitespace_pending = True

        # We don't want to add trailing whitespace when it shouldn't be there.
        self._whitespace_pending = False

        if self._sentence_end_pending:
            if self._buffer[-1] in [".", "?", "!"]:
                self._sentence_end_pending = False
                self._whitespace_pending = True

        self._whitespace_pending |= len(text.text.rstrip()) < text_len

    def image(self, image_node: Image) -> None:
        pass
