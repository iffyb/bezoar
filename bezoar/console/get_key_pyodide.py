from typing import Optional

import js  # type: ignore
from asyncio import Queue, create_task
from bezoar.console.key_stroke import KeyStroke

GetKeyCallable = object

_queue = Queue[Optional[KeyStroke]]()


def _key_handler(e):
    if e.ctrlKey or e.metaKey or e.altKey:
        return
    key_name = e.key
    key = KeyStroke.by_name(key_name)
    if key:
        create_task(_queue.put(key))
        e.preventDefault()


js.window.onkeydown = _key_handler


def wake_get_key() -> None:
    create_task(_queue.put(None))


async def get_key() -> Optional[KeyStroke]:
    return await _queue.get()
