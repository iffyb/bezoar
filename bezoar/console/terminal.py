from typing import Callable, Optional

from bezoar.base.environment import IS_PYODIDE
from bezoar.console.get_key import get_key, wake_get_key
from bezoar.console.key_stroke import KeyStroke

if IS_PYODIDE:
    import js  # type: ignore


class Terminal:
    """A low-level asynchronous terminal abstraction."""

    def __init__(self) -> None:
        self._last_line = ""
        self._prompt_level = 0
        self._referrant_callback: Callable[[str], None] = lambda x: None
        if IS_PYODIDE:
            js.window.terminal = self

    def wake_read_key(self) -> None:
        """
        Causes the current or next call to read_key to return immediately with
        a result of None.
        """

        wake_get_key()

    async def read_key(self) -> Optional[KeyStroke]:
        """
        Reads a keystroke from the terminal, waiting up to `timeout_ns`. If
        nothing was hit within that time, or wake_read_key() is called, returns
        None. If a key is hit, it is not echoed back.
        """

        return await get_key()

    def set_referrant_callback(self, callback: Callable[[str], None]) -> None:
        self._referrant_callback = callback

    def call_referrant(self, referrant: str) -> None:
        self._referrant_callback(referrant)

    def open_prompt(self) -> None:
        self._prompt_level += 1
        if self._prompt_level == 1:
            if IS_PYODIDE:
                js.window.open_prompt()
            else:
                if self._last_line:
                    self.write("\n")
                self._write_raw("\n")

    def close_prompt(self) -> None:
        if self._prompt_level == 0:
            return

        self._prompt_level -= 1
        if self._prompt_level == 0:
            if IS_PYODIDE:
                js.window.close_prompt()
            pass

    def clear_prompt(self) -> None:
        if self._prompt_level == 0:
            return
        if IS_PYODIDE:
            js.window.clear_prompt()

    def draw_prompt(self) -> None:
        if self._prompt_level == 0:
            return
        if IS_PYODIDE:
            js.window.draw_prompt()

    def write(self, text: str) -> None:
        """
        Writes the given text out to the terminal with no processing, but keeps
        track of the last line.
        """

        if not text:
            return

        last_newline = text.rfind("\n")
        if last_newline < 0:
            self._last_line += text
        else:
            self._last_line = text[last_newline + 1:]
        self._write_raw(text)

    def clear_line(self) -> None:
        """
        Clears the line the terminal cursor is currently on, moving the cursor
        to the beginning of the line.
        """

        count = len(self._last_line)
        self._write_raw("\r")
        self._write_raw(" " * count)
        self._write_raw("\r")
        self._last_line = ""

    def backspace(self, count: int = 1) -> None:
        """
        Destructively backs the terminal cursor up by one character.
        """

        max = len(self._last_line)
        if count > max:
            count = max
        self._last_line = self._last_line[0:-count]
        for x in range(0, count):
            self._write_raw("\b \b")

    def _write_raw(self, text: str) -> None:
        """Writes the given text out to the terminal with no processing."""

        if IS_PYODIDE:
            js.window.emit(text)
        else:
            print(text, end="", sep="", flush=True)

    @property
    def last_line(self) -> str:
        """
        The last line written out to the terminal. Resets on every newline.
        """

        return self._last_line
