from typing import Callable, Optional

from bezoar.base.environment import IS_PYODIDE
from bezoar.console.key_stroke import KeyStroke

GetKeyCallable = Callable[[], Optional[KeyStroke]]

if IS_PYODIDE:
    from bezoar.console.get_key_pyodide import get_key, wake_get_key
else:
    try:
        import termios

        from bezoar.console.get_key_termios import get_key, wake_get_key
    except ImportError:
        from bezoar.console.get_key_windows import get_key, wake_get_key
