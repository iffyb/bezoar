from typing import IO, Any, Callable, Optional

from asyncio import Event, Queue, create_task
from bezoar.console import key_stroke
from bezoar.console.terminal import Terminal

LineCallback = Callable[[str], None]


class _LineRequest(object):
    def __init__(self,
                 prompt: Optional[str] = "") -> None:
        super().__init__()
        self.prompt = prompt


_LineRequestList = list[_LineRequest]


class Console(object):
    """An input/output console with pluggable tab completion."""

    def __init__(self, terminal: Optional[Terminal] = None) -> None:
        if terminal is not None:
            self._terminal = terminal
        else:
            # Autodetect terminal, input/output
            self._terminal = Terminal()
        self._editor_buffer = ""
        self._editor_requests = _LineRequestList()
        self._editor_visible = False
        self._wake_token = False
        self._run_task: Any = None
        self._running_event: Event = Event()
        self._running_event.set()
        self._line_queue = Queue[str]()
        self._transcript_file: Optional[IO] = None
        self._transcript_filename: Optional[str] = None
        self._injection: Optional[str] = None

    def inject_command(self, command: str) -> None:
        if not self._editor_visible:
            return
        self._injection = command
        self._terminal.wake_read_key()

    def print(self, message: str) -> None:
        """Prints text to the console."""
        self._clear_editor()

        self._terminal.write(message)

        if self._transcript_file is not None:
            self._transcript_file.write(message)

    def start_transcript(self, transcript_filename: str) -> None:
        self._transcript_filename = transcript_filename
        self._transcript_file = open(transcript_filename, "a")

    def stop_transcript(self) -> None:
        if self._transcript_file is None:
            return
        self._transcript_file.close()
        self._transcript_file = None
        self._transcript_filename = None

    def set_referrant_callback(self, callback: Callable[[str], None]) -> None:
        self._terminal.set_referrant_callback(callback)

    async def read_line(self,
                        prompt: Optional[str] = "") -> Optional[str]:
        """
        Initiates user input once with the given prompt. Unless read_line() is
        called, the Console will drop input.
        """

        await self._start()
        if self.is_editing:
            return None
        self._editor_requests.append(_LineRequest(prompt))
        self._terminal.open_prompt()
        self._draw_editor()
        line = await self._line_queue.get()
        self._terminal.close_prompt()
        return line

    def stop(self) -> None:
        self._wake_token = True
        self._terminal.wake_read_key()

    async def _start(self) -> None:
        if self._run_task is not None:
            return
        self._wake_token = False
        self._run_task = create_task(self._run())

    async def _run(self) -> None:
        """
        Processes input and runs the editor, if editing. Returns False if the
        run timed out, and True if it was awoken.
        """

        try:
            while True:
                saved_editor_buffer = ""
                if self._wake_token:
                    self._wake_token = False
                    return

                key = await self._terminal.read_key()

                # Drop keys if not editing.
                if not self.is_editing:
                    continue

                if key is None:
                    if self._injection is None:
                        continue
                    injection = self._injection
                    self._injection = None
                    saved_editor_buffer = self._editor_buffer
                    for _ in range(0, len(self._editor_buffer)):
                        self._terminal.backspace()
                    self._editor_buffer = injection
                    self._terminal.write(injection)
                    key = key_stroke.CARRIAGE_RETURN

                # Ignore double spaces, which hypothetically should never be
                # needed.
                if key == key_stroke.SPACE:
                    if (self._editor_buffer.endswith(" ") or
                            self._editor_buffer.endswith("\u00A0")):
                        continue

                if key == key_stroke.BACKSPACE:
                    if self._editor_buffer:
                        self._terminal.backspace()
                        self._editor_buffer = self._editor_buffer[:-1]
                    continue

                if (key == key_stroke.LINE_FEED or
                        key == key_stroke.CARRIAGE_RETURN):
                    self._terminal.write("\n")
                    result = self._editor_buffer
                    self._editor_buffer = saved_editor_buffer
                    self._pop_editor_request()
                    await self._line_queue.put(result)
                    self._editor_visible = False
                    self._draw_editor()
                    continue

                if key == key_stroke.SPACE:
                    # Convert spaces to non-breaking spaces. Split and other
                    # things still seem to work.
                    key_str = "\u00A0"
                else:
                    key_str = chr(key.code)

                self._editor_buffer += key_str
                self._terminal.write(key_str)
        finally:
            self._run_task = None

    @property
    def is_editing(self) -> bool:
        return len(self._editor_requests) > 0

    @property
    def _prompt(self) -> Optional[str]:
        if not self.is_editing:
            return None
        return self._editor_requests[0].prompt

    def _pop_editor_request(self) -> Optional[_LineRequest]:
        if not self.is_editing:
            return None
        request = self._editor_requests[0]
        self._editor_requests = self._editor_requests[1:]
        return request

    def _clear_editor(self) -> None:
        if not self.is_editing:
            return

        if not self._editor_visible:
            return

        self._terminal.clear_line()
        self._terminal.clear_prompt()
        self._editor_visible = False

    def _draw_editor(self) -> None:
        if not self.is_editing:
            return

        if self._editor_visible:
            return

        self._terminal.draw_prompt()
        if self._prompt:
            self._terminal.write(self._prompt)
        self._terminal.write(self._editor_buffer)
        self._editor_visible = True
