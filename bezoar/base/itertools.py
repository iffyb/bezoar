from typing import Iterable, Optional, TypeVar

E = TypeVar('E')


def first(i: Iterable[E], default: Optional[E] = None) -> Optional[E]:
    try:
        return next(iter(i))
    except StopIteration:
        return default
