"""Generic, useful algorithms."""


from typing import Callable, Iterable, Iterator, Optional, TypeVar

Key = TypeVar('Key')
Value = TypeVar('Value')


def dfs(root: Key,
        get_value: Callable[[Key], Optional[Value]],
        get_children: Callable[[Key], Iterable[Key]]) -> Iterator[Value]:
    """Depth-First Search, returning a sequence in pre-order traversal."""

    if root is None:
        return

    result = get_value(root)
    if result is not None:
        yield result
    for child in get_children(root):
        yield from dfs(child, get_value, get_children)
