from __future__ import annotations

import math
from sys import maxsize
from typing import Any, Union
from bezoar.base.environment import IS_PYODIDE

DurationNs = int


def _strtype(o: Any) -> str:
    if o.__class__:
        return o.__class__.__name__
    return str(type(o))


_NS_PER_US = 1000
_NS_PER_MS = 1000 * _NS_PER_US
_NS_PER_SEC = 1000 * _NS_PER_MS
_NS_PER_MIN = 60 * _NS_PER_SEC
_NS_PER_HOUR = 60 * _NS_PER_MIN
_NS_PER_DAY = 24 * _NS_PER_HOUR
_NS_PER_WEEK = 7 * _NS_PER_DAY

_MAX_SIZE = maxsize if not IS_PYODIDE else maxsize * _NS_PER_MS


class Duration(object):
    def __init__(self, time_ns: DurationNs = 0) -> None:
        self._ns = time_ns

    def __lt__(self, o: Any) -> bool:
        if isinstance(o, Duration):
            return self._ns < o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Duration")

    def __le__(self, o: Any) -> bool:
        if isinstance(o, Duration):
            return self._ns <= o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Duration")

    def __eq__(self, o: Any) -> bool:
        if isinstance(o, Duration):
            return self._ns == o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Duration")

    def __gt__(self, o: Any) -> bool:
        return not self.__le__(o)

    def __ge__(self, o: Any) -> bool:
        return not self.__lt__(o)

    def __ne__(self, o: Any) -> bool:
        return not self.__eq__(o)

    def __hash__(self) -> int:
        return self._ns

    def __add__(self, o: Any) -> Union[Duration, Time]:
        if isinstance(o, Duration):
            return self.add(o)

        if isinstance(o, Time):
            return o.add(self)

        raise TypeError(f"Cannot add {_strtype(o)} to Duration")

    def __sub__(self, o: Any) -> Duration:
        if isinstance(o, Duration):
            return self.subtract(o)

        raise TypeError(
            f"Cannot subtract {_strtype(o)} from Duration")

    def __mul__(self, o: Any) -> Duration:
        return self.scale(float(o))

    def __repr__(self) -> str:
        return f"Duration({self._ns})"

    def __str__(self) -> str:
        sign = self._ns >= 0
        nanos = abs(self._ns)
        weeks = math.floor(nanos / _NS_PER_WEEK)
        nanos -= weeks * _NS_PER_WEEK
        days = math.floor(nanos / _NS_PER_DAY)
        nanos -= days * _NS_PER_DAY
        hours = math.floor(nanos / _NS_PER_HOUR)
        nanos -= hours * _NS_PER_HOUR
        mins = math.floor(nanos / _NS_PER_MIN)
        nanos -= mins * _NS_PER_MIN
        secs = math.floor(nanos / _NS_PER_SEC)
        nanos -= secs * _NS_PER_SEC
        millis = math.floor(nanos / _NS_PER_MS)
        nanos -= millis * _NS_PER_MS
        micros = math.floor(nanos / _NS_PER_US)
        nanos -= micros * _NS_PER_US
        result = []
        if weeks > 0:
            result.append(f"{weeks}w")
        if days > 0:
            result.append(f"{days}d")
        if hours > 0:
            result.append(f"{hours}h")
        if mins > 0:
            result.append(f"{mins}m")
        if secs > 0:
            result.append(f"{secs}s")
        if millis > 0:
            result.append(f"{millis}ms")
        if micros > 0:
            result.append(f"{micros}us")
        if nanos > 0:
            result.append(f"{nanos}ns")
        if not result:
            return "0"
        return f"{'' if sign else '-'}{' '.join(result)}"

    def to_seconds(self) -> float:
        return self._ns / 1000000000.0

    def to_milliseconds(self) -> int:
        return math.floor(self._ns / 1000000)

    def to_nanoseconds(self) -> DurationNs:
        return self._ns

    def add(self, duration: Duration) -> Duration:
        ns = duration._ns
        if ns >= _MAX_SIZE - self._ns:
            return max_duration()
        return Duration(ns + self._ns)

    def subtract(self, duration: Duration) -> Duration:
        return Duration(self._ns - duration._ns)

    def scale(self, factor: float) -> Duration:
        return Duration(math.floor(self._ns * factor))


class Time(object):
    def __init__(self, time_since_epoch_ns: DurationNs = 0) -> None:
        self._ns = time_since_epoch_ns

    def __lt__(self, o: Any) -> bool:
        if isinstance(o, Time):
            return self._ns < o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Time")

    def __le__(self, o: Any) -> bool:
        if isinstance(o, Time):
            return self._ns <= o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Time")

    def __eq__(self, o: Any) -> bool:
        if isinstance(o, Time):
            return self._ns == o._ns
        raise TypeError(f"Cannot compare {_strtype(o)} to Time")

    def __gt__(self, o: Any) -> bool:
        return not self.__le__(o)

    def __ge__(self, o: Any) -> bool:
        return not self.__lt__(o)

    def __ne__(self, o: Any) -> bool:
        return not self.__eq__(o)

    def __repr__(self) -> str:
        return f"Time({self._ns})"

    def __str__(self) -> str:
        return f"Time({self.to_duration_since_epoch()})"

    def __hash__(self) -> int:
        return self._ns

    def __add__(self, o: Any) -> Time:
        if isinstance(o, Duration):
            return self.add(o)

        raise TypeError(f"Cannot add {_strtype(o)} to Time")

    def __sub__(self, o: Any) -> Union[Time, Duration]:
        if isinstance(o, Duration):
            return self.subtract(o)

        if isinstance(o, Time):
            return self.subtract_time(o)

        raise TypeError(f"Cannot subtract {_strtype(o)} from Time")

    def to_duration_since_epoch(self) -> Duration:
        return Duration(self._ns)

    def to_nanoseconds_since_epoch(self) -> DurationNs:
        return self._ns

    def add(self, duration: Duration) -> Time:
        ns = duration._ns
        if ns >= _MAX_SIZE - self._ns:
            return max_time()
        return Time(ns + self._ns)

    def subtract(self, duration: Duration) -> Time:
        return Time(self._ns - duration._ns)

    def subtract_time(self, time: Time) -> Duration:
        return Duration(self._ns - time._ns)


def zero() -> Duration:
    return Duration()


def nanoseconds(ns: int) -> Duration:
    return Duration(ns)


def microseconds(us: int) -> Duration:
    return Duration(us * 1000)


def milliseconds(ms: Union[int, float]) -> Duration:
    return Duration(math.floor(ms * 1000000))


def seconds(sec: Union[int, float]) -> Duration:
    return Duration(math.floor(sec * 1000000000))


def max_duration() -> Duration:
    return Duration(_MAX_SIZE)


def epoch() -> Time:
    return Time()


def max_time() -> Time:
    return Time(_MAX_SIZE)
