"""Hierarchical logger."""

import sys
from bezoar.base.environment import IS_PYODIDE

_stacks: dict[str, list[str]] = {}

def hlstart(scope:str, m:str) -> None:
    if scope not in _stacks:
        _stacks[scope] = []
    stack = _stacks[scope]

    dest = sys.stderr
    if IS_PYODIDE:
        dest = sys.stdout
    print(("  " * len(stack)) + f"BEG [{scope}] " + m, file=dest)
    stack.append(m)

def hlend(scope:str) -> None:
    if not _stacks:
        raise Exception("unbalanced hlend 1")
    if scope not in _stacks:
        raise Exception("unbalanced hlend 2")
    stack = _stacks[scope]
    m = stack.pop()
    dest = sys.stderr
    if IS_PYODIDE:
        dest = sys.stdout
    print(("  " * len(stack)) + f"END [{scope}] " + m, file=dest)

def hlog(scope:str, m:str) -> None:
    if not _stacks:
        raise Exception("unbalanced hlog 1")
    if scope not in _stacks:
        raise Exception("unbalanced hlog 2")

    stack = _stacks[scope]
    dest = sys.stderr
    if IS_PYODIDE:
        dest = sys.stdout
    print(("  " * len(stack)) + f"LOG [{scope}] " + m, file=dest)
