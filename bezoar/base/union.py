def union(*args: dict) -> dict:
    new_dict = {}
    for a in args:
        if not isinstance(a, dict):
            return NotImplemented
        new_dict.update(a)
    return new_dict
