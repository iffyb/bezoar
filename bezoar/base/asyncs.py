from __future__ import annotations

import asyncio
from asyncio import Event
from typing import Any, Awaitable, Callable, Optional

from bezoar.base.time import Duration, Time, epoch, seconds

Closure = Callable[[], Any]
ExecutorCallable = Callable[[Closure, Closure], Any]


async def yield_async() -> None:
    await sleep_until(get_loop_time())


async def race(*args: Awaitable[Any]) -> None:
    """Races the given awaitables, canceling any that aren't completed when
    the first one completes."""
    _, pending = await asyncio.wait(args, return_when=asyncio.FIRST_COMPLETED)
    for p in pending:
        p.cancel()


async def sleep_for(delay: Duration,
                    cancellation_event: Optional[Event] = None) -> bool:
    deadline = get_loop_time().add(delay)
    return await sleep_until(deadline, cancellation_event)


async def sleep_until(deadline: Time,
                      cancellation_event: Optional[Event] = None) -> bool:
    """Sleep until deadline or cancellation_event is set. Returns False iff
    cancelled."""

    if cancellation_event is None:
        await create_deadline_coro(deadline)
        return True

    return await wait_until(deadline, cancellation_event)


async def wait_until(deadline: Time, event: asyncio.Event) -> bool:
    delay = deadline.subtract_time(get_loop_time())
    event_coro = event.wait()
    try:
        await asyncio.wait_for(event_coro, delay.to_seconds())
    except asyncio.exceptions.TimeoutError:
        # In this case the event is a cancellation event, so timing out
        # is what we want for normal operation.
        event_coro.close()
        return True
    return False


async def wait_for(delay: Duration, event: Event) -> bool:
    return await wait_until(get_loop_time().add(delay), event)


def get_loop_time() -> Time:
    return epoch().add(seconds(asyncio.get_running_loop().time()))


def create_deadline_coro(deadline: Time):
    delay = deadline.subtract_time(get_loop_time())
    return asyncio.sleep(delay.to_seconds())
