"""
A flexible task scheduler.
"""

from __future__ import annotations

from asyncio import Event, Task, create_task
from typing import Optional

from bezoar.base.asyncs import get_loop_time, sleep_until
from bezoar.base.runnable import Runnable
from bezoar.base.time import Duration, Time, epoch, max_time, nanoseconds, zero
from bezoar.base.typed_number import TypedNumber


class CancelResult(TypedNumber):
    """The result of canceling a Task."""

    def __init__(self, value: int) -> None:
        super().__init__(value)


CANCEL_RESULT_CANCELED = CancelResult(1)
CANCEL_RESULT_ALREADY_RAN = CancelResult(2)


class RunResult(TypedNumber):
    """The result of running a Task."""

    def __init__(self, value: int) -> None:
        super().__init__(value)


RUN_RESULT_RAN = RunResult(0)
RUN_RESULT_CANCELED = RunResult(1)
RUN_RESULT_ALREADY_RAN = RunResult(2)
RUN_RESULT_DEADLINE_PENDING = RunResult(3)


class ScheduledTask(object):
    """A cancellable task that has been scheduled on a scheduler."""

    def __init__(self, deadline: Time, scheduler: Scheduler,
                 runnable: Runnable) -> None:
        super().__init__()
        self.deadline = deadline
        self._runnable = runnable
        self._scheduler = scheduler
        self._resolved = Event()
        self._result = RUN_RESULT_DEADLINE_PENDING

    async def wait(self) -> RunResult:
        """Async waits until this task has been resolved (either canceled or
        run)."""
        await self._resolved.wait()
        return self._result

    def cancel(self) -> CancelResult:
        """Cancels this task if it hasn't run already."""

        if self._resolved.is_set():
            return CANCEL_RESULT_ALREADY_RAN
        self._resolve(RUN_RESULT_CANCELED)
        return self._scheduler.cancel(self)

    async def run(self) -> RunResult:
        """Runs this task. Raises exception if resolved or early."""

        if self._resolved.is_set():
            raise Exception(f"Already resolved.")
        now = self._scheduler.now()
        if self.deadline.__gt__(now):
            raise Exception(f"Too early: {now} <= {self.deadline}")
        coro = self._runnable()
        if coro:
            await coro
        self._resolve(RUN_RESULT_RAN)
        return self._result

    def _resolve(self, result: RunResult) -> None:
        if self._resolved.is_set():
            raise Exception()
        self._result = result
        self._resolved.set()

    def __str__(self) -> str:
        return f"ScheduledTask(deadline={self.deadline}, runnable={self._runnable})"

    def __repr__(self) -> str:
        return f"ScheduledTask(deadline={self.deadline}, runnable={self._runnable})"


class Scheduler(object):
    """A timed work scheduler, which can be paused and advanced, or let run in
    realtime."""

    def __init__(self, paused: bool) -> None:
        super().__init__()
        self._paused = paused
        self._wakeup_event = Event()
        self._queue: list[ScheduledTask] = []
        self._idle_queue: list[ScheduledTask] = []
        self._terminate = False
        self._started = False
        self._simulated_now = epoch()
        self._clock_offset = self._simulated_now.subtract_time(get_loop_time())
        self._run_task: Optional[Task[None]] = None
        self._target_now = self._simulated_now
        self._target_paused = self._paused
        self.scope = str(abs(hash(self)) % 1024)

    @property
    def paused(self) -> bool:
        return self._paused

    def start(self) -> None:
        """Starts the scheduler running."""

        if self._started:
            raise Exception()
        self._terminate = False
        self._started = True
        self._run_task = create_task(self._run())

    async def stop(self) -> None:
        """Stops the scheduler running. Drains tasks that are already due."""

        if not self._run_task:
            raise Exception()
        self._terminate = True
        self._wake()
        await self._run_task
        self._started = False
        self._run_task = None

    def pause(self) -> None:
        """Moves this scheduler into paused mode. This means that time will
        not advance unless advance() is called."""

        if self._paused:
            return
        self._paused = True
        self._target_paused = self._paused
        self._simulated_now = get_loop_time().add(self._clock_offset)
        self._target_now = self._simulated_now
        self._clock_offset = zero()
        self._wake()

    def resume(self) -> None:
        """Moves this scheduler into realtime mode. This means that time will
        advance along with real time."""

        if not self._paused:
            return
        self._paused = False
        self._target_paused = self._paused
        self._clock_offset = self._simulated_now.subtract_time(get_loop_time())
        self._wake()

    def now(self) -> Time:
        """Gets this scheduler's current clock time."""
        if self._paused:
            return self._simulated_now
        else:
            return get_loop_time().add(self._clock_offset)

    def advance_sync(self, duration: Duration) -> None:
        """Synchronously Advances the clock by the given amount of time such
        that it will not be idle if there are future tasks queued within the
        advance window."""

        now = self.now()
        eventual_now = now + duration
        self.advance_to_sync(eventual_now)

    def advance_to_sync(self, eventual_now: Time) -> None:
        """Synchronously Advances the clock to the given future time such that
        it will not be idle if there are future tasks queued within the advance
        window."""

        if not self._paused:
            self.pause()
            self._target_paused = False
        if eventual_now <= self._simulated_now:
            self.resume()
            return
        self._target_now = eventual_now
        self._wake()

    async def advance(self, duration: Duration) -> None:
        """Advances the clock by the given amount of time."""

        await self.advance_to(self.now().add(duration))

    async def advance_to(self, eventual_now: Time) -> None:
        """Advances the clock to the specified time, if that time is in the
        future."""

        wait_task = self.schedule_at(eventual_now, lambda: None)
        self.advance_to_sync(eventual_now)
        await wait_task.wait()

    def schedule_in(self, duration: Duration, runnable: Runnable) -> ScheduledTask:
        return self.schedule_at(self.now().add(duration), runnable)

    def schedule_at(self, deadline: Time, runnable: Runnable) -> ScheduledTask:
        """Schedules the given runnable to run at the given deadline in this
        Scheduler's clock space."""
        task = ScheduledTask(deadline, self, runnable)
        for i, t in enumerate(self._queue):
            if t.deadline.__gt__(task.deadline):
                self._queue.insert(i, task)
                self._wake()
                return task
        self._queue.append(task)
        self._wake()
        return task

    def run(self, runnable: Runnable) -> ScheduledTask:
        """Runs the given runnable asynchronously, but soon."""

        # Schedule for 1 nanosec into the future, so if you call run() while
        # paused, time MUST be advanced for the task to run.
        return self.schedule_in(nanoseconds(1), runnable)

    def run_once_idle(self, runnable: Runnable) -> ScheduledTask:
        """Runs the given runnable once, when the scheduler otherwise is idle."""

        task = ScheduledTask(epoch(), self, runnable)
        self._idle_queue.append(task)
        self._wake()
        return task

    async def wait_until_idle(self) -> None:
        while self._runnable_task_count() > 0:
            task = self.run_once_idle(lambda: None)
            await task.wait()

    def cancel(self, task: ScheduledTask) -> CancelResult:
        """Cancels the given task, if scheduled."""

        if task not in self._queue:
            return CANCEL_RESULT_ALREADY_RAN

        self._queue.remove(task)
        return CANCEL_RESULT_CANCELED

    async def _sleep_until(self, deadline: Time, cancellation_event: Optional[Event] = None) -> bool:
        loop_deadline = self._to_loop_time(deadline)
        return await sleep_until(loop_deadline, cancellation_event)

    async def _sleep_for(self, delay: Duration, cancellation_event: Optional[Event] = None) -> bool:
        return await self._sleep_until(self.now().add(delay), cancellation_event)

    def _to_loop_time(self, scheduler_time: Time) -> Time:
        return scheduler_time.subtract(self._get_loop_offset())

    def _to_scheduler_time(self, loop_time: Time) -> Time:
        return loop_time.add(self._get_loop_offset())

    def _get_loop_offset(self) -> Duration:
        """Gets the scheduler time offset from loop time (the monotonic system
        clock), taking into account paused state.

        The loop offset will grow in real-time when paused, but will not move
        when not paused."""
        if self._paused:
            return self._simulated_now.subtract_time(get_loop_time())
        return self._clock_offset

    def _wake(self) -> None:
        if not self._started:
            return
        self._wakeup_event.set()

    def _runnable_task_count(self) -> int:
        now = self.now()
        if self._target_now > now:
            now = self._target_now
        count = 0
        for x in self._queue:
            if x.deadline <= now:
                count += 1
        return count

    async def _run_one(self) -> bool:
        self._wakeup_event.clear()
        due_tasks: list[ScheduledTask] = []
        now = self.now()
        while self._queue and self._queue[0].deadline <= now:
            due_tasks.append(self._queue.pop(0))

        if due_tasks:
            for task in due_tasks:
                await task.run()
            return False

        if self._target_now > now and self._paused:
            # We've been set to advance time.
            if self._queue:
                # Advance time to the next queued task that's before the
                # target time.
                task = self._queue[0]
                if task.deadline <= self._target_now:
                    next_now = task.deadline
                    if self._simulated_now < next_now:
                        self._simulated_now = next_now
                        return False

            # No tasks to advance to, so just advance the rest the of way.
            self._simulated_now = self._target_now
            if self._paused != self._target_paused:
                self.resume()
                return False

        if self._terminate:
            return True

        if self._idle_queue:
            idle_tasks = self._idle_queue
            self._idle_queue = []
            for task in idle_tasks:
                await task.run()
            return False

        next_wakeup_time = max_time()
        if not self._paused:
            if self._queue:
                deadline = self._queue[0].deadline
                if deadline < next_wakeup_time:
                    next_wakeup_time = deadline
        await self._sleep_until(next_wakeup_time, self._wakeup_event)
        return False

    async def _run(self) -> None:
        while not await self._run_one():
            pass
