from textwrap import fill


def html_escape(s: str) -> str:
    if not s:
        return s
    # TODO: Do this better...
    r = s.replace("&", "&amp;")
    r = r.replace("\"", "&quot;")
    r = r.replace("'", "&apos;")
    r = r.replace("<", "&lt;")
    r = r.replace(">", "&gt;")
    return r


def splitlines(s: str) -> list[str]:
    if not s:
        return []
    return s.split("\n")


def normalize_node(s: str) -> str:
    slen = len(s)
    leading_whitespace = " " if len(s.lstrip()) < slen else ""
    trailing_whitespace = " " if len(s.rstrip()) < slen else ""
    return f"{leading_whitespace}{normalize_words(s)}{trailing_whitespace}"


def words(s: str) -> list[str]:
    s = s.replace("\f", " ")
    s = s.replace("\n", " ")
    s = s.replace("\r", " ")
    s = s.replace("\t", " ")
    s = s.replace("\v", " ")
    return [word.strip() for word in s.strip().split(" ") if word]


def normalize_words(s: str) -> str:
    return " ".join(words(s))


def to_paragraphs(s: str) -> list[str]:
    lines = [line.strip() for line in splitlines(s.strip())]
    last_blank = True
    for index in range(0, len(lines)):
        if lines[index]:
            last_blank = False
        else:
            if last_blank:
                lines[index] = ""
            else:
                lines[index] = "\n"
            last_blank = True
    paragraphs = splitlines(" ".join(lines).strip())
    # One line per paragraph.
    paragraphs = [normalize_words(paragraph) for paragraph in paragraphs]
    # One wrapped paragraph per entry.
    paragraphs = [fill(paragraph, width=78) for paragraph in paragraphs]
    return paragraphs


def normalize_paragraphs(s: str) -> str:
    return "\n\n".join(to_paragraphs(s)).strip()


def sentence(s: str) -> str:
    old_line = " ".join([line.strip() for line in splitlines(s.strip())])
    line = ""
    line += old_line[0].upper()
    line += old_line[1:]
    if old_line[- 1] not in [".", "!", "?"]:
        line += "."
    return line


def text_list(items: list[str]) -> str:
    comma_emitted = False
    use_comma = True
    use_and = True
    count = len(items)
    if count == 1:
        use_and = False
    if count <= 2:
        use_comma = False

    result: list[str] = []
    for index, item in enumerate(items):
        if use_comma:
            if not comma_emitted:
                comma_emitted = True
            else:
                result.append(",")
        if use_and:
            if index == count - 1:
                result.append(" and")
        result.append(" ")
        result.append(item)
    return "".join(result)


def split_words(s: str) -> list[str]:
    s = s.replace(".", " . ")
    s = s.replace(";", " ; ")
    s = s.replace(",", " , ")
    s = normalize_words(s)
    return s.split()


_IRREGULAR_NOUN_TO_PLURAL = {
    "child": "children",
    "foot": "feet",
    "goose": "geese",
    "man": "men",
    "mouse": "mice",
    "person": "people",
    "tooth": "teeth",
    "woman": "women",
}


def pluralize(name: str) -> str:
    if name in _IRREGULAR_NOUN_TO_PLURAL:
        return _IRREGULAR_NOUN_TO_PLURAL[name]
    if name in ["sheep", "series", "species", "deer"]:
        return name
    if name.endswith("on"):
        return f"{name[:-2]}a"
    if name.endswith("is"):
        return f"{name[:-2]}es"
    if name.endswith("us"):
        return f"{name[:-2]}i"
    if name.endswith("o"):
        if name not in ["photo", "piano", "halo"]:
            return f"{name}es"
    if name.endswith("y"):
        if name[-2:-1] in ["b", "c", "d", "f", "g", "h", "j", "k", "l", "m",
                           "n", "p", "q", "r", "s", "t", "v", "w", "x", "y",
                           "z"]:
            return f"{name[:-1]}ies"
    if name.endswith(("f", "fe")):
        if name not in ["roof", "belief", "chief", "chef"]:
            last_f = name.rfind("f")
            return f"{name[:last_f]}ves"
    if name.endswith(("s", "ss", "sh", "ch", "x", "z")):
        if name.endswith("z"):
            return f"{name}zes"
        if name.endswith("s") and not name.endswith("ss"):
            return f"{name}ses"
        return f"{name}es"

    return f"{name}s"


def get_article(name: str) -> str:
    if name[0] in ["a", "e", "i", "o", "u"]:
        return "an"
    return "a"
