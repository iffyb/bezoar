from typing import Any, Iterable, Iterator, Mapping, Optional, Tuple

# This is not using `collections.abc.MutableMapping` due to an incompatibility
# with Transcrypt.


# This code was adapted from the standard library's collections.abc -- BEGIN

class _KeysView(object):
    def __init__(self, map: '_BaseMapping'):
        self._map = map

    def __contains__(self, key: str):
        return key in self._map

    def __iter__(self):
        yield from self._map


class _ItemsView(object):
    def __init__(self, map: '_BaseMapping'):
        self._map = map

    def __contains__(self, item: Tuple[str, Any]):
        key, value = item
        try:
            actual_value = self._map[key]
        except KeyError:
            return False

        return value is actual_value or value == actual_value

    def __iter__(self):
        yield from self._map


class _ValuesView(object):
    def __init__(self, map: '_BaseMapping'):
        self._map = map

    def __contains__(self, value: Any):
        for key in self._map:
            v = self._map[key]
            if v is value or v == value:
                return True
        return False

    def __iter__(self):
        yield from self._map


class _Marker(object):
    pass


_marker = _Marker()


class _BaseMapping(object):
    def __iter__(self) -> Iterator[str]:
        raise NotImplementedError

    def __len__(self) -> int:
        raise NotImplementedError

    def __contains__(self, key: str) -> bool:
        raise NotImplementedError

    def __getitem__(self, key: str) -> Any:
        raise NotImplementedError

    def __setitem__(self, key: str, value: Any) -> None:
        raise NotImplementedError

    def __delitem__(self, key: str) -> None:
        raise NotImplementedError

    def get(self, key: str, default: Optional[Any] = None):
        if key in self:
            value = self.__getitem__(key)
            return value
        return default

    def keys(self) -> Iterable[str]:
        return _KeysView(self)

    def items(self) -> Iterable[Tuple[str, Any]]:
        return _ItemsView(self)

    def values(self) -> Iterable[Any]:
        return _ValuesView(self)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Mapping):
            return NotImplemented
        return dict(self.items()) == dict(other.items())

    def pop(self, key: str, default: Any = _marker) -> Any:
        if key in self:
            value = self.__getitem__(key)
            self.__delitem__(key)
            return value

        if default is _marker:
            raise
        return default

    def popitem(self) -> Tuple[str, Any]:
        try:
            key = next(iter(self))
        except StopIteration:
            raise KeyError from None
        value = self.__getitem__(key)
        self.__delitem__(key)
        return key, value

    def clear(self) -> None:
        try:
            while self.__len__() > 0:
                self.popitem()
        except KeyError:
            pass

    def update(self, other=(), /, **kwds) -> None:
        if isinstance(other, Mapping):
            for key in other:
                self.__setitem__(key, other[key])
        elif hasattr(other, "keys"):
            for key in other.keys():
                self.__setitem__(key, other[key])
        else:
            for key, value in other:
                self.__setitem__(key, value)
        for key, value in kwds.items():
            self.__setitem__(key, value)

    def setdefault(self, key: str, default: Optional[Any] = None) -> Optional[Any]:
        if key in self:
            return self.__getitem__(key)

        if default is None:
            return None

        self[key] = default
        return default

# This code was adapted from the standard library's collections.abc -- END


class OverlayMapping(_BaseMapping):
    """A dict that overlays the values in a Sequence of other backing Mappings.

    When a value is not set in the top-level dict, it searches the backing
    Mappings for a value.

    If a value is set in the top-level dict, it returns that value immediately.

    If a value is deleted, it retains that key and ensures the value is not
    returned, even if it is in a backing Mapping.

    If the value set is the same as the value that would be returned from the
    backing mappings, it will just remove the value from the top-level dict
    and leave it to be returned by the backing mapping.

    This mapping does not mutate the backing mappings, but assumes they can
    mutate independently. So, a len() operation is actually O(keys + mappings),
    since it has to count the unique keys in all mappings, which may overlap.
    set() and get() operations are O(mappings).

    """

    def __init__(self, *maps: Mapping[str, Any]) -> None:
        super().__init__()
        self._maps = list(maps)
        self._map: dict[str, Any] = {}
        self._deletedKeys: set[str] = set({})

    def _get_backing(self, k: str) -> Optional[Any]:
        for m in self._maps:
            if k in m:
                return m[k]
        return None

    def _key_set(self) -> set[str]:
        # Have to do this the hard way, because keys may overlap, and underlying
        # mappings may have changed.
        keyset: set[str] = set()
        keyset.update(self._map.keys())
        for m in self._maps:
            keyset.update(m.keys())
        return keyset.difference(self._deletedKeys)

    def __iter__(self) -> Iterator[str]:
        return iter(self._key_set())

    def __len__(self) -> int:
        return len(self._key_set())

    def __contains__(self, key: str) -> bool:
        return key in self._key_set()

    def __getitem__(self, k: str) -> Any:
        if k in self._deletedKeys:
            raise KeyError(k)
        if k in self._map:
            return self._map[k]
        result = self._get_backing(k)
        if result is None:
            raise KeyError(k)
        return result

    def __setitem__(self, k: str, v: Any) -> None:
        self._deletedKeys.discard(k)
        if k in self._map:
            del self._map[k]
        result = self._get_backing(k)
        if result is v:
            return
        self._map.__setitem__(k, v)

    def __delitem__(self, k: str) -> None:
        self._deletedKeys.add(k)
        if k in self._map:
            del self._map[k]
