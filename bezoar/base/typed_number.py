class TypedNumber(object):
    """
    A simplified enum substitute that works with Transcrypt.
    """

    def __init__(self, value: int, name:str = "") -> None:
        super().__init__()
        self.value = value
        self.name = name

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, TypedNumber):
            raise TypeError(f"Not a TypedNumber: {o}")
        return self.value == o.value

    def __ne__(self, o: object) -> bool:
        return not self.__eq__(o)

    def __le__(self, o: object) -> bool:
        if not isinstance(o, TypedNumber):
            raise TypeError(f"Not a TypedNumber: {o}")
        return self.value <= o.value

    def __lt__(self, o: object) -> bool:
        if not isinstance(o, TypedNumber):
            raise TypeError(f"Not a TypedNumber: {o}")
        return self.value < o.value

    def __gt__(self, o: object) -> bool:
        return not (self <= o)

    def __ge__(self, o: object) -> bool:
        return not (self < o)

    def __hash__(self) -> int:
        return self.value

    def __str__(self) -> str:
        return f"<{self.__class__.__name__} {self.name}({self.value})>"

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}({self.value}, {repr(self.name)})"
