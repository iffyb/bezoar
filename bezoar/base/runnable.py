from typing import Any, Callable, Coroutine, Union

Closure = Callable[[], None]
AsyncClosure = Callable[[], Coroutine[None, None, Any]]
Runnable = Union[Closure, AsyncClosure]
