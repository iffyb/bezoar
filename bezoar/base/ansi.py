ESC = "\u001B"
CURSOR_HOME = ESC + "[H"


def cursor_to(column: int, line: int) -> str:
    return f"{ESC}[{line};{column}H"


def cursor_up(lines: int) -> str:
    return f"{ESC}[{lines}A"


def cursor_down(lines: int) -> str:
    return f"{ESC}[{lines}B"


def cursor_right(columns: int) -> str:
    return f"{ESC}[{columns}C"


def cursor_left(columns: int) -> str:
    return f"{ESC}[{columns}D"


CURSOR_UP = cursor_up(1)
CURSOR_DOWN = cursor_down(1)
CURSOR_LEFT = cursor_left(1)
CURSOR_RIGHT = cursor_right(1)

CLEAR_SCREEN = ESC + "[2J"
CLEAR_TO_EOL = ESC + "[0K"
CLEAR_TO_BOL = ESC + "[1K"
CLEAR_LINE = ESC + "[2K"

RESET = ESC + "[0m"
BOLD = ESC + "[1m"
DIM = ESC + "[2m"
ITALIC = ESC + "[3m"
UNDERLINE = ESC + "[4m"

BLACK = ESC + "[30m"
RED = ESC + "[31m"
GREEN = ESC + "[32m"
YELLOW = ESC + "[33m"
BLUE = ESC + "[34m"
MAGENTA = ESC + "[35m"
CYAN = ESC + "[36m"
WHITE = ESC + "[37m"

BG_BLACK = ESC + "[40m"
BG_RED = ESC + "[41m"
BG_GREEN = ESC + "[42m"
BG_YELLOW = ESC + "[43m"
BG_BLUE = ESC + "[44m"
BG_MAGENTA = ESC + "[45m"
BG_CYAN = ESC + "[46m"
BG_WHITE = ESC + "[47m"


def to_color(red: int, green: int, blue: int) -> str:
    return f"{ESC}[38;2;{red};{green};{blue}m"


def to_bg_color(red: int, green: int, blue: int) -> str:
    return f"{ESC}[48;2;{red};{green};{blue}m"
