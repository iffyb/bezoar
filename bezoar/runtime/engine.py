from __future__ import annotations

from typing import Callable

from bezoar._model.actions import dispatch_action
from bezoar._model.entities import get_player
from bezoar.base.scheduler import ScheduledTask
from bezoar.base.text import normalize_words
from bezoar.mddom import Nodeish
from bezoar.model import (SCHEDULER_GAME, Emitter, all_entities,
                          all_entity_data, get_actions, get_scheduler,
                          get_version, initialize_model, set_actor)
from bezoar.runtime.parser import compile_grammar, parse


def _no_emitter(x: Nodeish) -> None:
    pass


def _no_stopper() -> None:
    pass


Stopper = Callable[[], None]

_emit: Emitter = _no_emitter
_stop: Stopper = _no_stopper


async def start(emitter: Emitter, stopper: Stopper) -> None:
    get_actions()
    await initialize_model()
    compile_grammar()
    global _emit
    _emit = emitter
    global _stop
    _stop = stopper
    player = get_player()
    if player is None:
        _emit("You are no one! <This indicates a bug.>")
        return
    player.emitter = _emit
    set_actor(player)
    get_version().tell(player)

    # TODO: Do this better.
    await submit_command_async("look")

def _submit_command_task(command: str) -> None:
    player = get_player()

    if player is None:
        _emit("You are no one! <This indicates a bug.>")
        return
    actor = player
    set_actor(actor)
    command = normalize_words(command)

    if command in ["q", "quit"]:
        actor.tell("Bye!")
        _stop()
        return

    if command in ["dump"]:
        _emit("STATIC DATA:")
        for ed in all_entity_data():
            _emit(str(ed))
        _emit("DYNAMIC DATA:")
        for entities in all_entities():
            for e in entities:
                _emit(str(e))
        return

    result = parse(player, command)
    dispatch_action(result)

    def action_ready() -> None:
        actor.handle_action_ready()

    scheduler = get_scheduler(SCHEDULER_GAME)
    scheduler.run_once_idle(action_ready)

def submit_command(command: str) -> ScheduledTask:
    scheduler = get_scheduler(SCHEDULER_GAME)
    def task() -> None:
        _submit_command_task(command)
    scheduler.schedule_at(scheduler.now(), task)
    return scheduler.run_once_idle(lambda: None)

async def submit_command_async(command: str) -> None:
    await submit_command(command).wait()
