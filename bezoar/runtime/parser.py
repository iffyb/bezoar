"""
The Parser.

See [parser.md](parser.md).
"""

from __future__ import annotations

from typing import Mapping, Optional, Sequence, Tuple, TypeVar, Union

from bezoar.base.itertools import first
from bezoar.base.text import split_words
from bezoar.base.typed_number import TypedNumber
from bezoar.model import (Action, Entity, ParseResult, Restriction,
                          add_reset_hook, all_entities, get_actions,
                          get_direction_synonym, get_synonym, get_verb_synonym)

_DEBUG_PARSER = False

# These are the canonical direction forms that you should refer to when
# declaring exits. Standard synonyms will automatically be applied.
_CANONICAL_DIRECTIONS = {
    "aft",
    "down",
    "east",
    "fore",
    "in",
    "north",
    "northeast",
    "northwest",
    "out",
    "port",
    "south",
    "southeast",
    "southwest",
    "starboard",
    "up",
    "west",
}


# These are the prepositions that you should refer to when declaring action
# syntax. These were chosen by the criteria of being short, one word,
# and common in contemporary English usage.
_CANONICAL_PREPOSITIONS = {
    "about",
    "above",
    "across",
    "after",
    "around",
    "at",
    "away from",
    "before",
    "behind",
    "between",
    "from",
    "in front of",
    "in",
    "near",
    "on",
    "opposite",
    "out",
    "over",
    "to",
    "towards",
    "under",
    "with",
}


# Universal synonyms to canonical prepositions.
_PREPOSITION_SYNONYMS = {
    "across from": "opposite",
    "beside": "near",
    "close to": "near",
    "inside": "in",
    "into": "in",
    "next to": "near",
    "on top of": "on",
    "onto": "on",
    "on to": "on",
    "outside of": "out",
    "regarding": "about",
}


# Words that may indicate the beginning of a new sentence.
_SENTENCE_TERMINATORS = {
    ",",
    ";",
    ".",
    "and",
    "then",
}


# Articles that might be used with nouns to indicate specificity.
_ARTICLES = {
    "a",
    "an",
    "the",
}


# Nouns that the parser will have to figure out what is being referred to.
_PRONOUNS = {
    "her",
    "him",
    "it",
    "them",
}


# Ways for the player to to refer to themselves. We leave it ambiguous whether
# the player should use the first or second person to refer to themselves.
_SELF = {
    "me",
    "myself",
    "self",
    "you",
    "yourself",
}


Word = str
Sentence = list[Word]


class Flag(TypedNumber):
    """
    Bits of the type of word a word could be. Some words can be used in multiple
    ways, so may have multiple bits set.
    """

    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


FLAG_ADJECTIVE = Flag(1, "ADJECTIVE")
FLAG_ALL = Flag(2, "ALL")
FLAG_AND = Flag(3, "AND")
FLAG_ANYTHING = Flag(4, "ANYTHING")
FLAG_ARTICLE = Flag(5, "ARTICLE")
FLAG_BUT = Flag(6, "BUT")
FLAG_COMMA = Flag(7, "COMMA")
FLAG_DEFINITE_ARTICLE = Flag(8, "DEFINITE_ARTICLE")
FLAG_DIRECTION = Flag(9, "DIRECTION")
FLAG_EVERYTHING = Flag(10, "EVERYTHING")
FLAG_EXCEPT = Flag(11, "EXCEPT")
FLAG_NOT = Flag(12, "NOT")
FLAG_NOUN = Flag(13, "NOUN")
FLAG_PLURAL = Flag(14, "PLURAL")
FLAG_PREPOSITION = Flag(15, "PREPOSITION")
FLAG_TERMINATOR = Flag(16, "TERMINATOR")
FLAG_VERB = Flag(17, "VERB")


Flags = set[Flag]


class Token(object):
    def __init__(self, word: Word, flags: Flags = None) -> None:
        super().__init__()
        self.word = word
        if flags:
            self.flags = flags
        else:
            self.flags = set()

    def __eq__(self, o: object) -> bool:
        if isinstance(o, Token):
            return o.word == self.word
        return super().__eq__(o)

    def __lt__(self, o: object) -> bool:
        if not isinstance(o, Token):
            raise TypeError()
        return self.word < o.word

    def __hash__(self) -> int:
        return hash(self.word)

    def to_string(self) -> str:
        return f"(Token \"{self.word}\", flags={self.flags})"

    def __repr__(self) -> str:
        return self.to_string()

    def __str__(self) -> str:
        return self.to_string()


Dictionary = dict[Word, Token]
Tokens = Sequence[Token]
Entities = list[Entity]
EntityAndTokens = Tuple[Optional[Entity], Tokens]
EntitiesAndTokens = Tuple[Optional[Entities], Tokens]


T = TypeVar('T')


def _first_index_of(sequence: Sequence[T], value: T,
                    start: int = 0, stop: int = -1) -> int:
    try:
        return sequence.index(value, start, stop)
    except ValueError:
        return -1


class _Reference(object):
    def __init__(self, specification: str) -> None:
        specification = specification.replace("{", "").replace("}", "")
        self.specification = specification
        parts = specification.split(':')
        prefix = parts[0].strip()
        parts = parts[1:]
        self.is_multiple = False
        if prefix[-1] == '+':
            self.is_multiple = True
            prefix = prefix[0:-1].strip()

        self.name = prefix
        restriction_spec = parts[0].strip() if parts else ""
        self.restriction = Restriction(restriction_spec)
        self.is_direction = self.name == "direction"

    def __str__(self) -> str:
        return (f"(_Reference \"{self.name}\" M:{self.is_multiple} " +
                f"D:{self.is_direction} {self.restriction})")

    def __hash__(self) -> int:
        return hash(str(self))

    def __eq__(self, rhs: object) -> bool:
        if not isinstance(rhs, _Reference):
            return False
        return (self.name == rhs.name and
                self.is_multiple == rhs.is_multiple and
                self.restriction == rhs.restriction)


def _count(firstval=0, step=1):
    x = firstval
    while 1:
        yield x
        x += step


_COUNTER = _count()


class GrammarTrie(object):
    def __init__(self) -> None:
        super().__init__()
        self._root = GrammarTrie.Node()
        self._dictionary: Dictionary = {}

    class Node(object):
        def __init__(self, term: Union[Token, _Reference, None] = None) -> None:
            super().__init__()
            self.children: dict[str, GrammarTrie.Node] = {}
            self.references: dict[_Reference, GrammarTrie.Node] = {}
            self.action: Optional[Action] = None
            self.could_be_objref = False
            self.could_be_multiple_objrefs = False
            self.could_be_direction = False
            self.could_be_verb = False
            self.reference: Optional[_Reference] = None
            self.term: Optional[Token] = None
            self.direction: Optional[GrammarTrie.Node] = None

            if isinstance(term, Token):
                self.term = term
            elif isinstance(term, _Reference):
                self.reference = term

        def to_string(self, indent: str = "") -> str:
            next_indent = indent + "  "
            result = f"{indent}Term: {self.term or self.reference}"
            if self.action:
                result += " " + str(self.action)
            result += "\n"
            for word in sorted(self.children.keys()):
                child = self.children[word]
                result += child.to_string(next_indent)
            if self.direction:
                result += self.direction.to_string(next_indent)
            for reference in self.references.keys():
                child = self.references[reference]
                result += child.to_string(next_indent)
            return result

        def preprocess(self, grammar: GrammarTrie) -> None:
            if self.direction:
                self.could_be_direction = True
            self.could_be_objref = len(self.references) > 0
            for reference in self.references.keys():
                if reference.is_multiple:
                    self.could_be_multiple_objrefs = True
            for child in self.children.values():
                child_token = child.term
                if (child_token is not None and FLAG_VERB in child_token.flags):
                    self.could_be_verb = True
                    break
            for child in self.children.values():
                child.preprocess(grammar)
            for child in self.references.values():
                child.preprocess(grammar)
            if self.direction:
                self.direction.preprocess(grammar)

        def __str__(self) -> str:
            return self.to_string()

    def __str__(self) -> str:
        return str(self._root)

    def _set_flag(self, word: Word, flag: Flag) -> None:
        token = self._tokenize(word, True)
        token.flags.add(flag)

    def _tokenize(self, word: Word, ensure: bool = False) -> Token:
        if word.startswith("{"):
            raise ValueError(word)
        word = word.strip().lower()
        if word not in self._dictionary:
            token = Token(word, set())
            if ensure:
                self._dictionary[word] = token
            return token

        token = self._dictionary[word]
        return token

    def add_action(self, act: Action) -> None:
        for syntax in act.syntaxes:
            specification_string = syntax.replace("[", " [ ").replace("]", " ] ").replace(
                "{", " { ").replace("}", " } ").replace(":", " : ")
            unmerged_specification = specification_string.split()

            # Merge braced expressions into single terms.
            specification: list[str] = []
            bracing = False
            brace_term: list[str] = []
            for term in unmerged_specification:
                if bracing:
                    if term == "{":
                        raise ValueError("Specification has double open braces: "
                                         f"{specification_string}")
                    if term == "}":
                        bracing = False
                        specification.append("{" + " ".join(brace_term) + "}")
                        brace_term = []
                        continue
                    brace_term.append(term)
                    continue
                if term == "{":
                    bracing = True
                    continue

                specification.append(term)

            if bracing:
                raise ValueError("Specification has unterminated open brace: "
                                 f"{specification_string}")

            verb = specification[0]

            # Kind of a special case for {direction}.
            if verb.startswith("{"):
                verb = ""

            # Find a bracketed term.
            bracket_start = _first_index_of(specification, "[")
            if bracket_start >= 0:
                bracket_end = _first_index_of(
                    specification, "]", bracket_start + 1)
                if bracket_end < 0:
                    raise ValueError("Specification has unbalanced brackets: "
                                     f"{specification_string}")
                if bracket_end - bracket_start != 2:
                    raise ValueError("Brackets wrap more than a single term: "
                                     f"{specification_string}")
                verb = specification[bracket_start + 1]
                specification.pop(bracket_end)
                specification.pop(bracket_start)
                if _first_index_of(specification, "[") != -1:
                    raise ValueError("More than one open bracket: "
                                     f"{specification_string}")
                if _first_index_of(specification, "]") != -1:
                    raise ValueError("More than one close bracket: "
                                     f"{specification_string}")

            if verb:
                self._set_flag(verb, FLAG_VERB)

            current = self._root
            for term in specification:
                if term.startswith("{"):
                    reference = _Reference(term)
                    if reference.is_direction:
                        current.direction = GrammarTrie.Node(reference)
                        current = current.direction
                    else:
                        if reference not in current.references:
                            current.references[reference] = GrammarTrie.Node(
                                reference)
                        current = current.references[reference]
                else:
                    token = self._tokenize(term, True)
                    if term != verb:
                        token.flags.add(FLAG_PREPOSITION)
                    if token.word not in current.children:
                        current.children[token.word] = GrammarTrie.Node(token)
                    current = current.children[token.word]

            if current.action is None:
                current.action = act
            else:
                raise ValueError("A syntax maps to multiple actions: "
                                 f"{specification_string}")

    def _parse_object_reference(self, actor: Entity, tokens: Sequence[Token],
                                possibilities: list[Entity]) -> EntitiesAndTokens:
        # TODO: Support multi-word adjectives.
        # TODO: Support multi-word nouns. e.g. "Science and Technology Magazine"

        if not tokens:
            return None, tokens

        current = tokens

        # Maybe a determiner?
        if FLAG_ARTICLE in current[0].flags:
            current = current[1:]

        # Adjective or noun.
        adjective: Optional[str] = None
        if current and FLAG_ADJECTIVE in current[0].flags:
            adjective = current[0].word
            current = current[1:]

        # Must be a noun.
        noun: Optional[str] = None
        if current and FLAG_NOUN in current[0].flags:
            noun = current[0].word
            current = current[1:]

        entity_set: set[Entity] = set()
        entities: list[Entity] = list()
        for e in possibilities:
            if e.matches(adjective, noun) and e not in entity_set:
                entities.append(e)
                entity_set.add(e)
        if entities:
            return entities, current

        # If not, back up.
        return None, tokens

    def _parse_objects_reference(self, actor: Entity, tokens: Sequence[Token],
                                 possibilities: list[Entity]) -> EntitiesAndTokens:
        # TODO: Support multiple objects with and/comma.
        # TODO: Support wildcards.
        # TODO: Support plurals.
        # TODO: Support inference when only one possibility.
        # TODO: Support ambiguity resolution.
        current = tokens
        entities: Entities = []
        while current:
            entity, remainder = self._parse_object_reference(
                actor, current, possibilities)
            if entity is None:
                return entities, remainder
            for e in entity:
                if e not in entities:
                    entities.append(e)
            current = remainder
            if current:
                if FLAG_AND in current[0].flags or FLAG_COMMA in current[0].flags:
                    current = current[1:]
                else:
                    break

        return entities, current

    class Parser(object):
        def __init__(self, grammar: GrammarTrie, actor: Entity,
                     current: GrammarTrie.Node, tokens: Sequence[Token]) -> None:
            self.counter = next(_COUNTER)
            self.actor = actor
            self.grammar = grammar
            self.current = current
            while tokens and FLAG_TERMINATOR in tokens[-1].flags:
                tokens = tokens[:-1]
            self._tokens = tokens

            self._subparsers: list[GrammarTrie.Parser] = []
            self._running = True
            self._error = ""
            self._delegate: Optional[GrammarTrie.Parser] = None
            self._objects: dict[str, Entities] = {}
            self.direction = ""

        def clone(self) -> GrammarTrie.Parser:
            other = GrammarTrie.Parser(
                self.grammar, self.actor, self.current, self._tokens)
            for key in self._objects.keys():
                other._objects[key] = []
                other._objects[key].extend(self._objects[key])
            return other

        @property
        def tokens(self) -> Sequence[Token]:
            if self._delegate:
                return self._delegate.tokens
            if self._subparsers:
                return []
            return self._tokens

        @property
        def running(self) -> bool:
            if self._delegate:
                return self._delegate.running

            if self._subparsers:
                running = False
                for s in self._subparsers:
                    running |= s.running
                return running

            return self._running

        @property
        def action(self) -> Optional[Action]:
            if self.running:
                return None
            if self._delegate:
                return self._delegate.action
            return self.current.action

        @property
        def objects(self) -> Mapping[str, Entities]:
            if self.running:
                return {}
            if self._delegate:
                return self._delegate.objects
            return self._objects

        @property
        def error(self) -> str:
            if self.running:
                return ""
            if self._delegate:
                return self._delegate.error
            return self._error

        def _set_error(self, message: str) -> None:
            if _DEBUG_PARSER:
                print(f"_set_error(\"{message}\")")
            if self._error:
                raise Exception()
            if not self.running:
                raise Exception()
            if not message:
                message = "ErRoR"
            self._error = message
            self._running = False
            self._subparsers = []
            self._delegate = None

        def __str__(self) -> str:
            s = f"Parser ({self.counter}) -- "
            s += f"R:{self.running}"
            if self.tokens:
                s += f", T:{self.tokens}"
            if self.error:
                s += f", E:{self.error}"
            if self.objects:
                s += f", O:{self.objects}"
            if self.action:
                s += f", A:{self.action}"
            return s

        def process_token(self) -> None:
            if self._subparsers:
                running: list[GrammarTrie.Parser] = []
                errors: list[GrammarTrie.Parser] = []
                candidates: list[GrammarTrie.Parser] = []

                for sub in self._subparsers:
                    sub.process_token()
                    if sub.running:
                        running.append(sub)
                    elif sub.error:
                        errors.append(sub)
                    else:
                        candidates.append(sub)

                if running:
                    return

                if len(candidates) == 1:
                    self._delegate = candidates[0]
                elif not candidates and errors:
                    self._delegate = errors[0]
                else:
                    self._set_error(
                        f"Candidates: {len(candidates)}, Errors: {len(errors)}")
                return

            if not self.running:
                return

            if not self._tokens:
                self._running = False
                if _DEBUG_PARSER:
                    print(self)
                return

            if _DEBUG_PARSER:
                print(self)

            token = self._tokens[0]
            synonym = get_synonym(token.word)
            if synonym is not None:
                resolved_token = self.grammar._tokenize(synonym)
            else:
                resolved_token = token

            if self.current.could_be_direction:
                direction_token = self.grammar._tokenize(
                    get_direction_synonym(token.word))
                if FLAG_DIRECTION in direction_token.flags:
                    self.direction = direction_token.word
                    if self.current.direction:
                        self.current = self.current.direction
                    self._tokens = self._tokens[1:]
                    return

            if self.current.could_be_verb:
                resolved_token = self.grammar._tokenize(
                    get_verb_synonym(token.word))

            if resolved_token.word in self.current.children:
                self.current = self.current.children[resolved_token.word]
                self._tokens = self._tokens[1:]

                if not self._tokens:
                    self._running = False
                return

            # At this point, if it can be an object reference, it must be,
            # because we've exhausted all other options.
            if self.current.could_be_objref:
                # TODO: reparse for all references?
                first_reference = first(self.current.references.keys())
                if not first_reference:
                    raise Exception()
                entity_possibilities = list(first_reference.restriction.find_possibilities(
                    self.actor))
                if self.current.could_be_multiple_objrefs:
                    entities, remainder = self.grammar._parse_objects_reference(
                        self.actor, self._tokens, entity_possibilities)
                else:
                    entities, remainder = self.grammar._parse_object_reference(
                        self.actor, self._tokens, entity_possibilities)
                if not entities:
                    self._set_error(
                        f"No objects found of {len(entity_possibilities)}")
                    return
                self._tokens = remainder
                possibilities: list[_Reference] = []
                multiple = len(entities) > 1
                for reference in self.current.references.keys():
                    if multiple:
                        if reference.is_multiple:
                            possibilities.append(reference)
                    else:
                        possibilities.append(reference)
                if not possibilities:
                    self._set_error("No possibilities")
                    return
                if len(possibilities) == 1:
                    name = possibilities[0].name
                    self._objects[name] = entities
                    self.current = self.current.references[reference]
                else:
                    for reference in possibilities:
                        sub = self.clone()
                        name = reference.name
                        self._subparsers.append(sub)
                        sub._objects[name] = entities
                        sub.current = sub.current.references[reference]
                        if _DEBUG_PARSER:
                            print(f"SPLIT: {sub}")
                return

            if not resolved_token.flags:
                self._set_error(f"Unrecognized word \"{token.word}\"")
                return

            self._set_error(f"Stuck at \"{token.word}\"")
            return

    def parse(self, actor: Entity, submission: str) -> Optional[ParseResult]:
        tokens = [self._tokenize(word) for word in split_words(submission)]
        parser = GrammarTrie.Parser(self, actor, self._root, tokens)

        while parser.running:
            parser.process_token()

        if parser.error:
            if _DEBUG_PARSER:
                print(f"ERROR: {parser.error}")
            actor.tell(parser.error)
            return None

        if parser.tokens:
            actor.tell(f"Didn't consume all of: \"{submission}\".")
            return None

        if parser.action:
            if _DEBUG_PARSER:
                print(f"Action: {parser.action}")
            return ParseResult(parser.action, actor, parser.objects, parser.direction)

        actor.tell(f"No action associated with: \"{submission}\".")
        return None

    def compile_grammar(self) -> None:
        self._set_flag("all", FLAG_ALL)
        self._set_flag("and", FLAG_AND)
        self._set_flag("but", FLAG_BUT)
        self._set_flag("everything", FLAG_EVERYTHING)
        self._set_flag("except", FLAG_EXCEPT)
        self._set_flag("not", FLAG_NOT)
        self._set_flag("the", FLAG_DEFINITE_ARTICLE)
        self._set_flag(",", FLAG_COMMA)
        for article in _ARTICLES:
            self._set_flag(article, FLAG_ARTICLE)
        for pronoun in _PRONOUNS:
            self._set_flag(pronoun, FLAG_NOUN)
        for direction in _CANONICAL_DIRECTIONS:
            self._set_flag(direction, FLAG_DIRECTION)
        for terminator in _SENTENCE_TERMINATORS:
            self._set_flag(terminator, FLAG_TERMINATOR)
        for self_reference in _SELF:
            self._set_flag(self_reference, FLAG_NOUN)
        for el in all_entities():
            for e in el:
                for n in e.nouns:
                    self._set_flag(n, FLAG_NOUN)
                for a in e.adjectives:
                    self._set_flag(a, FLAG_ADJECTIVE)
                for p in e.plurals:
                    self._set_flag(p, FLAG_PLURAL)
        for act in get_actions():
            self.add_action(act)
        self._root.preprocess(self)
        if _DEBUG_PARSER:
            print(self._root)


_grammar_trie = GrammarTrie()


def compile_grammar() -> None:
    _grammar_trie.compile_grammar()


def parse(actor: Entity, submission: str) -> Optional[ParseResult]:
    return _grammar_trie.parse(actor, submission)


def _reset() -> None:
    global _grammar_trie
    _grammar_trie = GrammarTrie()


add_reset_hook(_reset)
