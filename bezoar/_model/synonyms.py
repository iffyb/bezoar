from __future__ import annotations

from typing import Mapping

from bezoar._model.resetting import add_reset_hook

_synonyms: dict[str, str] = {}
_direction_synonyms: dict[str, str] = {}
_verb_synonyms: dict[str, str] = {}


def synonym(from_word: str, to_word: str) -> None:
    _synonyms[from_word] = to_word


def synonyms(nyms: Mapping[str, str]) -> None:
    _synonyms.update(nyms)


def direction_synonym(from_word: str, to_word: str) -> None:
    _direction_synonyms[from_word] = to_word


def direction_synonyms(nyms: Mapping[str, str]) -> None:
    _direction_synonyms.update(nyms)


def verb_synonym(from_word: str, to_word: str) -> None:
    _verb_synonyms[from_word] = to_word


def verb_synonyms(nyms: Mapping[str, str]) -> None:
    _verb_synonyms.update(nyms)


def clear_synonyms() -> None:
    global _synonyms
    _synonyms = {}
    global _direction_synonyms
    _direction_synonyms = {}
    global _verb_synonyms
    _verb_synonyms = {}


def get_synonym(nym: str) -> str:
    if nym not in _synonyms:
        return nym
    return _synonyms[nym]


def get_direction_synonym(nym: str) -> str:
    if nym not in _direction_synonyms:
        return nym
    return _direction_synonyms[nym]


def get_verb_synonym(nym: str) -> str:
    if nym not in _verb_synonyms:
        return nym
    return _verb_synonyms[nym]


add_reset_hook(clear_synonyms)
