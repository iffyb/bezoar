from __future__ import annotations

from typing import Callable

ResetHook = Callable[[], None]

_reset_hooks: list[ResetHook] = []


def add_reset_hook(hook: ResetHook) -> None:
    _reset_hooks.append(hook)


def reset() -> None:
    for hook in _reset_hooks:
        hook()
