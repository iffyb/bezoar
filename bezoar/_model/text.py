from __future__ import annotations

from typing import TYPE_CHECKING, Optional
from bezoar.base.itertools import first

from bezoar.base.text import get_article
from bezoar.mddom import ContentSpanOrString, Span, sp

if TYPE_CHECKING:
    from bezoar._model.entities import Entity


def ref(referrant: 'Entity', *args: Optional[ContentSpanOrString]) -> Span:
    result = sp(*args)
    result.referrant = referrant.counter
    return result


def a(self: 'Entity') -> Span:
    name = get_name(self)
    return sp(get_article(name), " ", ref(self, name))


def the(self: 'Entity') -> Span:
    name = get_name(self)
    return sp("the ", ref(self, name))


def get_name(self: 'Entity') -> str:
    if self.adjectives:
        name = (first(self.adjectives) or "") + " " + self.noun
        return name.strip()
    return self.noun
