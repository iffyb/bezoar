from __future__ import annotations

from typing import Iterable, Optional, Tuple

from bezoar._model.entities import (Entity, EntityReferenceFilter,
                                    EntityReferenceSearch, all_entities,
                                    get_entity_filter, get_entity_search,
                                    is_entity_filter, is_entity_search)
from bezoar.base.typed_number import TypedNumber


class _Type(TypedNumber):
    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


_TYPE_SEARCH = _Type(0, "SEARCH")
_TYPE_HYBRID_SEARCH = _Type(1, "HYBRID_SEARCH")
_TYPE_HYBRID_FILTER = _Type(2, "HYBRID_FILTER")
_TYPE_FILTER = _Type(3, "FILTER")


class _RestrictionNode(object):
    def __init__(self, *children: _RestrictionNode) -> None:
        self._children = list(children)
        self._children.sort(key=lambda x: x.type)
        if not self._children:
            self.type = _TYPE_FILTER
            return

        self._searches: list[_RestrictionNode] = []
        self._hybrid_searches: list[_RestrictionNode] = []
        self._hybrid_filters: list[_RestrictionNode] = []
        self._filters: list[_RestrictionNode] = []
        current_type = self._children[0].type
        for c in self._children:
            if c.type == _TYPE_SEARCH:
                self._searches.append(c)
                if current_type == _TYPE_HYBRID_FILTER:
                    current_type = _TYPE_HYBRID_SEARCH
                elif current_type == _TYPE_FILTER:
                    current_type = _TYPE_HYBRID_SEARCH
            elif c.type == _TYPE_HYBRID_SEARCH:
                self._hybrid_searches.append(c)
            elif c.type == _TYPE_HYBRID_FILTER:
                self._hybrid_filters.append(c)
                if current_type == _TYPE_SEARCH:
                    current_type = _TYPE_HYBRID_SEARCH
                elif current_type == _TYPE_FILTER:
                    current_type = _TYPE_HYBRID_FILTER
            elif c.type == _TYPE_FILTER:
                self._filters.append(c)
                if current_type == _TYPE_SEARCH:
                    current_type = _TYPE_HYBRID_SEARCH

        self.type = current_type

    @property
    def prefers_filter(self) -> bool:
        return self.type in [_TYPE_FILTER, _TYPE_HYBRID_FILTER]

    def search(self, source: Entity) -> Iterable[Entity]:
        # If no search is defined, run filter on all known entities. This should
        # be avoided, if possible.
        print("WARNING: Searching all entities.")
        return self.filter(source, self._all_entities())

    def filter(self, source: Entity, targets: Iterable[Entity]) -> Iterable[Entity]:
        if not self._children:
            return targets

        # Convert from search to filter, at some cost. This may or may not be
        # avoidable, depending on the circumstances.
        ins = set(self.search(source))
        return [t for t in targets if t in ins]

    def to_string(self, indent: str = "") -> str:
        return f"{indent}{self.__class__.__name__}"

    def to_string_children(self, indent: str = "") -> str:
        next_indent = indent + "  "
        result = self.to_string(indent) + "\n"
        for child in self._children:
            result += child.to_string_children(next_indent)
        return result

    def __str__(self) -> str:
        return self.to_string()

    def __repr__(self) -> str:
        return self.to_string()

    def _all_entities(self) -> Iterable[Entity]:
        entities_by_type = all_entities()
        entities: list[Entity] = []
        for entity_type_list in entities_by_type:
            for entity in entity_type_list:
                entities.append(entity)
        return entities


class _AndRestrictionNode(_RestrictionNode):
    def __init__(self, *children: _RestrictionNode) -> None:
        super().__init__(*children)
        self._first_search: Optional[_RestrictionNode] = None
        self._remaining_filters: list[_RestrictionNode] = []
        if self._searches:
            self._first_search = self._searches[0]
            self._remaining_filters.extend(self._searches[1:])
        if self._hybrid_searches:
            if not self._first_search:
                self._first_search = self._hybrid_searches[0]
                self._remaining_filters.extend(self._hybrid_searches[1:])
            else:
                self._remaining_filters.extend(self._hybrid_searches)
        if self._hybrid_filters:
            if not self._first_search:
                self._first_search = self._hybrid_filters[0]
                self._remaining_filters.extend(self._hybrid_filters[1:])
            else:
                self._remaining_filters.extend(self._hybrid_filters)
        if self._filters:
            if not self._first_search:
                self._first_search = self._filters[0]
                self._remaining_filters.extend(self._filters[1:])
            else:
                self._remaining_filters.extend(self._filters)

    def _process_filters(self, source: Entity, targets: Iterable[Entity], filters: list[_RestrictionNode]) -> Iterable[Entity]:
        result:  Iterable[Entity] = targets
        for f in filters:
            result = f.filter(source, result)
        return result

    def search(self, source: Entity) -> Iterable[Entity]:
        if not self._first_search:
            raise Exception()
        entities = set(self._first_search.search(source))
        return self._process_filters(source, entities, self._remaining_filters)

    def filter(self, source: Entity, targets: Iterable[Entity]) -> Iterable[Entity]:
        if not self.prefers_filter:
            return super().filter(source, targets)
        return self._process_filters(source, targets, self._children)


class _OrRestrictionNode(_RestrictionNode):
    def __init__(self, *children: _RestrictionNode) -> None:
        super().__init__(*children)
        if self.type == _TYPE_HYBRID_SEARCH:
            self.type = _TYPE_HYBRID_FILTER

    def search(self, source: Entity) -> Iterable[Entity]:
        result: set[Entity] = set([])
        for c in self._children:
            result = result.union(c.search(source))
        return result

    def filter(self, source: Entity, targets: Iterable[Entity]) -> Iterable[Entity]:
        result: set[Entity] = set([])
        for c in self._children:
            result = result.union(c.filter(source, targets))
        return result


class _NotRestrictionNode(_RestrictionNode):
    def __init__(self, child: _RestrictionNode) -> None:
        super().__init__(child)
        self.type = _TYPE_FILTER

    def filter(self, source: Entity, targets: Iterable[Entity]) -> Iterable[Entity]:
        c = self._children[0]
        if c.prefers_filter:
            outs = set(c.filter(source, targets))
            return [t for t in targets if t not in outs]

        # Not efficient, but works.
        outs = set(c.search(source))
        return [t for t in targets if t not in outs]


class _SearchRestrictionNode(_RestrictionNode):
    def __init__(self, search: EntityReferenceSearch) -> None:
        self._search = search
        super().__init__()
        self.type = _TYPE_SEARCH

    def search(self, source: Entity) -> Iterable[Entity]:
        return self._search.func(source)

    def to_string(self, indent: str = "") -> str:
        return f"{super().to_string(indent)}[\"{self._search.id}\"]"


class _FilterRestrictionNode(_RestrictionNode):
    def __init__(self, filter: EntityReferenceFilter) -> None:
        self._filter = filter
        super().__init__()

    def filter(self, source: Entity, targets: Iterable[Entity]) -> Iterable[Entity]:
        return [t for t in targets if self._filter.func(source, t)]

    def to_string(self, indent: str = "") -> str:
        return f"{super().to_string(indent)}[\"{self._filter.id}\"]"


_ParseState = Tuple[list[str], _RestrictionNode]

def _parse_expression(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    children: list[_RestrictionNode] = []
    tokens, child = _parse_term(tokens)
    children.append(child)
    while tokens and tokens[0] == "or":
        tokens = tokens[1:]  # Consume "or"
        tokens, child = _parse_term(tokens)
        children.append(child)

    if len(children) == 1:
        result = children[0]
    else:
        result = _OrRestrictionNode(*children)
    return tokens, result


def _parse_term(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    children: list[_RestrictionNode] = []
    tokens, child = _parse_atom(tokens)
    children.append(child)
    while tokens and tokens[0] == "and":
        tokens = tokens[1:]  # Consume "and"
        tokens, child = _parse_atom(tokens)
        children.append(child)

    if len(children) == 1:
        result = children[0]
    else:
        result = _AndRestrictionNode(*children)
    return tokens, result


def _parse_atom(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    if is_entity_search(tokens[0]):
        return _parse_search(tokens)

    if is_entity_filter(tokens[0]):
        return _parse_filter(tokens)

    if tokens[0] == "(":
        return _parse_grouped_expression(tokens)

    if tokens[0] == "not":
        return _parse_not_expression(tokens)

    raise Exception(f"Unrecognized token. Remaining tokens: {tokens}")


def _parse_search(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    entity_search = get_entity_search(tokens[0])
    if not entity_search:
        raise Exception()
    result = _SearchRestrictionNode(entity_search)
    tokens = tokens[1:]
    return tokens, result


def _parse_filter(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    entity_filter = get_entity_filter(tokens[0])
    if not entity_filter:
        raise Exception()
    result = _FilterRestrictionNode(entity_filter)
    tokens = tokens[1:]
    return tokens, result


def _parse_grouped_expression(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    tokens = tokens[1:]  # Consume "("
    tokens, child = _parse_expression(tokens)
    if tokens[0] != ")":
        raise Exception()
    tokens = tokens[1:]  # Consume ")"
    return tokens, child


def _parse_not_expression(tokens: list[str]) -> _ParseState:
    if not tokens:
        raise Exception()

    tokens = tokens[1:]  # Consume "not"
    tokens, child = _parse_atom(tokens)
    result = _NotRestrictionNode(child)
    return tokens, result


class Restriction(object):
    def __init__(self, text: str) -> None:
        self.text = text.strip() if text else ""
        canonical_split = self.text.replace(
            "&&", " and ").replace(
            "&", " and ").replace(
            "||", " or ").replace(
            "|", " or ").replace(
            "!", " not ").replace(
            "(", " ( ").replace(
            ")", " ) ").strip().split()
        self.canonical_text = " ".join(canonical_split)
        if not canonical_split:
            self._root = _RestrictionNode()
            return
        tokens, root = _parse_expression(canonical_split)
        if tokens:
            raise Exception(
                f"original: \"{self.text}\" canonical: \"{self.canonical_text}\" " +
                f"remaining tokens: {tokens}")
        self._root = root

    def find_possibilities(self, source: Entity) -> Iterable[Entity]:
        return self._root.search(source)

    def find(self, source: Entity, adjective: Optional[str], noun: Optional[str]) -> Iterable[Entity]:
        entity_set: set[Entity] = set()
        entities: list[Entity] = list()
        for e in self._root.search(source):
            if e.matches(adjective, noun) and e not in entity_set:
                entities.append(e)
                entity_set.add(e)
        return entities

    def __str__(self) -> str:
        return f"Restriction(\"{self.canonical_text}\")"

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Restriction):
            raise TypeError(f"Not a Restriction: {o}")
        return self.canonical_text == o.canonical_text

    def __repr__(self) -> str:
        return str(self)
