from __future__ import annotations

from typing import TYPE_CHECKING, Callable

if TYPE_CHECKING:
    import bezoar._model.entities as entities


class Event(object):
    def __init__(self, type: str):
        self._type = type

    @property
    def type(self) -> str:
        return self._type


StartHook = Callable[[], None]
EventHandler = Callable[['entities.Entity', Event], bool]
EventHandlers = dict[str, EventHandler]


_start_hooks: list[StartHook] = []


def add_start_hook(hook: StartHook) -> None:
    _start_hooks.append(hook)


def run_start_hooks() -> None:
    for hook in _start_hooks:
        hook()
