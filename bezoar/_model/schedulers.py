from typing import Optional

from bezoar.base.scheduler import Scheduler
from bezoar.base.typed_number import TypedNumber

_scheduler_game: Optional[Scheduler] = None
_scheduler_real: Optional[Scheduler] = None
_scheduler_avfx: Optional[Scheduler] = None


class SchedulerType(TypedNumber):
    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


SCHEDULER_REAL = SchedulerType(0, "REAL")
SCHEDULER_AVFX = SchedulerType(1, "AVFX")
SCHEDULER_GAME = SchedulerType(2, "GAME")


def get_scheduler(type: SchedulerType) -> Scheduler:
    if type.__eq__(SCHEDULER_GAME):
        global _scheduler_game
        if not _scheduler_game:
            _scheduler_game = Scheduler(True)
            _scheduler_game.start()
        return _scheduler_game
    if type.__eq__(SCHEDULER_REAL):
        global _scheduler_real
        if not _scheduler_real:
            _scheduler_real = Scheduler(False)
            _scheduler_real.start()
        return _scheduler_real
    if type.__eq__(SCHEDULER_AVFX):
        global _scheduler_avfx
        if not _scheduler_avfx:
            _scheduler_avfx = Scheduler(False)
            _scheduler_avfx.start()
        return _scheduler_avfx
    raise Exception(f"Unknown type: {type}")


async def initialize_schedulers() -> None:
    global _scheduler_game
    if _scheduler_game:
        await _scheduler_game.stop()
        _scheduler_game = None
    global _scheduler_real
    if _scheduler_real:
        await _scheduler_real.stop()
        _scheduler_real = None
    global _scheduler_avfx
    if _scheduler_avfx:
        await _scheduler_avfx.stop()
        _scheduler_avfx = None

    get_scheduler(SCHEDULER_AVFX)
    get_scheduler(SCHEDULER_GAME)
    get_scheduler(SCHEDULER_REAL)
