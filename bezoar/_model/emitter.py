from typing import Callable

import bezoar.mddom as mddom

Emitter = Callable[[mddom.Nodeish], None]
