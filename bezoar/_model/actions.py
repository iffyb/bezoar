from __future__ import annotations

from typing import Callable, Iterable, Mapping, Optional, Sequence, Union, cast

import bezoar._model.entities as entities
import bezoar._model.events as events
import bezoar._model.schedulers as schedulers
from bezoar._model.resetting import add_reset_hook
from bezoar._model.restrictions import Restriction
from bezoar.base.overlay_mapping import OverlayMapping
from bezoar.base.time import Duration, zero
from bezoar.base.typed_number import TypedNumber


class Phase(TypedNumber):
    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


PHASE_ISSUE = Phase(0, "ISSUE")
PHASE_ACTIVATE = Phase(1, "ACTIVATE")
PHASE_TRIGGER = Phase(2, "TRIGGER")
PHASE_DEACTIVATE = Phase(3, "DEACTIVATE")
PHASE_DONE = Phase(4, "DONE")


class ActionResult(TypedNumber):
    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


RESULT_NOT_HANDLED = ActionResult(0, "NOT_HANDLED")
RESULT_DONE = ActionResult(1, "DONE")
RESULT_ACTIVATE = ActionResult(2, "ACTIVATE")
RESULT_DEACTIVATE = ActionResult(3, "DEACTIVATE")


EVENT_ACTION_INTERRUPT = "action_interrupt"
EVENT_ACTION_NOTIFICATION = "action_notification"
EVENT_ACTION_READY = "action_ready"


class ParseResult(object):
    """
    The complete results of a parse, as applied to the context of an actor.
    TODO: Add verb used
    TODO: Let actions provide implied direct/indirect objects
    """

    def __init__(
        self,
        act: Action,
        actor: 'entities.Entity',
        objects: Mapping[str, list['entities.Entity']],
        direction: Optional[str],
    ) -> None:
        super().__init__()
        self.phase = PHASE_ISSUE
        self.properties: entities.Properties = OverlayMapping()
        self.action = act
        self.actor = actor
        self.objects = objects
        if direction is not None:
            self.direction = direction
        else:
            self.direction = ""


class HandlerResult(object):
    def __init__(self, result: ActionResult, duration: Duration = zero()) -> None:
        super().__init__()
        self.result = result
        self.duration = duration


ActionResultSpec = Optional[Union[HandlerResult, ActionResult, bool]]
ActionHandler = Callable[['entities.Entity', ParseResult], ActionResultSpec]
EntityActionHandler = Callable[[
    'entities.Entity', str, ParseResult], ActionResultSpec]


_actions: dict[str, Action] = {}


def _parse_action_result_spec(spec: ActionResultSpec) -> HandlerResult:
    if spec is None or spec is False:
        return HandlerResult(RESULT_NOT_HANDLED)
    if spec is True:
        return HandlerResult(RESULT_DONE)
    if isinstance(spec, ActionResult):
        return HandlerResult(spec)
    return cast(HandlerResult, spec)


def bind_reference_name_to_entity_action_handler(reference_name: str, handler: EntityActionHandler) -> ActionHandler:
    return lambda e, r: handler(e, reference_name, r)


class ActionInterruptEvent(events.Event):
    def __init__(self, result: ParseResult):
        super().__init__(EVENT_ACTION_INTERRUPT)
        self.result = result


class ActionNotificationEvent(events.Event):
    def __init__(self, result: ParseResult):
        super().__init__(EVENT_ACTION_NOTIFICATION)
        self.result = result


def dispatch_action(result: Optional[ParseResult]) -> bool:
    if result is None:
        return False

    # TODO:
    #   1. Check for interceptors in entities specified by the action_broadcast
    #      restriction.
    #
    #   2. If the handler returns ACTIVATE, delay and call the same handler back
    #      in the TRIGGER phase.
    #
    #      If the handler returns DEACTIVATE, delay and then give the Actor back
    #      their prompt.
    possibilities: Iterable[entities.Entity] = []
    if _action_broadcast:
        possibilities = _action_broadcast.find_possibilities(result.actor)

    if possibilities:
        interrupted = False
        for po in possibilities:
            if interrupted:
                break
            interrupted = po.handle_event(ActionInterruptEvent(result))
        if interrupted:
            return True

    if result.action is not None:
        stop = False
        if not stop:
            for reference_name in sorted(result.objects.keys()):
                es: list[entities.Entity] = result.objects[reference_name]
                for e in es:
                    stop = e.handle_action(reference_name, result)
                    if stop:
                        break
                if stop:
                    break

        if not stop:
            return invoke_handler(result.actor, result.action.handler, result)
        return stop

    if possibilities:
        for pos in possibilities:
            pos.handle_event(ActionNotificationEvent(result))

    result.actor.tell("I really don't understand. <This indicates a bug.>")
    return False


def invoke_handler(entity: entities.Entity,
                   handler: ActionHandler,
                   parse_result: ParseResult) -> bool:
    if handler is None:
        return False

    scheduler = schedulers.get_scheduler(schedulers.SCHEDULER_GAME)
    while parse_result.phase.__ne__(PHASE_DONE):
        handler_result = _parse_action_result_spec(
            handler(entity, parse_result))
        if handler_result.result.__eq__(RESULT_ACTIVATE):
            parse_result.phase = PHASE_ACTIVATE
            duration = handler_result.duration
            if duration > zero():
                async def activate() -> None:
                    parse_result.phase = PHASE_TRIGGER
                    invoke_handler(entity, handler, parse_result)
                scheduler.schedule_in(duration, activate)
                if scheduler.paused and entity is entities.get_player():
                    scheduler.advance_sync(duration)
                return True
            continue

        if handler_result.result.__eq__(RESULT_DEACTIVATE):
            parse_result.phase = PHASE_DONE
            duration = handler_result.duration
            if duration > zero():
                async def deactivate() -> None:
                    invoke_handler(entity, handler, parse_result)
                scheduler.schedule_in(duration, deactivate)
                if scheduler.paused and entity is entities.get_player():
                    scheduler.advance_sync(duration)
                return True
            continue

        if handler_result.result.__eq__(RESULT_NOT_HANDLED):
            if parse_result.phase.__eq__(PHASE_ISSUE):
                return False
            parse_result.phase = PHASE_DONE
            continue

        if handler_result.result.__eq__(RESULT_DONE):
            parse_result.phase = PHASE_DONE
            continue

    return True


class Action(object):
    def __init__(
        self,
        id: str,
        syntaxes: Sequence[str],
        handler: ActionHandler
    ) -> None:
        super().__init__()

        if not syntaxes:
            raise ValueError("You must provide at least one action "
                             "syntax.")

        if handler is None:
            raise ValueError("You must provide an action handler routine.")
        self.id = id
        self.syntaxes: list[str] = []
        for syntax in syntaxes:
            self.syntaxes.append(syntax)

        self.handler: ActionHandler = handler

        global _actions
        _actions[self.id] = self

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, Action):
            raise TypeError(f"Not an Action: {o}")
        return self.id == o.id

    def __hash__(self) -> int:
        return self.id.__hash__()

    def __str__(self) -> str:
        return f"(Action {self.id})"


def action(
    id: str,
    syntaxes: Sequence[str],
    handler: ActionHandler
) -> Action:
    """
    Defines a valid action rule, specifying its syntax and mapping it to its
    action handler. Multiple syntaxes are expected to be mapped to the same
    action, to define different language forms of the same action.

    If multiple action syntaxes are indistinguishable, this will be raised
    as an exception.

    Specification language:
        text - Raw text defines the literal words used to invoke the action. The
               first word is assumed to be the verb, and other words are assumed
               to be prepositions.

        [<verb>] - If the verb is not the first word in the specification, it
                   can be bracketed. The other words are assumed to be
                   prepositions. E.g. "from {indirect} [get] {direct}"

        {<group>} - A placeholder for an object reference with the given group
                    name. The action will receive a list of objects parsed
                    for each object group name in the specification.

                    Classic group names might be "direct" for the direct object,
                    and "indirect" for any indirect objects, but this is not
                    enforced.

                    This ends up referring to a built-in sub-grammar that
                    supports articles, adjectives, and adjectival prepositions.
                    E.g. "lamp" or "the green sword in the brown chest".

        {<group>+} - A placeholder for a multiple object reference.

        {direction} - A placeholder for a direction. There can be zero or one
                      direction specifications per action specification.

    Future, maybe:
        {verb} - A placeholder for another verb. An action specification that
                 refers to another verb is a form that modifies other verbs
                 rather than defining their own behavior.

        {the} - A placeholder for an optional "the". For example, "look at {the}
                back of {direct}" would match both "look at back of note" and
                "look at the back of".

    Object restrictions:
        {<object>:<restriction>} - A restriction limits which or what type of
                                   objects can be used in that object reference.
                                   If no restriction is specified, then all
                                   objects are available to be matched.

                                   A restriction is a boolean expression made up
                                   of filters and searches. A search grows the
                                   set of objects. e.g. "here" might mean "all
                                   objects in the same location as the actor." A
                                   filter shrinks the set of objects. e.g.
                                   "takeable" might mean "objects that can be
                                   taken." The difference is really just a
                                   matter of performance. If a filter is
                                   specified, and no search, the parser will
                                   start with all known objects and filter down.
                                   With a search, it will find the objects,
                                   rather than filter down from all objects. The
                                   system is smart enough to use searches to
                                   efficiently evaluate the restriction, but if
                                   a search isn't available in the right place,
                                   it may have to filter through all objects, in
                                   which case it will print a warning.

    Examples:
        "get {direct:here and gettable}"
        "read {direct:(have or here) and readable}"
        "put {direct+:have} on {indirect:here and surface}"
    """

    return Action(id=id, syntaxes=syntaxes, handler=handler)


class ActionAndHandler(object):
    def __init__(
        self,
        action: Action,
        handler: EntityActionHandler
    ) -> None:
        self.action = action
        self.handler = handler


def find_action(id: str) -> Optional[Action]:
    if id not in _actions:
        return None
    return _actions[id]


def get_actions() -> Iterable[Action]:
    return _actions.values()


_action_broadcast: Optional[Restriction] = None


def set_action_broadcast(id: str) -> None:
    """Sets the scope to broadcast actions from the actor, in restriction
    language."""
    global _action_broadcast
    _action_broadcast = Restriction(id)


def _reset():
    global _actions
    _actions.clear()
    global _action_broadcast
    _action_broadcast = None


add_reset_hook(_reset)
