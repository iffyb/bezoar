from __future__ import annotations

from typing import TYPE_CHECKING, Optional

from bezoar.mddom import Block, b, i, l, p
from bezoar._model.entities import Entity

class Version(object):
    def __init__(self, app_name: str, copyright: str, version: str,
                 intro: str) -> None:
        super().__init__()
        self.app_name = app_name
        self.copyright = copyright
        self.version = version
        self.intro = intro

    def tell(self, actor: Entity) -> None:
        block: Optional[Block] = None
        if self.app_name:
            if not block:
                block = p()
            block.children.append(l(b(self.app_name)))
        if self.copyright:
            if not block:
                block = p()
            block.children.append(l(i(self.copyright)))
        if self.version:
            if not block:
                block = p()
            block.children.append(l(f"version {self.version}"))

        if block:
            actor.tell(block)

        if self.intro:
            actor.tell(p(self.intro))


_version: Version = Version("", "", "", "")


def version(app_name: str, copyright: str = "", version: str = "",
            intro: str = "") -> Version:
    global _version
    _version = Version(app_name=app_name,
                       copyright=copyright,
                       version=version,
                       intro=intro)
    return _version


def get_version() -> Version:
    return _version
