from bezoar._model.entities import (all_entity_data, find_entity,
                                    get_player_data, set_player)
from bezoar._model.events import run_start_hooks
from bezoar._model.relationships import RelationshipData
from bezoar._model.schedulers import initialize_schedulers


async def initialize_model() -> None:
    await initialize_schedulers()

    for ed in all_entity_data():
        ed.resolve_links()

    for rd in RelationshipData.RELATIONSHIP_DATA.values():
        rd.resolve_links()

    # Instantiate the player and things related to the player.
    player_data = get_player_data()
    if player_data is None:
        raise Exception("No player data defined.")

    # TODO: clean this mess up
    for ed in all_entity_data():
        if ed.related_to(player_data) or ed.should_instantiate:
            if find_entity(ed.id) is None:
                ed.instantiate()

    player = find_entity(player_data.id)
    if player is None:
        raise Exception("Player not instantiated.")
    set_player(player)

    run_start_hooks()
