from __future__ import annotations

from typing import (Callable, Mapping, MutableMapping, MutableSequence,
                    Optional, Sequence, TypeVar, Union)

import bezoar._model.entities as entities
from bezoar._model.resetting import add_reset_hook
from bezoar.base.typed_number import TypedNumber


class Arity(TypedNumber):
    """The number of simultaneous connections for one side of a Relationship.

    ONE - Each Entity can only have ONE Relationship of this type. Forming a new
          relationship of this type will cause the existing Relationship to be
          automatically broken.

    MANY - Each Entity can have MANY Relationships of this type. Forming a
           relationship of this type will have no effect on the existing
           Relationships of this entities.

    Relationships can be either uni-directional or bi-directional, and thus each
    direction of a relationship can have its own Arity.

    Example:
        "container" - "contents" is a ONE to MANY bi-directional Relationship.
        Each Entity can only have ONE "container" Relationship, but may have
        MANY "contents" Relationships. A container can contain MANY things,
        but each thing can only be (directly) contained by ONE thing at a time.
    """

    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


ARITY_ONE = Arity(1, "ONE")
ARITY_MANY = Arity(2, "MANY")


class TargetFlags(TypedNumber):
    """Flags relaxing target constraints of one side of a Relationship.

    SELF - The Entity may have this Relationship with itself. (The default is
           that this would cause an error.)

    NONE - The Entity may have this Relationship with no Target Entity. (The
           default is that this would cause an error.)

    REPEAT - The Entity may have multiple Relationships to the same Target
             Entity. (The default is that this would cause an error.)
    """

    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


TARGET_FLAGS_SELF = TargetFlags(1, "SELF")
TARGET_FLAGS_NONE = TargetFlags(2, "NONE")
TARGET_FLAGS_REPEAT = TargetFlags(3, "REPEAT")


class RelationshipValueFullSpecification(object):
    def __init__(
            self,
            target: Optional[entities.EntityDataSpecification],
            properties: entities.PropertiesSpec
    ) -> None:
        super().__init__()
        self.target = target
        self.properties = properties

    def __repr__(self) -> str:
        return f"(RelationshipValueFullSpec target={repr(self.target)} properties={self.properties})"


class RelationshipValue(object):
    def __init__(self,
                 relationship_data: RelationshipData,
                 target: Optional[entities.EntityData],
                 properties: entities.PropertiesSpec = {}) -> None:
        super().__init__()
        self.relationship_data = relationship_data
        self.target = target
        self.properties = entities.copy_spec(properties)

    def __repr__(self) -> str:
        return f"(RelationshipValue target={repr(self.target)} properties={self.properties})"


RelationshipValueSpecification = Union['entities.EntityDataSpecification',
                                       RelationshipValueFullSpecification]

RelationshipDataSpecification = str
RelationshipsSpecification = Mapping[RelationshipDataSpecification,
                                     Sequence[RelationshipValueSpecification]]
MutableRelationshipsSpecification = MutableMapping[RelationshipDataSpecification,
                                                   MutableSequence[RelationshipValueSpecification]]
RelationshipsData = Mapping[str, Sequence[RelationshipValue]]
MutableRelationshipsData = MutableMapping[str,
                                          MutableSequence[RelationshipValue]]
Relationships = MutableMapping[str, MutableSequence['Relationship']]
MutableRelationships = Mapping[str, Sequence['Relationship']]

CanRelateHandler = Callable[['entities.Entity',
                             'RelationshipData', Optional['entities.Entity']], bool]
RelateHandler = Callable[['entities.Entity',
                          'RelationshipData', Optional['entities.Entity']], None]
CanUnrelateHandler = Callable[[
    'Relationship', Optional['entities.Entity']], bool]
UnrelateHandler = Callable[['Relationship', Optional['entities.Entity']], None]


class RelationshipHandlerSpecification(object):
    def __init__(
            self,
            can_relate: Optional[CanRelateHandler],
            can_unrelate: Optional[CanUnrelateHandler],
            on_relate: Optional[RelateHandler],
            on_unrelate: Optional[UnrelateHandler],
    ) -> None:
        super().__init__()
        self.can_relate = can_relate
        self.can_unrelate = can_unrelate
        self.on_relate = on_relate
        self.on_unrelate = on_unrelate


class RelationshipDataAndHandlers(object):
    def __init__(
            self,
            data: RelationshipData,
            handlers: RelationshipHandlerSpecification,
    ) -> None:
        super().__init__()
        self.data = data
        self.handlers = handlers


RelationshipHandlers = Mapping[str, RelationshipDataAndHandlers]
MutableRelationshipHandlers = MutableMapping[str, RelationshipDataAndHandlers]
RelationshipHandlersSpecification = Mapping[RelationshipDataSpecification,
                                            RelationshipHandlerSpecification]
MutableRelationshipHandlersSpecification = MutableMapping[RelationshipDataSpecification,
                                                          RelationshipHandlerSpecification]


def _count(firstval=0, step=1):
    x = firstval
    while 1:
        yield x
        x += step


def copy_relationships_specification(spec: RelationshipsSpecification) -> MutableRelationshipsSpecification:
    if spec is None:
        return {}
    result: MutableRelationshipsSpecification = {}
    for k, v in spec.items():
        result[k] = list(v)
    return result


def evaluate_relationship_value_specification(
        relationship_data: RelationshipData,
        entity_data: entities.EntityData,
        value: RelationshipValueSpecification) -> RelationshipValue:
    if isinstance(value, RelationshipValueFullSpecification):
        if value.target:
            data = entities.evaluate_entity_data_specification(
                entity_data, value.target)
            if data is None:
                raise ValueError(
                    f"Unable to find entity for {entity_data}: {value}")
        else:
            data = None
        return RelationshipValue(relationship_data, data, value.properties)

    data = entities.evaluate_entity_data_specification(entity_data, value)
    if (value is not None or value == "") and data is None:
        raise ValueError(
            f"Unable to find entity for {entity_data}: {value}")
    return RelationshipValue(relationship_data, data)


def evaluate_relationship_data_specification(
        entity_data: entities.EntityData,
        value: RelationshipDataSpecification) -> Optional[RelationshipData]:
    if value is None:
        return None
    if isinstance(value, RelationshipData):
        return value
    if isinstance(value, str):
        return find_relationship_data(value)
    if callable(value):
        return value(entity_data)
    raise ValueError("Not a RelationshipDataSpecification: "
                     f"{value.__class__.__name__}")


def evaluate_relationships_data_specification(
        entity_data: entities.EntityData,
        value: RelationshipsSpecification) -> RelationshipsData:
    result: MutableRelationshipsData = {}
    if not value:
        return result
    for rel_spec, value_specs in value.items():
        if not rel_spec:
            raise ValueError(
                f"Empty relationship specification for {entity_data}")
        if not value_specs:
            raise ValueError(
                "Empty relationship value specification for "
                f"{entity_data}: {rel_spec}")
        rel_data = evaluate_relationship_data_specification(
            entity_data, rel_spec)
        if rel_data is None:
            raise ValueError(
                f"Unable to find relationship for {entity_data}: {rel_spec}")
        if rel_data.id not in result:
            entities_data: list[RelationshipValue] = []
            result[rel_data.id] = entities_data
        for e in value_specs:
            d = evaluate_relationship_value_specification(
                rel_data, entity_data, e)
            entities_data.append(d)
    return result


def evaluate_relationship_handlers_specification(
        entity_data: entities.EntityData,
        value: RelationshipHandlersSpecification) -> RelationshipHandlers:
    result: MutableRelationshipHandlers = {}
    if value is None:
        return result
    for rel_spec, handler_specs in value.items():
        if not rel_spec:
            raise ValueError(
                f"Empty relationship specification for {entity_data}")
        if handler_specs is None:
            raise ValueError(
                f"Empty entity specification for {entity_data}: {rel_spec}")
        rel_data = evaluate_relationship_data_specification(
            entity_data, rel_spec)
        if rel_data is None:
            raise ValueError(
                f"Unable to find relationship for {entity_data}: {rel_spec}")
        result[rel_data.id] = RelationshipDataAndHandlers(
            rel_data,
            relate_handlers(handler_specs.can_relate,
                            handler_specs.can_unrelate,
                            handler_specs.on_relate,
                            handler_specs.on_unrelate))
    return result


class RelationshipData(object):
    _COUNTER = _count()
    RELATIONSHIP_DATA: dict[str, RelationshipData] = {}
    RELATIONSHIP_DATA_BY_COUNTER: dict[int, RelationshipData] = {}

    def __init__(self,
                 id: str,
                 handlers: RelationshipHandlerSpecification,
                 arity: Arity,
                 target_flags: set[TargetFlags],
                 reverse_id: Optional[str],
                 ) -> None:
        super().__init__()
        self.counter = next(RelationshipData._COUNTER)
        self.id = id
        self.arity = arity
        self.target_flags = target_flags
        self.reverse_id = reverse_id
        self.reverse: Optional[RelationshipData] = None
        self.handlers = handlers
        if self.id in RelationshipData.RELATIONSHIP_DATA:
            raise ValueError(f"Duplicate Relationship ID: {self.id}")
        if self.counter in RelationshipData.RELATIONSHIP_DATA_BY_COUNTER:
            raise Exception(
                f"Duplicate Relationship counter: {self.counter}")
        RelationshipData.RELATIONSHIP_DATA[self.id] = self
        RelationshipData.RELATIONSHIP_DATA_BY_COUNTER[self.counter] = self

    def can_relate(self, from_entity: entities.Entity, new_entity: Optional[entities.Entity]) -> bool:
        if new_entity is from_entity and TARGET_FLAGS_SELF not in self.target_flags:
            return False
        if new_entity is None and TARGET_FLAGS_NONE not in self.target_flags:
            return False
        if self.handlers.can_relate is None:
            return True
        return self.handlers.can_relate(from_entity, self, new_entity)

    def can_unrelate(self, rel: Relationship, next_entity: Optional[entities.Entity] = None) -> bool:
        if self.handlers.can_unrelate is None:
            return True
        return self.handlers.can_unrelate(rel, next_entity)

    def on_relate(self, from_entity: entities.Entity, new_entity: Optional[entities.Entity]) -> None:
        if self.handlers.on_relate is None:
            return
        self.handlers.on_relate(from_entity, self, new_entity)

    def on_unrelate(self, rel: Relationship, next_entity: Optional[entities.Entity] = None) -> None:
        if self.handlers.on_unrelate is None:
            return
        self.handlers.on_unrelate(rel, next_entity)

    def __eq__(self, o: object) -> bool:
        if not isinstance(o, RelationshipData):
            raise TypeError(f"Not a RelationshipData: {o}")
        return self.counter == o.counter

    def __hash__(self) -> int:
        return self.counter

    def __repr__(self) -> str:
        return f"(RelationshipData {self.id}({self.counter}))"

    def resolve_links(self) -> None:
        if self.reverse is None and self.reverse_id:
            self.reverse = RelationshipData.RELATIONSHIP_DATA[self.reverse_id]


class Relationship(object):
    def __init__(self, data: RelationshipData, from_entity: entities.Entity,
                 to_entity: Optional[entities.Entity],
                 properties: entities.PropertiesSpec = {}) -> None:
        super().__init__()
        self.data = data
        self.from_entity = from_entity
        self.to_entity = to_entity
        self.properties = entities.copy_spec(properties)
        self.reverse: Optional[Relationship] = None

    def can_unrelate(self, next_entity: Optional[entities.Entity] = None) -> bool:
        return self.data.can_unrelate(self, next_entity)

    def __repr__(self) -> str:
        return (f"(Relationship {self.data.id}({self.data.counter}) "
                f"{self.from_entity} -> {self.to_entity} {self.properties})")


def relate_handlers(can_relate: Optional[CanRelateHandler] = None,
                    can_unrelate: Optional[CanUnrelateHandler] = None,
                    on_relate: Optional[RelateHandler] = None,
                    on_unrelate: Optional[UnrelateHandler] = None,
                    ) -> RelationshipHandlerSpecification:
    return RelationshipHandlerSpecification(can_relate=can_relate,
                                            can_unrelate=can_unrelate,
                                            on_relate=on_relate,
                                            on_unrelate=on_unrelate)


def relationship(id: str,
                 handlers: RelationshipHandlerSpecification = relate_handlers(),
                 arity: Arity = ARITY_ONE,
                 target_flags: set[TargetFlags] = set({}),
                 reverse_id: Optional[str] = None,
                 reverse_handlers: RelationshipHandlerSpecification = relate_handlers(),
                 reverse_arity: Arity = ARITY_ONE,
                 reverse_target_flags: set[TargetFlags] = set({}),
                 ) -> RelationshipData:
    forward = RelationshipData(
        id=id,
        arity=arity,
        target_flags=target_flags,
        handlers=handlers,
        reverse_id=reverse_id,
    )
    if reverse_id:
        reverse = RelationshipData(
            id=reverse_id,
            arity=reverse_arity,
            target_flags=reverse_target_flags,
            handlers=reverse_handlers,
            reverse_id=id,
        )
    return forward


def relationship_value(target: Optional[entities.EntityDataSpecification],
                       properties: entities.PropertiesSpec = {}) -> RelationshipValueFullSpecification:
    return RelationshipValueFullSpecification(target, properties)


K = TypeVar('K')
V = TypeVar('V')


def _get(m: Mapping[K, V], k: K, d: V) -> V:
    if k in m:
        return m[k]
    return d


def find_relationship_data(id: Union[str, int]) -> Optional[RelationshipData]:
    if isinstance(id, str):
        return _get(RelationshipData.RELATIONSHIP_DATA, id, None)
    if isinstance(id, int):
        return _get(RelationshipData.RELATIONSHIP_DATA_BY_COUNTER, id, None)
    raise TypeError(f"id is not int or str: {id}")


def _reset():
    RelationshipData.RELATIONSHIP_DATA.clear()
    RelationshipData.RELATIONSHIP_DATA_BY_COUNTER.clear()


add_reset_hook(_reset)
