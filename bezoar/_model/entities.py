from __future__ import annotations

from typing import (Any, Callable, Iterable, Mapping, MutableMapping,
                    MutableSequence, Optional, Sequence, TypeVar, Union)

import bezoar._model.actions as actions
import bezoar._model.events as events
import bezoar._model.relationships as relshp
import bezoar.mddom as mddom
from bezoar._model.emitter import Emitter
from bezoar._model.resetting import add_reset_hook
from bezoar._model.text import a
from bezoar.base.algorithms import dfs
from bezoar.base.itertools import first
from bezoar.base.overlay_mapping import OverlayMapping
from bezoar.base.text import pluralize

Properties = OverlayMapping

PropertiesSpec = Mapping[str, Any]

StringSequence = Sequence[str]
MutableStringSequence = MutableSequence[str]
StringProducer = Callable[['Entity'], str]
StringSequenceProducer = Callable[['Entity'], StringSequence]

DynamicString = Union[str, StringProducer]

# A DynamicStringSequence can either be a Sequence of strings, a function that
# produces a Sequence of strings, or a Sequence of DynamicStrings |
# StringSequenceProducers. StringSequenceProducers inside Sequence will get
# flattened into the resulting Sequence.
DynamicStringSequence = Union[Sequence[Union[DynamicString, StringSequenceProducer]],
                              StringSequenceProducer]
EntityDataSpecification = Union['EntityData', str]
ContentsData = MutableSequence[EntityDataSpecification]

MoveHandler = Callable[['Entity', 'Entity'], bool]

ActionSpecification = Union['actions.Action', str]
ActionHandlersSpecification = Mapping[ActionSpecification,
                                      'actions.EntityActionHandler']
MutableActionHandlersSpecification = MutableMapping[ActionSpecification,
                                                    'actions.EntityActionHandler']
ActionHandlers = Mapping[str, 'actions.ActionAndHandler']
MutableActionHandlers = MutableMapping[str, 'actions.ActionAndHandler']

# Given a source and target entity, returns true to retain the target entity.
EntityFilter = Callable[['Entity', 'Entity'], bool]

# Given a source entity, returns an Iterable of Entities found from the source.
EntitySearch = Callable[['Entity'], Iterable['Entity']]

NodeishProducer = Callable[['Entity'], mddom.Nodeish]
SpanishProducer = Callable[['Entity'], mddom.Spanish]
BlockishProducer = Callable[['Entity'], mddom.Blockish]
DynamicNodeish = Union[mddom.Nodeish, NodeishProducer]
DynamicSpanish = Union[mddom.Spanish, SpanishProducer]
DynamicBlockish = Union[mddom.Blockish, BlockishProducer]

# A generic type for a function bound to an Entity instance.
EntityClosure = Callable[['Entity'], None]


def empty_entity_closure(_: Entity) -> None:
    pass


class DescriptionData(object):
    def __init__(self,
                 quick: Optional[DynamicSpanish],
                 quick_renderer: Optional[SpanishProducer],
                 situ: Optional[DynamicSpanish],
                 situ_renderer: Optional[SpanishProducer],
                 detail: Optional[DynamicBlockish],
                 detail_renderer: Optional[BlockishProducer],
                 prefers_situ: Optional[bool],
                 ) -> None:
        """
        """
        self.quick = quick
        self.quick_renderer = quick_renderer
        self.situ = situ
        self.situ_renderer = situ_renderer
        self.detail = detail
        self.detail_renderer = detail_renderer
        self.prefers_situ = prefers_situ


def description(quick: Optional[DynamicSpanish] = None,
                quick_renderer: Optional[SpanishProducer] = None,
                situ: Optional[DynamicSpanish] = None,
                situ_renderer: Optional[SpanishProducer] = None,
                detail: Optional[DynamicBlockish] = None,
                detail_renderer: Optional[BlockishProducer] = None,
                prefers_situ: Optional[bool] = None,
                ) -> DescriptionData:
    return DescriptionData(quick=quick,
                           quick_renderer=quick_renderer,
                           situ=situ,
                           situ_renderer=situ_renderer,
                           detail=detail,
                           detail_renderer=detail_renderer,
                           prefers_situ=prefers_situ,
                           )


NO_DESCRIPTION = description()


def _count(firstval=1, step=1):
    x = firstval
    while 1:
        yield x
        x += step


_counter = _count()


K = TypeVar('K')
V = TypeVar('V')


def _get(m: Mapping[K, V], k: K, d: V) -> V:
    if k in m:
        return m[k]
    return d


def copy_spec(spec: Mapping[K, V]) -> MutableMapping[K, V]:
    if not spec:
        return {}
    result: MutableMapping[K, V] = {}
    result.update(spec)
    return result


def auto(prefix: Optional[str] = "autoid") -> str:
    global _counter
    return f"{prefix}_{next(_counter)}"


def evaluate_entity_data_specification(
        entity_data: EntityData,
        value: EntityDataSpecification) -> Optional[EntityData]:
    if value is None:
        return None
    if isinstance(value, EntityData):
        return value
    if isinstance(value, str):
        return find_data(value)
    if callable(value):
        return value(entity_data)
    raise ValueError("Not a EntityDataSpecification: "
                     f"{value.__class__.__name__}")


def evaluate_action_specification(
        entity_data: EntityData,
        value: ActionSpecification) -> Optional['actions.Action']:
    if value is None:
        return None
    if isinstance(value, actions.Action):
        return value
    if isinstance(value, str):
        return actions.find_action(value)
    if callable(value):
        return value(entity_data)
    raise ValueError("Not an ActionSpecification: "
                     f"{value.__class__.__name__}")


def evaluate_action_handlers_specification(
        entity_data: EntityData,
        value: ActionHandlersSpecification) -> ActionHandlers:
    result: MutableActionHandlers = {}
    if value is None:
        return result
    for action_spec, action_handler in value.items():
        if action_spec is None or action_spec == "":
            raise ValueError(
                f"No action specification for {entity_data}")
        if action_handler is None:
            raise ValueError(
                f"Empty action handler for {entity_data}: {action_spec}")
        action_data = evaluate_action_specification(entity_data, action_spec)
        if action_data is None:
            raise ValueError(
                f"Unable to find action for {entity_data}: {action_spec}")
        result[action_data.id] = actions.ActionAndHandler(
            action_data, action_handler)
    return result


def copy_dynamic_string_sequence(spec: DynamicStringSequence) -> DynamicStringSequence:
    if spec is None:
        return set()
    if callable(spec):
        return spec
    return [i for i in spec]


def dfs_mixins(root: EntityData, get_value: Callable[[EntityData], Optional[V]]) -> Iterable[V]:
    return dfs(root, get_value, lambda x: x.mixins)


_ENTITY_DATA: dict[str, EntityData] = {}


class EntityData(object):
    """
    The static (constant, non-instance) data of a game object.
    """

    def __init__(
        self,
        id: str,
        description: DescriptionData,
        adjectives: DynamicStringSequence,
        aliases: DynamicStringSequence,
        mixins: Sequence[EntityDataSpecification],
        relationships: relshp.RelationshipsSpecification,
        relationship_handlers: relshp.RelationshipHandlersSpecification,
        on_initialize: Optional[EntityClosure],
        on_action_ready: Optional[EntityClosure],
        actions: ActionHandlersSpecification,
        properties: PropertiesSpec,
        event_handlers: events.EventHandlers,
        should_instantiate: bool,
    ) -> None:
        self.id = id
        self.description = description
        self.adjectives = copy_dynamic_string_sequence(adjectives)
        self.aliases = copy_dynamic_string_sequence(aliases)
        self.properties = dict(properties)
        self._raw_mixins = list(mixins)
        self.mixins: Sequence[EntityData] = []
        self._raw_relationships: relshp.MutableRelationshipsSpecification = relshp.copy_relationships_specification(
            relationships)
        self._raw_relationship_handlers = relationship_handlers
        self.relationships: relshp.RelationshipsData = {}
        self.relationship_handlers: relshp.RelationshipHandlers = {}
        self.on_initialize = on_initialize
        self.on_action_ready = on_action_ready
        self._raw_actions: MutableActionHandlersSpecification = copy_spec(
            actions)
        self.actions: ActionHandlers = {}
        self.event_handlers = copy_spec(event_handlers)
        self.should_instantiate = should_instantiate

        if self.id in _ENTITY_DATA:
            raise ValueError(f"Existing ID: {self.id}")
        _ENTITY_DATA[self.id] = self

    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(id={repr(self.id)})"

    def __str__(self) -> str:
        return f"<{repr(self)}>"

    def resolve_links(self) -> None:
        mixins = [evaluate_entity_data_specification(
            self, x) for x in self._raw_mixins]
        self.mixins = [x for x in mixins if x]
        self._raw_mixins = []
        self.relationships = relshp.evaluate_relationships_data_specification(
            self, self._raw_relationships)
        self._raw_relationships = {}
        self.relationship_handlers = relshp.evaluate_relationship_handlers_specification(
            self, self._raw_relationship_handlers)
        self._raw_relationship_handlers = {}
        self.actions = evaluate_action_handlers_specification(
            self, self._raw_actions)
        self._raw_actions = {}

    def isa(self, e: EntityDataSpecification) -> bool:
        ed = evaluate_entity_data_specification(self, e)
        if ed is None:
            return False
        return ed in [x for x in dfs_mixins(self, lambda x: x)]

    def related_to(self, e: EntityDataSpecification) -> bool:
        for k in self.relationships.keys():
            for v in self.relationships[k]:
                if v.target == e:
                    return True
        return False

    def instantiate(self) -> Entity:
        return Entity(self)


_COUNTER = _count()
_ENTITIES: dict[str, list[Entity]] = {}
_ENTITIES_BY_COUNTER: dict[int, Entity] = {}


class Entity(object):
    def __init__(self, data: EntityData) -> None:
        super().__init__()
        self._data = data
        self._busy = True
        self._counter = next(_COUNTER)
        self._emit: Emitter = lambda x: None
        self._relationships: relshp.Relationships = {}
        self._properties = OverlayMapping(
            *dfs_mixins(self._data, lambda x: x.properties))
        id = self.id
        if id not in _ENTITIES:
            _ENTITIES[id] = []
        id_list = _ENTITIES[id]
        if self in id_list:
            raise ValueError(f"Entity already registered: {id}")
        id_list.append(self)
        if self._counter in _ENTITIES_BY_COUNTER:
            raise Exception(f"Duplicate counter for {id}: {self._counter}")
        _ENTITIES_BY_COUNTER[self._counter] = self

        # Instantiate all uninstantiated relationships.
        for values in self._data.relationships.values():
            for v in values:
                target = v.target
                if target is not None:
                    instance = find_entity(target.id)
                    if instance is None:
                        instance = target.instantiate()
                else:
                    instance = None
                self.relate(v.relationship_data, instance, v.properties)

        for initializer in reversed(list(dfs_mixins(self._data, lambda x: x.on_initialize))):
            initializer(self)

    @property
    def emitter(self) -> Emitter:
        return self._emit

    @emitter.setter
    def emitter(self, emit: Emitter) -> None:
        self._emit = emit

    def tell(self, message: mddom.Nodeish) -> None:
        self._emit(message)

    def isa(self, e: EntityDataSpecification) -> bool:
        return self._data.isa(e)

    def dump_relationships(self) -> str:
        return repr(self._relationships)

    def handle_action_ready(self) -> None:
        if self != get_player():
            return
        handler = first(dfs_mixins(self._data, lambda x: x.on_action_ready))
        if handler:
            handler(self)

    def handle_action(self, reference_name: str, result: 'actions.ParseResult') -> bool:
        handlers: Iterable[actions.ActionAndHandler] = dfs_mixins(self._data,
                                                                  lambda x: _get(x.actions, result.action.id, None))
        for handler in handlers:
            if handler is None:
                continue
            bound_handler = actions.bind_reference_name_to_entity_action_handler(
                reference_name, handler.handler)
            if actions.invoke_handler(self, bound_handler, result):
                return True
        return False

    def handle_event(self, event: events.Event) -> bool:
        handlers: Iterable[events.EventHandler] = dfs_mixins(self._data,
                                                             lambda x: _get(x.event_handlers, event.type, None))
        handler = first(handlers, None)
        if handler:
            return handler(self, event)
        return False

    def _can_relate(self, rel_data: relshp.RelationshipData, new_entity: Optional[Entity]) -> bool:
        handlerses: Iterable[relshp.RelationshipDataAndHandlers] = dfs_mixins(self._data,
                                                                              lambda x: _get(x.relationship_handlers, rel_data.id, None))
        for rel_handlers in handlerses:
            if rel_handlers is None or rel_handlers.handlers.can_relate is None:
                continue
            if not rel_handlers.handlers.can_relate(self, rel_data, new_entity):
                return False
        return True

    def _on_relate(self, rel_data: relshp.RelationshipData, new_entity: Optional[Entity]) -> None:
        handlerses: Iterable[relshp.RelationshipDataAndHandlers] = dfs_mixins(self._data,
                                                                              lambda x: _get(x.relationship_handlers, rel_data.id, None))
        for rel_handlers in handlerses:
            if rel_handlers is None or rel_handlers.handlers.on_relate is None:
                continue
            rel_handlers.handlers.on_relate(self, rel_data, new_entity)

    def _relate_raw(self, rel_data: relshp.RelationshipData,
                    new_entity: Optional[Entity],
                    properties: PropertiesSpec) -> relshp.Relationship:
        if rel_data.id in self._relationships:
            rels = self._relationships[rel_data.id]
        else:
            rels = []
            self._relationships[rel_data.id] = rels
        rel = relshp.Relationship(rel_data, self, new_entity, properties)
        rels.append(rel)
        return rel

    def relate(self, rel_data: relshp.RelationshipData,
               new_entity: Optional[Entity],
               properties: PropertiesSpec = {}) -> Optional[relshp.Relationship]:
        if (not rel_data.can_relate(self, new_entity) or
                not self._can_relate(rel_data, new_entity)):
            return None

        reverse_data = rel_data.reverse
        if reverse_data is not None and new_entity is not None:
            if (not reverse_data.can_relate(new_entity, self) or
                    not new_entity._can_relate(reverse_data, self)):
                return None
            if reverse_data.arity is relshp.ARITY_ONE:
                existing = new_entity.get_relationships(reverse_data)
                old = first(existing, None)
                if old is not None:
                    if not new_entity.unrelate(old, self):
                        return None

        if rel_data.arity is relshp.ARITY_ONE:
            existing = self.get_relationships(rel_data)
            old = first(existing, None)
            if old is not None:
                if not self.unrelate(old, new_entity):
                    return None

        rel_data.on_relate(self, new_entity)
        self._on_relate(rel_data, new_entity)
        if reverse_data is not None and new_entity is not None:
            reverse_data.on_relate(new_entity, self)
            new_entity._on_relate(reverse_data, self)

        rel = self._relate_raw(rel_data, new_entity, properties)
        reverse_rel = None
        if reverse_data is not None and new_entity is not None:
            reverse_rel = new_entity._relate_raw(
                reverse_data, self, properties)
        if reverse_rel is not None:
            rel.reverse = reverse_rel
            reverse_rel.reverse = rel
        return rel

    def _can_unrelate(self, rel: relshp.Relationship, next_entity: Optional[Entity] = None) -> bool:
        handlerses: Iterable[relshp.RelationshipDataAndHandlers] = dfs_mixins(self._data,
                                                                              lambda x: _get(x.relationship_handlers, rel.data.id, None))
        for rel_handlers in handlerses:
            if not rel_handlers or rel_handlers.handlers.can_unrelate is None:
                continue
            if not rel_handlers.handlers.can_unrelate(rel, next_entity):
                return False
        return True

    def _on_unrelate(self, rel: relshp.Relationship, next_entity: Optional[Entity] = None) -> None:
        handlerses: Iterable[relshp.RelationshipDataAndHandlers] = dfs_mixins(self._data,
                                                                              lambda x: _get(x.relationship_handlers, rel.data.id, None))
        for rel_handlers in handlerses:
            if not rel_handlers or not rel_handlers.handlers.on_unrelate:
                continue
            rel_handlers.handlers.on_unrelate(rel, next_entity)

    def _unrelate_raw(self, rel: relshp.Relationship) -> None:
        rels = self._relationships[rel.data.id]
        for existing_rel in rels:
            if existing_rel is rel:
                rels.remove(rel)
                break

    def unrelate(self, rel: relshp.Relationship, next_entity: Optional[Entity] = None) -> bool:
        if (not rel.can_unrelate(next_entity) or
                not self._can_unrelate(rel, next_entity)):
            return False

        reverse_rel = rel.reverse
        if reverse_rel is not None:
            if (not reverse_rel.can_unrelate(next_entity) or
                    not reverse_rel.from_entity._can_unrelate(reverse_rel)):
                return False

        rel.data.on_unrelate(rel, next_entity)
        rel.from_entity._on_unrelate(rel, next_entity)
        if reverse_rel is not None:
            reverse_rel.data.on_unrelate(reverse_rel, next_entity)
            reverse_rel.from_entity._on_unrelate(reverse_rel, next_entity)

        self._unrelate_raw(rel)
        if reverse_rel is not None:
            reverse_rel.from_entity._unrelate_raw(reverse_rel)

        return True

    def get_relationships(self, rel_data: relshp.RelationshipData) -> Sequence[relshp.Relationship]:
        if rel_data.id not in self._relationships:
            return []

        return self._relationships[rel_data.id]

    def get_related(self, rel_data: relshp.RelationshipData) -> Sequence[Entity]:
        entities: MutableSequence[Entity] = []
        rels = self.get_relationships(rel_data)
        for rel in rels:
            if rel.to_entity is not None:
                entities.append(rel.to_entity)
        return entities

    def matches(self, adjective: Optional[str], noun: Optional[str]) -> bool:
        if adjective is not None and len(adjective) < 1:
            if adjective not in self.adjectives:
                return False
        if noun:
            if noun in self.nouns:
                return True
        else:
            return True
        return False

    def __repr__(self) -> str:
        return (f"{self.__class__.__name__}(id={repr(self.id)}, counter={self.counter}")

    def __str__(self) -> str:
        return f"<{repr(self)}>"

    @property
    def counter(self) -> int:
        return self._counter

    @property
    def id(self) -> str:
        return self._data.id

    @property
    def noun(self) -> str:
        return first(self.nouns, "") or ""

    @property
    def quick_raw(self) -> Optional[mddom.Span]:
        def get_value(x: EntityData) -> Optional[mddom.Span]:
            return evaluate_dynamic_span(self, x.description.quick) or None

        return first(dfs_mixins(self._data, get_value), None)

    @property
    def quick(self) -> mddom.Span:
        def get_rendered(x: EntityData) -> Optional[mddom.Span]:
            return evaluate_dynamic_span(self, x.description.quick_renderer) or None

        desc = first(dfs_mixins(self._data, get_rendered), None)
        if desc:
            return desc

        desc = self.quick_raw

        if desc:
            return desc

        return a(self)

    @property
    def situ_raw(self) -> Optional[mddom.Span]:
        def get_value(x: EntityData) -> Optional[mddom.Span]:
            return evaluate_dynamic_span(self, x.description.situ) or None

        return first(dfs_mixins(self._data, get_value), None)

    @property
    def situ(self) -> mddom.Span:
        def get_rendered(x: EntityData) -> Optional[mddom.Span]:
            return evaluate_dynamic_span(self, x.description.situ_renderer) or None

        desc = first(dfs_mixins(self._data, get_rendered), None)
        if desc:
            return desc

        desc = self.situ_raw

        if desc:
            return desc

        return mddom.s("You see ", self.quick, " here.")

    @property
    def detail_raw(self) -> Optional[mddom.Block]:
        def get_value(x: EntityData) -> Optional[mddom.Block]:
            return evaluate_dynamic_block(self, x.description.detail) or None

        return first(dfs_mixins(self._data, get_value), None)

    @property
    def detail(self) -> mddom.Block:
        def get_rendered(x: EntityData) -> Optional[mddom.Block]:
            return evaluate_dynamic_block(self, x.description.detail_renderer) or None

        desc = first(dfs_mixins(self._data, get_rendered), None)
        if desc:
            return desc

        desc = self.detail_raw

        if desc:
            return desc

        return mddom.p(mddom.s("It's ", self.quick))

    @property
    def prefers_situ(self) -> bool:
        explicit = first(dfs_mixins(
            self._data, lambda x: x.description.prefers_situ), None)
        if explicit is not None:
            return explicit
        # TODO: Precalculate.
        if self.situ_raw:
            return True
        return False

    @property
    def adjectives(self) -> StringSequence:
        def get_value(x: EntityData) -> StringSequence:
            return evaluate_dynamic_string_sequence(self, x.adjectives)

        result: MutableStringSequence = []
        for seq in dfs_mixins(self._data, get_value):
            for s in seq:
                if s not in result:
                    result.append(s)
        return result

    @property
    def nouns(self) -> StringSequence:
        def get_value(x: EntityData) -> StringSequence:
            return evaluate_dynamic_string_sequence(self, x.aliases)

        result: MutableStringSequence = []
        for seq in dfs_mixins(self._data, get_value):
            for s in seq:
                if s not in result:
                    result.append(s)
        return result

    @property
    def plurals(self) -> StringSequence:
        return [pluralize(x) for x in self.nouns]

    @property
    def properties(self) -> Properties:
        return self._properties


def evaluate_dynamic_string(entity: Entity,
                            value: Optional[DynamicString]) -> Optional[str]:
    if value is None:
        return None
    if callable(value):
        return value(entity)
    return value


def evaluate_dynamic_node(entity: Entity,
                          value: Optional[DynamicNodeish]) -> Optional[mddom.Node]:
    if value is None:
        return None
    if callable(value):
        value = value(entity)
    if isinstance(value, mddom.Node):
        return value
    return mddom.Text(value)


def evaluate_dynamic_span(entity: Entity,
                          value: Optional[DynamicSpanish]) -> Optional[mddom.Span]:
    if value is None:
        return None
    if callable(value):
        value = value(entity)
    if isinstance(value, mddom.Span):
        return value
    return mddom.sp(value)


def evaluate_dynamic_block(entity: Entity,
                           value: Optional[DynamicBlockish]) -> Optional[mddom.Block]:
    if value is None:
        return None
    if callable(value):
        value = value(entity)
    if isinstance(value, mddom.Block):
        return value
    return mddom.p(value)


def evaluate_dynamic_string_sequence(entity: Entity,
                                     value: Optional[DynamicStringSequence],
                                     passed_result: Optional[MutableStringSequence] = None) -> StringSequence:
    if value is None:
        if passed_result is None:
            return []
        return passed_result

    def get_result() -> MutableStringSequence:
        if passed_result is not None:
            return passed_result
        return []

    result = get_result()

    def add(s: str) -> None:
        if not s:
            return
        if s not in result:
            result.append(s)

    if callable(value):
        for x in value(entity):
            add(x)
        return result

    if isinstance(value, str):
        add(value)
        return result

    for element in value:
        if isinstance(element, str):
            result.append(element)
            continue
        if callable(element):
            subset_or_string = element(entity)
            if isinstance(subset_or_string, str):
                add(subset_or_string)
                continue
            for subelement in subset_or_string:
                add(subelement)
            continue
        raise TypeError("Unexpected type in DynamicStringSequence: "
                        f"{element.__class__.__name__}")
    return result


def find_data(id: str) -> Optional[EntityData]:
    if id not in _ENTITY_DATA:
        return None
    return _ENTITY_DATA[id]


def find_entities(id: str) -> list[Entity]:
    if id not in _ENTITIES:
        return []
    return list(_ENTITIES[id])


def find_entity(id: str) -> Optional[Entity]:
    entities = find_entities(id)
    if not entities:
        return None
    return entities[0]


def find_entity_by_counter(counter: int) -> Optional[Entity]:
    if counter not in _ENTITIES_BY_COUNTER:
        return None
    return _ENTITIES_BY_COUNTER[counter]


def all_entity_data() -> Iterable[EntityData]:
    return _ENTITY_DATA.values()


def all_entities() -> Iterable[list[Entity]]:
    return _ENTITIES.values()


def entity(
    id: str,
    description: DescriptionData = NO_DESCRIPTION,
    adjectives: DynamicStringSequence = [],
    aliases: DynamicStringSequence = [],
    mixins: Sequence[EntityDataSpecification] = [],
    relationships: relshp.RelationshipsSpecification = {},
    relationship_handlers: relshp.RelationshipHandlersSpecification = {},
    on_initialize: Optional[EntityClosure] = None,
    on_action_ready: Optional[EntityClosure] = None,
    actions: ActionHandlersSpecification = {},
    properties: PropertiesSpec = {},
    event_handlers: events.EventHandlers = {},
    instantiate: bool = False,
) -> EntityData:
    return EntityData(
        id=id,
        description=description,
        adjectives=adjectives,
        aliases=aliases,
        mixins=mixins,
        relationships=relationships,
        relationship_handlers=relationship_handlers,
        on_initialize=on_initialize,
        on_action_ready=on_action_ready,
        actions=actions,
        properties=properties,
        event_handlers=event_handlers,
        should_instantiate=instantiate,
    )


class EntityReferenceSearch(object):
    def __init__(self, id: str, func: EntitySearch) -> None:
        self.id = id
        self.func = func


class EntityReferenceFilter(object):
    def __init__(self, id: str, func: EntityFilter) -> None:
        self.id = id
        self.func = func


_ENTITY_REFERENCE_SEARCHES: dict[str, EntityReferenceSearch] = {}
_ENTITY_REFERENCE_FILTERS: dict[str, EntityReferenceFilter] = {}


def entity_search(id: str, func: EntitySearch) -> EntityReferenceSearch:
    global _ENTITY_REFERENCE_SEARCHES
    global _ENTITY_REFERENCE_FILTERS
    if id in _ENTITY_REFERENCE_SEARCHES:
        raise Exception(f"\"{id}\" is already a search")
    if id in _ENTITY_REFERENCE_FILTERS:
        raise Exception(f"\"{id}\" is already a filter")
    s = EntityReferenceSearch(id=id, func=func)
    _ENTITY_REFERENCE_SEARCHES[id] = s
    return s


def entity_filter(id: str, func: EntityFilter) -> EntityReferenceFilter:
    global _ENTITY_REFERENCE_SEARCHES
    global _ENTITY_REFERENCE_FILTERS
    if id in _ENTITY_REFERENCE_SEARCHES:
        raise Exception(f"\"{id}\" is already a search")
    if id in _ENTITY_REFERENCE_FILTERS:
        raise Exception(f"\"{id}\" is already a filter")
    f = EntityReferenceFilter(id=id, func=func)
    _ENTITY_REFERENCE_FILTERS[id] = f
    return f


def is_entity_search(id: str) -> bool:
    global _ENTITY_REFERENCE_SEARCHES
    return id in _ENTITY_REFERENCE_SEARCHES


def is_entity_filter(id: str) -> bool:
    global _ENTITY_REFERENCE_FILTERS
    return id in _ENTITY_REFERENCE_FILTERS


def get_entity_search(id: str) -> Optional[EntityReferenceSearch]:
    global _ENTITY_REFERENCE_SEARCHES
    return _ENTITY_REFERENCE_SEARCHES.get(id, None)


def get_entity_filter(id: str) -> Optional[EntityReferenceFilter]:
    global _ENTITY_REFERENCE_FILTERS
    return _ENTITY_REFERENCE_FILTERS.get(id, None)


_actor: Optional[Entity] = None


def set_actor(actor: Entity) -> None:
    global _actor
    _actor = actor


def get_actor() -> Optional[Entity]:
    global _actor
    return _actor


_player_data: Optional[EntityData] = None
_player: Optional[Entity] = None


def set_player_data(player_data: EntityData) -> None:
    global _player_data
    _player_data = player_data


def get_player_data() -> Optional[EntityData]:
    global _player_data
    return _player_data


def set_player(player: Entity) -> None:
    global _player
    _player = player


def get_player() -> Optional[Entity]:
    global _player
    return _player


def _reset():
    global _ENTITY_DATA
    global _ENTITIES
    _ENTITY_DATA.clear()
    _ENTITIES.clear()

    global _ENTITY_REFERENCE_SEARCHES
    global _ENTITY_REFERENCE_FILTERS
    _ENTITY_REFERENCE_SEARCHES = {}
    _ENTITY_REFERENCE_FILTERS = {}

    global _actor
    _actor = None

    global _player_data
    _player_data = None

    global _player
    _player = None


add_reset_hook(_reset)
