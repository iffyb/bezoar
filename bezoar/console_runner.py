import asyncio
import logging

import bezoar.runtime.engine
from bezoar.base.environment import IS_PYODIDE
from bezoar.base.text import html_escape, normalize_paragraphs
from bezoar.console.console import Console
from bezoar.mddom import AnsiTerminalRenderer, HtmlRenderer, Nodeish, Processor
from bezoar.model import find_entity_by_counter

_stop = False


async def main_loop() -> None:
    console = Console()

    if IS_PYODIDE:
        renderer: Processor = HtmlRenderer()
    else:
        renderer = AnsiTerminalRenderer()

    def emit(node: Nodeish) -> None:
        if isinstance(node, str):
            # TODO: Parse as IF-Enhanced MarkDown, whatever that is.
            text = normalize_paragraphs(node.strip())
            if IS_PYODIDE:
                text = html_escape(text)
            console.print(text + "\n")
            renderer.reset()
            return

        renderer.process(node)
        console.print(renderer.buffer)
        renderer.clear()

    def referrant_callback(referrant: str) -> None:
        counter = int(referrant)
        e = find_entity_by_counter(counter)
        if not e:
            return
        console.inject_command(f"Look at the {e.noun}.")

    console.set_referrant_callback(referrant_callback)

    await bezoar.runtime.engine.start(emit, stop)
    global _stop
    while not _stop:
        command = await console.read_line("→\u00A0")
        renderer.reset()
        if not command:
            continue

        await bezoar.runtime.engine.submit_command_async(command)


def stop() -> None:
    global _stop
    _stop = True


def run() -> int:
    if IS_PYODIDE:
        asyncio.create_task(main_loop())
    else:
        asyncio.run(main_loop())
    return 0


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    exit(run())
