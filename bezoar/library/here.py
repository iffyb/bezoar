from typing import Iterable, Optional
from bezoar.base.itertools import first

from bezoar.library.containers import get_contents, get_environment
from bezoar.model import Entity, entity_search, Restriction


def _have_search_helper(source: Entity, entities: list[Entity]) -> None:
    entities.append(source)
    for e in get_contents(source):
        _have_search_helper(e, entities)


def _have_search(source: Entity) -> Iterable[Entity]:
    entities: list[Entity] = []
    _have_search_helper(source, entities)
    return entities


def _here_search(source: Entity) -> Iterable[Entity]:
    env = get_environment(source)
    if not env:
        return _have_search(source)
    return _have_search(env)


def _self_search(source: Entity) -> Iterable[Entity]:
    return [source]


entity_search("have", _have_search)
entity_search("here", _here_search)
entity_search("self", _self_search)


_HERE_RESTRICTION: Optional[Restriction] = None


def _get_here_restriction() -> Restriction:
    global _HERE_RESTRICTION
    if not _HERE_RESTRICTION:
        _HERE_RESTRICTION = Restriction("here")
    return _HERE_RESTRICTION


def find_here(source: Entity, adjective: Optional[str], noun: Optional[str]) -> Iterable[Entity]:
    return _get_here_restriction().find(source, adjective, noun)


def find_first_here(source: Entity, adjective: Optional[str], noun: Optional[str]) -> Optional[Entity]:
    return first(find_here(source, adjective, noun), None)
