from __future__ import annotations

from typing import List, Optional
from bezoar._model.entities import EntityClosure

from bezoar.library.takeables import takeable as takeable_mixin
from bezoar.library.visibles import visible as visible_mixin
from bezoar.model import (ActionHandlersSpecification, DescriptionData,
                          DynamicStringSequence, EntityData,
                          EntityDataSpecification, EventHandlers,
                          PropertiesSpec, RelationshipHandlersSpecification,
                          RelationshipsSpecification, description, entity)


def item(
    id: str,
    description: DescriptionData = description(),
    adjectives: DynamicStringSequence = [],
    aliases: DynamicStringSequence = [],
    mixins: List[EntityDataSpecification] = [],
    relationships: RelationshipsSpecification = {},
    relationship_handlers: RelationshipHandlersSpecification = {},
    on_initialize: Optional[EntityClosure] = None,
    actions: ActionHandlersSpecification = {},
    properties: PropertiesSpec = {},
    event_handlers: EventHandlers = {},
) -> EntityData:
    full_mixins = list(mixins)
    full_mixins.extend([takeable_mixin, visible_mixin, "describable"])
    return entity(
        id=id,
        description=description,
        adjectives=adjectives,
        aliases=aliases,
        mixins=full_mixins,
        relationships=relationships,
        relationship_handlers=relationship_handlers,
        on_initialize=on_initialize,
        actions=actions,
        properties=properties,
        event_handlers=event_handlers,
        instantiate=True,
    )
