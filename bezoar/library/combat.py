from __future__ import annotations

from typing import Optional

import bezoar.model as model
from bezoar.base.typed_number import TypedNumber

COMBAT_STATISTICS_PROPERTY = "combat_statistics"


class CombatState(TypedNumber):
    """
    States that a Combat Actor can be in.
    """

    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


class CombatStates:
    """
    Initial state. The combatant is not in combat, does not have a target.
    """
    IDLE = CombatState(1, "idle")

    """
    The combatant has a potential target, but is watching to see what happens.
    """
    WARY = CombatState(2, "wary")

    """
    The combatant is chasing a target.
    """
    CHASING = CombatState(3, "chasing")

    """
    The combatant is actively fighting a target.
    """
    ENGAGED = CombatState(4, "engaged")

    """
    The combatant is actively fleeing a target.
    """
    FLEEING = CombatState(5, "fleeing")

    """
    The combatant is hiding from a target.
    """
    HIDING = CombatState(6, "hiding")


class CombatStatistics(object):
    def __init__(self, pstr: int, pdex: int, pend: int, mstr: int, mdex: int,
                 mend: int, speed: int) -> None:
        self.physical_strength = pstr
        self.physical_dexterity = pdex
        self.physical_endurance = pend
        self.mental_strength = mstr
        self.mental_dexterity = mdex
        self.mental_endurance = mend
        self.speed = speed
        self.target: Optional[model.Entity] = None
        self.state = CombatStates.IDLE


combatant = model.entity(
    id="combatant",
    mixins=["actor"],
)
