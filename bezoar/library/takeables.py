import bezoar.model as model


takeable = model.entity(
    id="takeable")


def is_takeable(entity: model.Entity) -> bool:
    return entity.isa(takeable)
