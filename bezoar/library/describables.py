from __future__ import annotations

from bezoar.library.containers import get_content_description, container
from bezoar.mddom import Blockish, Spanish, bl, p, sp
from bezoar.model import Entity, description, entity


def _describable_situ_renderer(self: Entity) -> Spanish:
    raw = self.situ_raw
    result = sp(raw)
    if self.isa(container):
        result.children.append(get_content_description(self))
    return result


def _describable_detail_renderer(self: Entity) -> Blockish:
    raw = self.detail_raw
    result = bl(raw)
    if self.isa("container"):
        result.children.append(p(get_content_description(self)))
    return result


describable = entity(
    id="describable",
    description=description(
        situ_renderer=_describable_situ_renderer,
        detail_renderer=_describable_detail_renderer,
    ),
)
