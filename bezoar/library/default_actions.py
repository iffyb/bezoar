from __future__ import annotations

import bezoar.library.describables
import bezoar.library.here
import bezoar.library.visibles
from bezoar._model.actions import set_action_broadcast
from bezoar._model.schedulers import (SCHEDULER_AVFX, SCHEDULER_GAME,
                                      SCHEDULER_REAL, get_scheduler)
from bezoar.base.itertools import E, first
from bezoar.base.time import seconds
from bezoar.library.containers import (container, get_content_description,
                                       get_contents, get_environment, move)
from bezoar.library.locations import (ExitStateHandled, get_exit, is_location,
                                      location_mixin)
from bezoar.library.takeables import is_takeable
from bezoar.library.visibles import is_visible
from bezoar.mddom import f, h3, l, p, s, sp, text_list
from bezoar.model import (PHASE_ISSUE, PHASE_TRIGGER, RESULT_ACTIVATE,
                          RESULT_DEACTIVATE, ActionResultSpec, Entity,
                          HandlerResult, ParseResult, action, entity_filter,
                          the, verb_synonyms)


def _container_filter(source: Entity, target: Entity) -> bool:
    return target.isa(container)


def _surface_filter(source: Entity, target: Entity) -> bool:
    return False


def _takeable_filter(source: Entity, target: Entity) -> bool:
    return is_takeable(target)


entity_filter("container", _container_filter)
entity_filter("surface", _surface_filter)
entity_filter("takeable", _takeable_filter)


def _look(loc: Entity, actor: Entity) -> None:
    frag = f(
        h3(loc.quick),
        loc.detail,
    )

    contents = get_contents(loc)
    quick: list[Entity] = []
    situ: list[Entity] = []
    for x in contents:
        if not is_visible(x):
            continue
        if x.prefers_situ:
            situ.append(x)
        else:
            quick.append(x)

    if situ:
        for x in situ:
            desc = x.situ
            if desc:
                frag.children.append(p(desc))

    if quick:
        frag.children.append(
            p(s("You see ", text_list([x.quick for x in quick]), " here")))

    actor.tell(frag)


def _action_look(self: Entity, result: ParseResult) -> bool:
    loc = get_environment(self)
    if not loc:
        result.actor.tell("You are nowhere! <This indicates a bug.>")
        return True
    if not loc.isa(location_mixin):
        result.actor.tell("You are not in a Location! <This indicates a bug.>")
        return True

    _look(loc, result.actor)
    return True


def _action_look_at(self: Entity, result: ParseResult) -> bool:
    directs = result.objects["direct"]
    if not directs:
        result.actor.tell("Look at what?")
        return False

    if len(directs) == 1:
        result.actor.tell(
            directs.__iter__().__next__().detail)
        return True

    for d in directs:
        result.actor.tell(f"{d.noun}: {d.detail}")

    return True


def _action_move(self: Entity, result: ParseResult) -> ActionResultSpec:
    if not result.direction:
        result.actor.tell("Where do you want to go?")
        return True
    loc = get_environment(result.actor)
    if loc is None:
        result.actor.tell("You are nowhere! <This indicates a bug.>")
        return True
    if not is_location(loc):
        result.actor.tell("You are not in a location! <This indicates a bug.>")
        return True
    destination = get_exit(loc, result.direction, result.actor)
    if isinstance(destination, ExitStateHandled):
        # assuming HANDLED for now
        return True
    if destination is None:
        result.actor.tell("You can't go that way.")
        return True
    if result.phase.__eq__(PHASE_ISSUE):
        return HandlerResult(RESULT_ACTIVATE, seconds(1))

    if result.phase.__eq__(PHASE_TRIGGER):
        if not move(result.actor, destination):
            return False
        new_loc = get_environment(result.actor)
        if new_loc is not None:
            _look(new_loc, result.actor)
        return HandlerResult(RESULT_DEACTIVATE, seconds(1))
    return False


def _action_get(self: Entity, result: ParseResult) -> bool:
    directs = result.objects["direct"]
    if not directs:
        result.actor.tell("What do you want to get?")
        return True

    show_prefix = len(directs) > 1
    for obj in directs:
        if show_prefix:
            prefix = sp(obj.noun, ": ")
        else:
            prefix = sp()
        success = False
        if is_takeable(obj):
            success = move(obj, result.actor)
        if success:
            result.actor.tell(l(s(prefix, "Taken")))
        else:
            result.actor.tell(l(s(prefix, "You can't pick that up")))
    return True


def _action_put_on(self: Entity, result: ParseResult) -> bool:
    actor = result.actor
    env = get_environment(actor)
    if env is None:
        result.actor.tell("You can't put anything anywhere when nowhere. "
                          "<This indicates a bug.>")
        return True

    indirects = result.objects["destination"]
    destination = first(indirects, None)
    if not destination:
        actor.tell(f"I don't understand what you want to put things on.")
        return True

    if True:  # not destination.isa(surface):
        actor.tell(s("You can't put anything on ", the(destination)))
        return True

    sources = result.objects["sources"]
    show_prefix = len(sources) > 1
    for source in sources:
        if show_prefix:
            prefix = sp(source.noun, ": ")
        else:
            prefix = sp()
        if get_environment(source) != actor:
            actor.tell(s(prefix, "You don't have ", source.the))
        else:
            success = move(source, destination)
            if success:
                actor.tell(l(s(prefix, "Done")))
            else:
                actor.tell(l(s(prefix, "You can't put ", source.the, " on ",
                           destination.the)))
    return True


def _action_put_in(self: Entity, result: ParseResult) -> bool:
    actor = result.actor
    directs = result.objects["direct"]
    indirects = result.objects["indirect"]
    if len(indirects) > 1:
        actor.tell(f"You can't put into multiple objects.")
        return True

    destination = first(indirects, None)
    if not destination:
        actor.tell(f"I don't understand what you want to put things into.")
        return True

    if not destination.isa(container):
        actor.tell(s("You can't put anything into ", the(destination)))
        return True

    show_prefix = len(directs) > 1
    for obj in directs:
        if show_prefix:
            prefix = sp(obj.noun, ": ")
        else:
            prefix = sp()
        if get_environment(obj) != actor:
            actor.tell(s(prefix, "You don't have ", the(obj)))
        else:
            success = move(obj, destination)
            if success:
                actor.tell(l(s(prefix, "Done")))
            else:
                actor.tell(l(s(prefix, "You can't put ", the(obj), " into ",
                           the(destination))))
    return True


def _action_drop(self: Entity, result: ParseResult) -> bool:
    actor = result.actor
    env = get_environment(actor)
    if not env:
        result.actor.tell("You can't drop anything when nowhere. "
                          "<This indicates a bug.>")
        return False
    directs = result.objects["direct"]
    show_prefix = len(directs) > 1
    for obj in directs:
        if show_prefix:
            prefix = sp(obj.noun, ": ")
        else:
            prefix = sp()
        if get_environment(obj) != actor:
            actor.tell(s(prefix, "You don't have ", the(obj)))
        else:
            success = move(obj, env)
            if success:
                actor.tell(l(s(prefix, "Dropped")))
            else:
                actor.tell(l(s(prefix, "You can't drop ", the(obj))))
    return True


def _action_give(self: Entity, result: ParseResult) -> bool:
    return False


def _action_inventory(self: Entity, result: ParseResult) -> bool:
    contents = get_contents(self)
    message = sp()
    if not contents:
        message.children.append(s("You don't have anything."))
    else:
        message.children.append(s("You are carrying ", text_list(
            [x.quick for x in contents])))

        for obj in contents:
            if get_contents(obj):
                message.children.append(get_content_description(obj))
    result.actor.tell(message)
    return True


def _action_time(self: Entity, result: ParseResult) -> bool:
    result.actor.tell(f"real: {get_scheduler(SCHEDULER_REAL).now()}")
    result.actor.tell(f"avfx: {get_scheduler(SCHEDULER_AVFX).now()}")
    result.actor.tell(f"game: {get_scheduler(SCHEDULER_GAME).now()}")
    return True


action(id="time_debug",
       syntaxes=["time"],
       handler=_action_time,
       )


move_action = action(id="move",
                     syntaxes=[
                         "{direction}",
                         "go {direction}",
                         "move {direction}",
                         "travel {direction}",
                         "walk {direction}",
                     ],
                     handler=_action_move)

get_action = action(id="get",
                    syntaxes=[
                        "get {direct+: here and takeable}",
                        "take {direct+: here and takeable}",
                        "acquire {direct+: here and takeable}",
                        "procure {direct+: here and takeable}",
                        "obtain {direct+: here and takeable}",
                        "pick up {direct+: here and takeable}",
                        "pick {direct+: here and takeable} up",
                        "get {direct+: here and takeable} from {indirect+: here and container}",
                        "take {direct+: here and takeable} from {indirect+: here and container}",
                        "acquire {direct+: here and takeable} from {indirect+: here and container}",
                        "procure {direct+: here and takeable} from {indirect+: here and container}",
                        "obtain {direct+: here and takeable} from {indirect+: here and container}",
                        "pick up {direct+: here and takeable} from {indirect+: here and container}",
                        "pick {direct+: here and takeable} up from {indirect+: here and container}",
                        "from {indirect+: here and container} [get] {direct+}",
                        "from {indirect+: here and container} [take] {direct+}",
                        "from {indirect+: here and container} [acquire] {direct+}",
                        "from {indirect+: here and container} [procure] {direct+}",
                        "from {indirect+: here and container} [obtain] {direct+}",
                        "from {indirect+: here and container} [pick] up {direct+}",
                    ],
                    handler=_action_get)

drop_action = action(id="drop",
                     syntaxes=[
                         "drop {direct+: have}",
                         "put down {direct+: have}",
                         "put {direct+: have} down",
                     ],
                     handler=_action_drop)

put_on_action = action(id="put_on",
                       syntaxes=[
                           "put {sources+: have} on {destination: here and surface}",
                           "place {sources+: have} on {destination: here and surface}",
                           "set {sources+: have} on {destination: here and surface}",
                           "put {sources+: have} onto {destination: here and surface}",
                           "place {sources+: have} onto {destination: here and surface}",
                           "set {sources+: have} onto {destination: here and surface}",
                           "put {sources+: have} on top of {destination: here and surface}",
                           "place {sources+: have} on top of {destination: here and surface}",
                           "set {sources+: have} on top of {destination: here and surface}",
                       ],
                       handler=_action_put_on)

put_in_action = action(id="put_in",
                       syntaxes=[
                           "put {direct+: have} in {indirect: here and container}",
                           "place {direct+: have} in {indirect: here and container}",
                           "set {direct+: have} in {indirect: here and container}",
                           "put {direct+: have} into {indirect: here and container}",
                           "place {direct+: have} into {indirect: here and container}",
                           "set {direct+: have} into {indirect: here and container}",
                       ],
                       handler=_action_put_in)

give_action = action(id="give",
                     syntaxes=[
                         "give {direct+: have} to {indirect: here and actor}",
                         "hand {direct+: have} to {indirect: here and actor}",
                     ],
                     handler=_action_give)

look_action = action(id="look",
                     syntaxes=[
                         "look",
                         "look around",
                         "look up",
                         "look down",
                     ],
                     handler=_action_look)

look_at_action = action(id="look_at",
                        syntaxes=[
                            "look at {direct:here}",
                            "examine {direct:here}",
                        ],
                        handler=_action_look_at)

inventory_action = action(id="inventory",
                          syntaxes=[
                              "inventory",
                          ],
                          handler=_action_inventory)

# Pretty good default for action broadcasting.
set_action_broadcast("here")

verb_synonyms({
    "exa": "examine",
    "g": "again",
    "i": "inventory",
    "inv": "inventory",
    "l": "look",
    "q": "quit",
    "x": "examine",
    "z": "wait",
})
