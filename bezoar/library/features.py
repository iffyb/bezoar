from __future__ import annotations

from typing import List, Optional

import bezoar.model as model
from bezoar.library.visibles import visible as visible_mixin


def feature(
    id: str,
    description: model.DescriptionData = model.description(),
    adjectives: model.DynamicStringSequence = [],
    aliases: model.DynamicStringSequence = [],
    mixins: List[model.EntityDataSpecification] = [],
    relationships: model.RelationshipsSpecification = {},
    relationship_handlers: model.RelationshipHandlersSpecification = {},
    on_initialize: Optional[model.EntityClosure] = None,
    actions: model.ActionHandlersSpecification = {},
    properties: model.PropertiesSpec = {},
    event_handlers: model.EventHandlers = {},
    visible: bool = False,
) -> model.EntityData:
    full_mixins = list(mixins)
    if visible:
        full_mixins.append(visible_mixin)
    full_mixins.append("describable")
    return model.entity(
        id=id,
        description=description,
        adjectives=adjectives,
        aliases=aliases,
        mixins=full_mixins,
        relationships=relationships,
        relationship_handlers=relationship_handlers,
        on_initialize=on_initialize,
        actions=actions,
        properties=properties,
        event_handlers=event_handlers,
        instantiate=True,
    )
