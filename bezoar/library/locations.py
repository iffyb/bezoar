from __future__ import annotations

from typing import Callable, Mapping, Optional, Sequence, Union
from bezoar._model.entities import EntityClosure

import bezoar.library.containers as containers
import bezoar.model as model
from bezoar.library.features import feature
from bezoar.model import (ARITY_MANY, TARGET_FLAGS_NONE, TARGET_FLAGS_REPEAT,
                          TARGET_FLAGS_SELF, DescriptionData, DynamicBlockish,
                          Entity, EntityData, EntityDataSpecification,
                          EventHandlers, MutableRelationshipsSpecification,
                          PropertiesSpec, RelationshipHandlersSpecification,
                          RelationshipsSpecification, auto,
                          copy_relationships_specification, copy_spec,
                          description, direction_synonyms, entity, find_data,
                          find_entity, relationship, relationship_value)

# Universal direction abbreviations or synonyms that will automatically be
# applied when typed by the user in a direction context. Can also be used
# equivalently in the declaration syntax.
_DIRECTION_SYNONYMS = {
    "d": "down",
    "e": "east",
    "enter": "in",
    "exit": "out",
    "n": "north",
    "ne": "northeast",
    "nw": "northwest",
    "s": "south",
    "se": "southeast",
    "sw": "southwest",
    "u": "up",
    "w": "west",
}


class ExitStateHandled(object):
    pass


ExitResult = Optional[Union['Entity', ExitStateHandled]]
ExitHandler = Callable[['Entity', str,
                        'Entity'], Optional['Entity']]
LocationDataSpecifcation = Union['EntityDataSpecification',
                                 ExitHandler]
RawExitData = Mapping[str, LocationDataSpecifcation]
QuickFeatures = Mapping[str, 'DynamicBlockish']

_DIRECTION_KEY = "*direction*"
_EXIT_HANDLER_KEY = "*exit_handler*"
_EXIT_RELATIONSHIP_NAME = "exit"
_LOCATION_MIXIN_NAME = "*location*"


location_mixin = entity(
    id=_LOCATION_MIXIN_NAME,
)


def location(
    id: str,
    exits: RawExitData,
    description: DescriptionData = description(),
    mixins: Sequence[EntityDataSpecification] = [],
    relationships: RelationshipsSpecification = {},
    relationship_handlers: RelationshipHandlersSpecification = {},
    on_initialize: Optional[EntityClosure] = None,
    properties: PropertiesSpec = {},
    event_handlers: EventHandlers = {},
    features: QuickFeatures = {},
) -> EntityData:
    mutable_mixins = list(mixins)
    mutable_mixins.append(location_mixin)
    mutable_relationships: MutableRelationshipsSpecification = copy_relationships_specification(
        relationships)
    reldata = containers.contents_relationship
    for spec, descr in copy_spec(features).items():
        names = [x.strip() for x in spec.strip().split("|")]
        if not names:
            raise ValueError(f"Empty feature names: '{spec}'")
        if "" in names:
            raise ValueError(f"Empty feature synonym: '{spec}'")
        if reldata.id not in mutable_relationships:
            mutable_relationships[reldata.id] = []
        mutable_relationships[reldata.id].append(
            feature(
                id=auto(names[0]),
                aliases=names,
                description=model.description(detail=descr),
                visible=False,
            ))
    for dir_spec, exit_spec in exits.items():
        if exit_spec is None or exit_spec == "":
            raise ValueError(f"Invalid Exit Specification for {id}: "
                             f"'{dir_spec}': {exit_spec}")
        dirs = [d.strip() for d in dir_spec.split("|")]
        dirs = [_DIRECTION_SYNONYMS[d]
                if d in _DIRECTION_SYNONYMS else d for d in dirs]
        if _EXIT_RELATIONSHIP_NAME not in mutable_relationships:
            mutable_relationships[_EXIT_RELATIONSHIP_NAME] = []
        exit_list = mutable_relationships[_EXIT_RELATIONSHIP_NAME]
        if callable(exit_spec):
            exit_handler: Optional[ExitHandler] = exit_spec
            exit_target: Optional[EntityDataSpecification] = None
        else:
            exit_handler = None
            exit_target = exit_spec
        for dir in dirs:
            exit_list.append(
                relationship_value(
                    target=exit_target,
                    properties={
                        _DIRECTION_KEY: dir,
                        _EXIT_HANDLER_KEY: exit_handler,
                    }))

    return entity(
        id=id,
        description=description,
        adjectives=[],
        aliases=[],
        mixins=mutable_mixins,
        relationships=mutable_relationships,
        relationship_handlers=relationship_handlers,
        on_initialize=on_initialize,
        actions={},
        properties=properties,
        event_handlers=event_handlers,
        instantiate=True,
    )


def is_location(e: Entity) -> bool:
    return e.isa(location_mixin)


def is_location_data(ed: EntityData) -> bool:
    return ed.isa(location_mixin)


def find_location_data(id: str) -> Optional[EntityData]:
    maybe_location = find_data(id)
    if maybe_location is None:
        return None
    if is_location_data(maybe_location):
        return maybe_location
    return None


def find_location(id: str) -> Optional[Entity]:
    maybe_location = find_entity(id)
    if maybe_location is None:
        return None
    if is_location(maybe_location):
        return maybe_location
    return None


def get_exit(loc: Entity, direction: str, actor: Entity) -> ExitResult:
    for rel in loc.get_relationships(exit_relationship):
        if rel.properties.get(_DIRECTION_KEY, "") != direction:
            continue
        handler: ExitHandler = rel.properties.get(
            _EXIT_HANDLER_KEY, None)
        if callable(handler):
            handler_result = handler(loc, direction, actor)
            if handler_result is None:
                return ExitStateHandled()
            return handler_result
        return rel.to_entity
    return None


direction_synonyms(_DIRECTION_SYNONYMS)

exit_relationship = relationship(
    id=_EXIT_RELATIONSHIP_NAME,
    arity=ARITY_MANY,
    target_flags={
        TARGET_FLAGS_NONE,
        TARGET_FLAGS_REPEAT,
        TARGET_FLAGS_SELF,
    },
)
