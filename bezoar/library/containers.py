from __future__ import annotations

from typing import Optional, Sequence

from bezoar.base.itertools import first
from bezoar.base.time import epoch
from bezoar.mddom import Span, s, text_list
from bezoar.model import (ARITY_MANY, ARITY_ONE, SCHEDULER_GAME, Entity, Event,
                          RelationshipData, Restriction, entity,
                          find_relationship_data, get_scheduler, relationship,
                          the)

container = entity(
    id="container",
    mixins=["describable"],
)

contents_relationship = relationship(
    id="contents",
    arity=ARITY_MANY,
    reverse_id="container",
    reverse_arity=ARITY_ONE,
)

EVENT_ENTERED = "entered"
EVENT_EXITED = "exited"


class MoveEvent(Event):
    def __init__(self, type: str, source: Entity):
        super().__init__(type)
        self.source = source


def _get_contents_relationship_data() -> RelationshipData:
    r = find_relationship_data("container")
    if r is None:
        raise Exception("bug")
    return r


container_relationship: RelationshipData = _get_contents_relationship_data()


def get_environment(obj: Entity) -> Optional[Entity]:
    containers = obj.get_related(container_relationship)
    return first(containers, None)


def get_contents(obj: Entity) -> Sequence[Entity]:
    return obj.get_related(contents_relationship)


_here_restriction: Optional[Restriction] = None


def move(obj: Entity, destination: Entity) -> bool:
    global _here_restriction
    if not _here_restriction:
        _here_restriction = Restriction("here")
    exitees = _here_restriction.find_possibilities(obj)
    for ex in exitees:
        ex.handle_event(MoveEvent(EVENT_EXITED, obj))
    if not destination.relate(contents_relationship, obj):
        return False

    # TODO: This is a hack to have the ENTERED notification occur after the
    # look from a move. Need to structure this better so this isn't necessary.
    restriction = _here_restriction

    def dispatch_entered() -> None:
        enterees = restriction.find_possibilities(obj)
        for en in enterees:
            en.handle_event(MoveEvent(EVENT_ENTERED, obj))
    get_scheduler(SCHEDULER_GAME).schedule_at(epoch(), dispatch_entered)

    return True


def get_content_description(obj: Entity) -> Span:
    contents = get_contents(obj)
    if not contents:
        return Span()

    result = Span()
    result.children.append(s(the(obj), " contains ", text_list(
        [x.quick for x in contents])))

    for cobj in contents:
        content_description = get_content_description(cobj)
        if content_description:
            result.children.append(content_description)
    return result


def _get_short_description_with_contents(obj: Entity) -> Optional[Span]:
    result = Span()
    result.children.append(obj.situ)
    result.children.append(get_content_description(obj))
    return result
