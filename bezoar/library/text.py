from __future__ import annotations

from bezoar.library.here import find_first_here
from bezoar.mddom import Spanish
from bezoar.model import Entity, ref


def vref(e: Entity, term: str) -> Spanish:
    v = find_first_here(e, None, term)
    if not v:
        return term
    return ref(v, term)
