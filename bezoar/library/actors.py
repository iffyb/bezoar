from typing import Optional

import bezoar.model as model
from bezoar.base.scheduler import ScheduledTask
from bezoar.base.time import Time, seconds


EVENT_TICK = "tick"

actor = model.entity(
    id="actor",
)


def is_actor(entity: model.Entity) -> bool:
    return entity.isa(actor)


def _actor_filter(source: model.Entity, target: model.Entity) -> bool:
    return is_actor(target)


_tick_task:Optional[ScheduledTask] = None

def _schedule_tick(next_tick: Time) -> None:
    global _tick_task
    if _tick_task:
        _tick_task.cancel()
    next_next_tick = next_tick + seconds(1)
    def next_tick_function() -> None:
        _repeating_tick(next_next_tick)
    scheduler = model.get_scheduler(model.SCHEDULER_GAME)
    _tick_task = scheduler.schedule_at(next_tick, next_tick_function)

def _repeating_tick(next_tick: Time) -> None:
    global _tick_task
    _tick_task = None

    _schedule_tick(next_tick)
    for en_list in model.all_entities():
        for en in en_list:
            ev = model.Event(EVENT_TICK)
            en.handle_event(ev)

def _schedule_first_tick() -> None:
    scheduler = model.get_scheduler(model.SCHEDULER_GAME)
    next_tick = scheduler.now() + seconds(1)
    _schedule_tick(next_tick)


model.add_start_hook(_schedule_first_tick)

model.entity_filter("actor", _actor_filter)
