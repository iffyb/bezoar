from __future__ import annotations

from bezoar.model import Entity, entity, entity_filter


visible = entity(
    id="visible")


def is_visible(entity: Entity) -> bool:
    return entity.isa(visible)


def _visible_filter(source: Entity, target: Entity) -> bool:
    return is_visible(target)


entity_filter("visible", _visible_filter)
