mkdir "out" -Force | Out-Null
rm -Force -Recurse out/*
Get-ChildItem . -Recurse | Where{$_.Name -Match "__pycache__"} | Remove-Item -Recurse
