mkdir "out" -Force | Out-Null
Push-Location -Path "out"
try {
  python -m http.server
} finally {
  Pop-Location
}
