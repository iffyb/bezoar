from typing import Tuple
from unittest import TestCase, main

from bezoar.mddom import (AnsiTerminalRenderer, Block, Fragment, Image, Node,
                          Processor, Span, Text, b, f, h1, i, p, s)


class Event(object):
    def __init__(self, start: bool, callback: str, node: Node) -> None:
        super().__init__()
        self.start = start
        self.callback = callback
        self.node = node

    def __repr__(self) -> str:
        return f"Event({self.start}, {self.node})"

    def __str__(self) -> str:
        return self.__repr__()


class TestProcessor(Processor):
    def __init__(self) -> None:
        super().__init__()
        self.events: list[Event] = []

    def fragment_start(self, fragment: Fragment) -> None:
        self.events.append(Event(True, "fragment", fragment))

    def fragment_end(self, fragment: Fragment) -> None:
        self.events.append(Event(False, "fragment", fragment))

    def block_start(self, block: Block) -> None:
        self.events.append(Event(True, "block", block))

    def block_end(self, block: Block) -> None:
        self.events.append(Event(False, "block", block))

    def span_start(self, span: Span) -> None:
        self.events.append(Event(True, "span", span))

    def span_end(self, span: Span) -> None:
        self.events.append(Event(False, "span", span))

    def text(self, text: Text) -> None:
        self.events.append(Event(True, "text", text))

    def image(self, image_node: Image) -> None:
        self.events.append(Event(True, "image", image_node))


TEST_REF: object = []
TEST_TREE = f(
    h1(b("Lorem Ipsum")),
    p(
        s(i(b("lorem"), " ipsum dolor sit amet, ",
            "consectetur adipiscing elit, sed do eiusmod tempor incididunt ",
            "ut labore et dolore magna ", b("aliqua"))),
        s("Ut enim ad minim veniam, quis nostrud exercitation ullamco ",
            "laboris nisi ut aliquip ex ea commodo consequat."),
        s("duis aute irure dolor in reprehenderit in voluptate velit esse ",
            "cillum dolore eu fugiat nulla pariatur"),
        s("", "  ", "excepteur sint occaecat cupidatat non proident, sunt in ",
          "culpa qui officia deserunt mollit anim id est laborum", "  ", ""),
    ),
    p(
        s("Sed ut perspiciatis unde omnis iste natus error sit voluptatem ",
          "accusantium doloremque laudantium, totam rem aperiam, eaque ipsa ",
          "quae ab illo inventore veritatis et quasi architecto beatae vitae ",
          "dicta sunt explicabo."),
        s("Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit ",
          "aut fugit, sed quia consequuntur magni dolores eos qui ratione ",
          "voluptatem sequi nesciunt."),
        s("Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, ",
          "consectetur, adipisci velit, sed quia non numquam eius modi tempora ",
          "incidunt ut labore et dolore magnam aliquam quaerat voluptatem."),
        s("Ut enim ad minima veniam, quis nostrum exercitationem ullam ",
          "corporis suscipit laboriosam, nisi ut aliquid ex ea commodi ",
          "consequatur?"),
        s("Quis autem vel eum iure reprehenderit qui in ea voluptate velit ",
          "esse quam nihil molestiae consequatur, vel illum qui dolorem eum ",
          "fugiat quo voluptas nulla pariatur?"),
    ),
)


class MdDomTest(TestCase):
    def check_events(self, events: list[Event], checks: list[Tuple[bool, str, str]]) -> None:
        num_events = len(events)
        num_checks = len(checks)
        n = min(num_events, num_checks)
        for i in range(0, n):
            event = events[i]
            start, callback, class_name = checks[i]
            message = f"[{i}] {event} ?= ({start}, {callback}, {class_name})"
            self.assertEqual(event.start, start, msg=message)
            self.assertEqual(event.callback, callback, msg=message)
            self.assertEqual(event.node.__class__.__name__,
                             class_name, msg=message)

        self.assertGreaterEqual(n, num_checks)

    def test_to_pretty_string(self):
        self.assertRegex(TEST_TREE.to_pretty_string(), r"^\[Fragment")

    def test_get_text(self):
        self.assertRegex(TEST_TREE.get_text().strip(), r"^Lorem Ipsum")
        self.assertRegex(TEST_TREE.get_text(), r" sed do ")

    def test_processor(self):
        processor = TestProcessor()
        processor.process(TEST_TREE)
        self.check_events(processor.events, [
            (True, "fragment", "Fragment"),

            # Header
            (True, "block", "Block"),
            (True, "span", "Span"),
            (True, "text", "Text"),
            (False, "span", "Span"),
            (False, "block", "Block"),

            # Lorem Ipsum Paragraph
            (True, "block", "Block"),
            (True, "span", "Span"),  # s
            (True, "span", "Span"),  # i
            (True, "span", "Span"),  # b
            (True, "text", "Text"),  # "Lorem"
            (False, "span", "Span"),  # /b
            (True, "text", "Text"),
            (True, "span", "Span"),  # b
            (True, "text", "Text"),
            (False, "span", "Span"),  # /b
            (False, "span", "Span"),  # /i
            (False, "span", "Span"),  # /s

            (True, "span", "Span"),
            (True, "text", "Text"),
            (False, "span", "Span"),

            (True, "span", "Span"),
            (True, "text", "Text"),
            (False, "span", "Span"),

            (True, "span", "Span"),
            (True, "text", "Text"),
            (False, "span", "Span"),

            (False, "block", "Block"),
        ])

    def test_ansi_renderer(self):
        renderer = AnsiTerminalRenderer()
        renderer.process(TEST_TREE)
        self.assertRegex(renderer.buffer, r"Lorem Ipsum")
        self.assertRegex(renderer.buffer, r" explicabo.\sNemo ")
        self.assertRegex(renderer.buffer, r". Ut enim ")
        self.assertRegex(renderer.buffer, r"consequat. Duis")
        self.assertRegex(renderer.buffer, r"pariatur. Excepteur")
        self.assertRegex(renderer.buffer, r"laborum.\n")
        self.assertRegex(renderer.buffer, r" nulla pariatur\?")


if __name__ == '__main__':
    main()
