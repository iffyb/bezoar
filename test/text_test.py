from unittest import TestCase, main

from bezoar.base.text import *


class TextTest(TestCase):
    def test_normalize_node(self):
        abcd_cases = [
            "a b c d",
            "a  b c d",
            "a b  c d",
            "a b\tc d",
            "a b\t \tc d",
            "a b \t c d",
        ]

        for case in abcd_cases:
            self.assertEqual("a b c d", normalize_node(case))

        leading_cases = [
            "\ta b c d",
            " a b c d",
            "    a b c d",
            "\t \ta b c d",
            " \t a b c d",
        ]

        for case in leading_cases:
            self.assertEqual(" a b c d", normalize_node(case))

        trailing_cases = [
            "a b c d\t",
            "a b c d ",
            "a b c d    ",
            "a b c d\t \t",
            "a b c d \t ",
        ]

        for case in trailing_cases:
            self.assertEqual("a b c d ", normalize_node(case))


if __name__ == '__main__':
    main()
