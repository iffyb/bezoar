from typing import MutableSequence, Optional
from unittest import TestCase, main

from bezoar.base.algorithms import dfs


class _Node(object):
    def __init__(self, v: Optional[str] = None) -> None:
        super().__init__()
        self.value = v
        self.children: MutableSequence[_Node] = []


class AlgorithmTest(TestCase):
    def test_dfs(self):
        root = _Node("")
        a = _Node("a")
        b = _Node("b")
        c = _Node("c")
        d = _Node("d")
        e = _Node("e")
        f = _Node("f")
        g = _Node("g")
        blank = _Node()
        empty = _Node("")
        root.children.append(a)
        a.children.append(b)
        root.children.append(c)
        root.children.append(d)
        d.children.append(e)
        d.children.append(f)
        root.children.append(g)
        g.children.append(empty)
        g.children.append(blank)

        raw = dfs(root, lambda x: x.value, lambda x: x.children)
        result = list(raw)
        self.assertEqual(9, len(result))
        self.assertEqual("abcdefg", "".join(result))


if __name__ == '__main__':
    main()
