import asyncio
import sys
from types import ModuleType
from typing import Optional
from unittest import TestCase

from bezoar import reset, start
from bezoar.base.reset_module import get_all_referenced_modules
from bezoar.base.text import words
from bezoar.mddom import AnsiTerminalRenderer, Nodeish, sp
from bezoar.runtime.engine import submit_command_async

# Need to reference all modules referenced from __main__ or else things get
# unloaded unintentionally.
_main_modules: list[ModuleType] = []

# Need to maintain a single loop or else infrastructure gets confused.
_event_loop: Optional[asyncio.AbstractEventLoop] = None

class IntegrationTestBase(TestCase):
    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)
        self.model: Optional[ModuleType] = None
        self._model_modules: list[ModuleType] = []
        global _main_modules
        if not _main_modules:
            _main_modules = list(
                get_all_referenced_modules(sys.modules["__main__"]))

    def import_model(self) -> Optional[ModuleType]:
        return None

    def setUp(self):
        global _event_loop
        if not _event_loop:
            _event_loop= asyncio.new_event_loop()
        self.loop = _event_loop
        reset()

        self.model = self.import_model()
        if self.model:
            self._model_modules = list(get_all_referenced_modules(self.model))

        self.log = list[str]()

        def emit(s: Nodeish) -> None:
            if isinstance(s, str):
                s = sp(s)

            renderer = AnsiTerminalRenderer()
            renderer.process(s)
            self.log.extend(renderer.buffer.splitlines())

        def stop() -> None:
            pass

        self.loop.run_until_complete(start(emit, stop))
        self.log.clear()

    def compare_log_to_text(self, log: list[str], text: str) -> None:
        log_copy = [x.strip() for x in log]
        remaining_words = words(text)
        while log_copy and remaining_words:
            while log_copy and not log_copy[0]:
                log_copy = log_copy[1:]

            if not log_copy:
                break

            word = remaining_words[0]
            index = log_copy[0].find(word)
            if index == -1:
                log_copy = log_copy[1:]
                continue
            remaining_words = remaining_words[1:]
            log_copy[0] = log_copy[0][index + len(word):].strip()
        self.assertEqual(len(remaining_words), 0,
                         f"Text was not fully consumed: {' '.join(remaining_words)}")

    def tearDown(self):
        self.loop.run_until_complete(submit_command_async("quit"))
        reset()
        self.loop = None
        del self.model
        for mod in self._model_modules:
            name = mod.__name__
            if name in sys.modules:
                del sys.modules[name]
            else:
                print(f"Module not found: {name}")
                newline = "\n"
                print(f"Main ------\n{newline.join([m.__name__ for m in _main_modules])}")
                print(f"Model ------\n{newline.join([m.__name__ for m in self._model_modules])}")
        self._model_modules = []
