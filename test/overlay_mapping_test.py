from unittest import TestCase, main

from bezoar.base.overlay_mapping import OverlayMapping


class OverlayMappingTest(TestCase):
    def setUp(self):
        super().setUp()

    def tearDown(self):
        super().setUp()

    def test_as_regular_mapping(self):
        sut = OverlayMapping()
        self.assertEqual(0, len(sut))
        sut.__setitem__("test", 1)
        self.assertEqual(1, len(sut))
        sut.__delitem__("test")
        self.assertEqual(0, len(sut))
        sut.__setitem__("test1", 1)
        sut.__setitem__("test2", 2)
        self.assertEqual(2, len(sut))
        sut.clear()
        self.assertEqual(0, len(sut))

    def test_as_overlay_1(self):
        backup = {
            "test1": 1,
            "test2": 2,
        }
        self.assertEqual(2, len(backup))
        sut = OverlayMapping(backup)
        self.assertEqual(2, len(sut))
        sut.__setitem__("test3", 3)
        self.assertEqual(3, len(sut))
        sut.__setitem__("test1", 4)
        self.assertEqual(3, len(sut))
        self.assertEqual(4, sut.get("test1", 0))
        self.assertEqual(2, sut.get("test2", 0))
        self.assertEqual(3, sut.get("test3", 0))
        sut.__delitem__("test2")
        self.assertEqual(2, len(sut))
        self.assertEqual(0, sut.get("test2", 0))

        keys: set[str] = set([])
        for k in sut:
            keys.add(k)
        self.assertEqual(len(keys), len(sut))

    def test_as_overlay_2(self):
        backup1 = {
            "test1": 1,
            "test2": 2,
        }
        backup2 = {
            "test3": 3,
            "test4": 4,
        }
        sut = OverlayMapping(backup1, backup2)
        self.assertEqual(4, len(sut))
        sut.__setitem__("test5", 5)
        self.assertEqual(5, len(sut))
        sut.__setitem__("test1", 6)
        self.assertEqual(5, len(sut))
        self.assertEqual(6, sut.get("test1"))
        self.assertEqual(2, sut.get("test2"))
        self.assertEqual(3, sut.get("test3"))
        self.assertEqual(4, sut.get("test4"))
        sut.__delitem__("test3")
        self.assertEqual(4, len(sut))
        self.assertEqual(-1, sut.get("test3", -1))
        sut.__setitem__("test3", 3)
        self.assertEqual(5, len(sut))
        self.assertEqual(3, sut.get("test3", 3))

        keys: set[str] = set([])
        for k in sut:
            keys.add(k)
        self.assertEqual(len(keys), len(sut))


if __name__ == '__main__':
    main()
