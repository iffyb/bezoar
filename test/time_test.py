from unittest import TestCase, main

from bezoar.base.time import (Duration, Time, epoch, max_duration, max_time,
                              microseconds, milliseconds, nanoseconds, seconds,
                              zero)


class TimeTest(TestCase):
    def test_zero(self):
        z = zero()
        self.assertIsNotNone(z)
        self.assertEqual(0, z.to_nanoseconds())

    def test_nanoseconds(self):
        n = nanoseconds(1)
        self.assertIsNotNone(n)
        self.assertEqual(1 / 1000000000.0, n.to_seconds())
        self.assertEqual(0, n.to_milliseconds())
        self.assertEqual(1, n.to_nanoseconds())

    def test_microseconds(self):
        m = microseconds(1)
        self.assertIsNotNone(m)
        self.assertEqual(1 / 1000000.0, m.to_seconds())
        self.assertEqual(0, m.to_milliseconds())
        self.assertEqual(1000, m.to_nanoseconds())

    def test_milliseconds(self):
        m = milliseconds(1)
        self.assertIsNotNone(m)
        self.assertEqual(1 / 1000.0, m.to_seconds())
        self.assertEqual(1, m.to_milliseconds())
        self.assertEqual(1000000, m.to_nanoseconds())

    def test_seconds(self):
        s = seconds(1)
        self.assertIsNotNone(s)
        self.assertEqual(1, s.to_seconds())
        self.assertEqual(1000, s.to_milliseconds())
        self.assertEqual(1000000000, s.to_nanoseconds())

    def test_max_duration(self):
        d = max_duration()
        self.assertIsNotNone(d)
        dminus = Duration(d.to_nanoseconds() - 1)
        self.assertLess(dminus, d)
        self.assertLess(zero(), d)

    def test_epoch(self):
        e = epoch()
        self.assertIsNotNone(e)
        self.assertEqual(0, e.to_nanoseconds_since_epoch())

    def test_max_time(self):
        t = max_time()
        self.assertIsNotNone(t)
        tminus = Time(t.to_nanoseconds_since_epoch() - 1)
        self.assertLess(tminus, t)
        self.assertLess(epoch(), t)

    def test_add_duration(self):
        d1 = Duration(1)
        d2 = Duration(2)
        d3 = d1.add(d2)
        self.assertIsNotNone(d3)
        self.assertLess(d1, d2)
        self.assertLess(d1, d3)
        self.assertLess(d2, d3)
        self.assertEqual(d1.to_nanoseconds() +
                         d2.to_nanoseconds(), d3.to_nanoseconds())

    def test_add_time(self):
        t1 = Time(1)
        d2 = Duration(2)
        t3 = t1.add(d2)
        self.assertIsNotNone(t3)
        self.assertLess(t1, t3)
        self.assertEqual(t1.to_nanoseconds_since_epoch() +
                         d2.to_nanoseconds(), t3.to_nanoseconds_since_epoch())

    def test_subtract_duration(self):
        d2 = Duration(2)
        d3 = Duration(3)
        d1 = d3.subtract(d2)
        self.assertIsNotNone(d1)
        self.assertLess(d1, d2)
        self.assertLess(d1, d3)
        self.assertLess(d2, d3)
        self.assertEqual(d1.to_nanoseconds() +
                         d2.to_nanoseconds(), d3.to_nanoseconds())

    def test_subtract_duration_from_time(self):
        d2 = Duration(2)
        t3 = Time(3)
        t1 = t3.subtract(d2)
        self.assertIsNotNone(t1)
        self.assertLess(t1, t3)
        self.assertEqual(t1.to_nanoseconds_since_epoch() +
                         d2.to_nanoseconds(), t3.to_nanoseconds_since_epoch())

    def test_duration_comparisons(self):
        z = Duration(0)
        d1 = Duration(1)
        d2 = Duration(2)
        self.assertEqual(z, z)
        self.assertEqual(d1, d1)
        self.assertEqual(d2, d2)
        self.assertLess(z, d1)
        self.assertLess(z, d2)
        self.assertLess(d1, d2)
        self.assertGreater(d2, d1)
        self.assertGreater(d2, z)
        self.assertGreater(d1, z)
        self.assertGreaterEqual(z, z)
        self.assertGreaterEqual(d2, z)
        self.assertGreaterEqual(d2, d1)
        self.assertGreaterEqual(d2, d2)
        self.assertLessEqual(z, z)
        self.assertLessEqual(z, d1)
        self.assertLessEqual(z, d2)
        self.assertLessEqual(d1, d2)

    def test_time_comparisons(self):
        e = Time(0)
        t1 = Time(1)
        t2 = Time(2)
        self.assertEqual(e, e)
        self.assertEqual(t1, t1)
        self.assertEqual(t2, t2)
        self.assertLess(e, t1)
        self.assertLess(e, t2)
        self.assertLess(t1, t2)
        self.assertGreater(t2, t1)
        self.assertGreater(t2, e)
        self.assertGreater(t1, e)
        self.assertGreaterEqual(t2, t2)
        self.assertGreaterEqual(t2, t1)
        self.assertGreaterEqual(t2, e)
        self.assertGreaterEqual(t1, t1)
        self.assertGreaterEqual(t1, e)
        self.assertGreaterEqual(e, e)
        self.assertLessEqual(e, e)
        self.assertLessEqual(e, t1)
        self.assertLessEqual(e, t2)
        self.assertLessEqual(t1, t1)
        self.assertLessEqual(t1, t2)
        self.assertLessEqual(t2, t2)


if __name__ == '__main__':
    main()
