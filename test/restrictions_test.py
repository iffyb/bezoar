from typing import Iterable
from unittest import TestCase, main

from bezoar.model import Entity, entity_filter, entity_search, Restriction


def _dummy_search(source: Entity) -> Iterable[Entity]:
    return []


def _dummy_filter(source: Entity, target: Entity) -> bool:
    return False


class RestrictionsTest(TestCase):
    def test_parse(self):
        entity_search("have", _dummy_search)
        entity_search("here", _dummy_search)
        entity_filter("visible", _dummy_filter)
        entity_filter("container", _dummy_filter)
        entity_filter("surface", _dummy_filter)
        entity_filter("takeable", _dummy_filter)
        r = Restriction("have and not visible")
        self.assertIsNotNone(r)
        r = Restriction("have and here")
        self.assertIsNotNone(r)
        r = Restriction("(have or here) and visible")
        self.assertIsNotNone(r)
        r = Restriction("have or here and visible")
        self.assertIsNotNone(r)
        r = Restriction("have and here and visible")
        self.assertIsNotNone(r)
        r = Restriction("have or here or visible")
        self.assertIsNotNone(r)
        r = Restriction("have or here or not visible")
        self.assertIsNotNone(r)
        r = Restriction("have||here||!visible")
        self.assertIsNotNone(r)

        r = Restriction("((have) or here) and visible")
        self.assertIsNotNone(r)
        r = Restriction("((have or here)) and visible")
        self.assertIsNotNone(r)
        r = Restriction("((have or here) and visible)")
        self.assertIsNotNone(r)
        r = Restriction("not (not (not (have) or here) and visible)")
        self.assertIsNotNone(r)
        r = Restriction("not not have or not here and not visible")
        self.assertIsNotNone(r)


if __name__ == '__main__':
    main()
