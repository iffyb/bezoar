import math
from unittest import IsolatedAsyncioTestCase, main

from bezoar.base.asyncs import sleep_for
from bezoar.base.scheduler import Runnable, ScheduledTask, Scheduler
from bezoar.base.time import Time, milliseconds, zero
from bezoar.base.typed_number import TypedNumber


class TestType(TypedNumber):
    def __init__(self, value: int, name: str) -> None:
        super().__init__(value, name)


TEST_REALTIME = TestType(0, "REALTIME")
TEST_PAUSED = TestType(1, "PAUSED")
TEST_HYBRID = TestType(2, "HYBRID")


class SchedulerTest(IsolatedAsyncioTestCase):
    async def run_clock_scheduler_test(self, test_type: TestType):
        initially_paused = test_type is TEST_PAUSED or test_type is TEST_HYBRID
        scheduler = Scheduler(initially_paused)
        test_start = scheduler.now()
        scheduler.start()
        iterations = 30
        delay = milliseconds(50)
        late_tolerance = milliseconds(35)
        times: dict[int, dict[str, Time]] = {}
        tasks: list[ScheduledTask] = []
        order: list[int] = []
        testname = f"test {test_type}"

        for i in range(0, iterations):
            start = scheduler.now()
            slot = math.floor(i / 3) + 1
            target_timepoint = start.add(delay.scale(slot * 2))

            def make_sub_callback(source_iteration: int, target_time: Time) -> Runnable:
                # Capture variables without loop context.
                label = (source_iteration * 10) + 5

                async def callback() -> None:
                    end = scheduler.now()
                    times[label] = {"end": end, "target": target_time}
                    order.append(label)
                return callback

            def make_callback(iteration: int, target_time: Time) -> Runnable:
                # Capture variables without loop context.
                def callback() -> None:
                    end = scheduler.now()
                    label = iteration * 10
                    times[label] = {"end": end, "target": target_time}
                    order.append(label)
                    next_target = target_time.add(delay.scale(0.5))
                    scheduler.schedule_at(
                        next_target, make_sub_callback(iteration, next_target))
                return callback

            tasks.append(scheduler.schedule_at(
                target_timepoint, make_callback(i, target_timepoint)))

        # Cancel 1/3 of just-scheduled tasks, which means their sub tasks won't
        # be scheduled at all.
        for i in range(0, len(tasks), 3):
            tasks[i].cancel()

        if initially_paused:
            # Wait a bit to make sure nothing is happening.
            await sleep_for(delay.scale((iterations / 4) + 2))
            self.assertEqual(len(order), 0, testname)

        if test_type is TEST_PAUSED:
            # Advance past the end of all the tasks.
            await scheduler.advance(delay.scale(iterations + 2))
            self.assertGreater(len(order), 0, testname)
        elif test_type is TEST_HYBRID:
            # Advance past forward a bit, which should run some tasks.
            await scheduler.advance(delay.scale((iterations / 4) + 2))
            self.assertGreater(len(order), 0, testname)
            self.assertLess(len(order), math.floor(
                iterations / 3) * 2, testname)

            # Switch to real-time for the rest of the test.
            scheduler.resume()

        for t in reversed(tasks):
            await t.wait()

        errors: list[str] = []
        for key in order:
            if key not in times:
                continue
            value = times[key]
            target_offset = value['target'].subtract_time(test_start)
            end_offset = value['end'].subtract_time(test_start)
            delta = target_offset.subtract(end_offset)

            if delta.__gt__(zero()):
                errors.append(
                    f"{testname} {key}: {target_offset} - {end_offset} = {delta} > {zero()} (to target)")
            lower_bound = late_tolerance.scale(-1)
            if delta.__lt__(lower_bound):
                errors.append(
                    f"{testname} {key}: {target_offset} - {end_offset} = {delta} < {lower_bound} (to target)")
        self.assertEqual(len(errors), 0, "\n" + "\n".join(errors))

    async def test_real_clock_scheduler(self):
        await self.run_clock_scheduler_test(TEST_REALTIME)

    async def test_simulated_clock_scheduler(self):
        await self.run_clock_scheduler_test(TEST_PAUSED)

    async def test_hybrid_clock_scheduler(self):
        await self.run_clock_scheduler_test(TEST_HYBRID)


if __name__ == '__main__':
    main()
