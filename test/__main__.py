from test import (algorithm_test, feature_test, integration_test,
                  mddom_test, overlay_mapping_test,
                  restrictions_integration_test, restrictions_test,
                  scheduler_test, text_test, time_test)
from unittest import TestSuite, TextTestRunner, makeSuite

# These are to ensure the core runtime is fully loaded before loading the game
# for integration tests.
from bezoar.base import *
from bezoar.base.reset_module import *
from bezoar.mddom import *
from bezoar.model import *
from bezoar._model import *
from bezoar.runtime import *
from bezoar.base.environment import IS_PYODIDE


def suite() -> TestSuite:
    """Defines all the tests of the package."""
    s = TestSuite()
    s.addTest(makeSuite(algorithm_test.AlgorithmTest))
    s.addTest(makeSuite(feature_test.FeatureTest))
    s.addTest(makeSuite(mddom_test.MdDomTest))
    s.addTest(makeSuite(overlay_mapping_test.OverlayMappingTest))
    s.addTest(makeSuite(restrictions_test.RestrictionsTest))
    if not IS_PYODIDE:
        s.addTest(
            makeSuite(restrictions_integration_test.RestrictionsIntegrationTest))
        s.addTest(makeSuite(scheduler_test.SchedulerTest))
    s.addTest(makeSuite(text_test.TextTest))
    s.addTest(makeSuite(time_test.TimeTest))
    if not IS_PYODIDE:
        s.addTest(makeSuite(integration_test.IntegrationTest))
    return s


runner = TextTestRunner(verbosity=2)
result = runner.run(suite())
if not result.wasSuccessful():
    exit(1)
