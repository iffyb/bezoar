import asyncio
import logging
from typing import Any, Coroutine, Optional

from bezoar.console import Console


async def delayed(
        delay: float,
        coro: Coroutine[Any, Any, Any],
        event: Optional[asyncio.Event] = None) -> Any:
    if not asyncio.coroutines.iscoroutine(coro):
        raise TypeError(f"a coroutine was expected, got {coro!r}")
    try:
        if event is None:
            await asyncio.sleep(delay)
        else:
            try:
                if await asyncio.wait_for(event.wait(), delay):
                    # Handle cancellation event signalling.
                    coro.close()
                    return
            except asyncio.exceptions.TimeoutError:
                # In this case the event is a cancellation event, so timing out
                # is what we want for normal operation.
                pass
    except:
        coro.close()
        raise
    return await coro


async def main() -> None:
    command_holder: list[str] = []
    console = Console()
    stop_event = asyncio.Event()

    def done() -> bool:
        return len(command_holder) >= 3

    async def read_loop() -> None:
        while not done():
            command = await console.read_line("> ")
            if command is not None:
                command_holder.append(command)
        stop_event.set()
        console.stop()

    async def timed_print() -> None:
        console.print("Waiting.\n")

    async def timed_print2() -> None:
        console.print("Still Waiting.\n")

    async def print_loop1() -> None:
        while not done():
            await delayed(2.25, timed_print(), stop_event)

    async def print_loop2() -> None:
        while not done():
            await delayed(5, timed_print2(), stop_event)

    task1 = asyncio.create_task(print_loop1())
    task2 = asyncio.create_task(print_loop2())
    task3 = asyncio.create_task(delayed(1.5, read_loop()))

    console.print("Will exit after three commands, or one CTRL-C.\n")
    try:
        await asyncio.wait({task1, task2, task3})
    except KeyboardInterrupt:
        task1.cancel()
        task2.cancel()
        task3.cancel()
        await asyncio.wait({task1, task2, task3})

    for command in command_holder:
        print("Received: >", command, "< (", len(command), ")", sep="")


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    asyncio.run(main())
