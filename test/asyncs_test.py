from asyncio import Event, Queue, create_task
from unittest import IsolatedAsyncioTestCase, main

from bezoar.base.asyncs import get_loop_time, sleep_for, sleep_until
from bezoar.base.time import Time, milliseconds, zero


class AsyncsTest(IsolatedAsyncioTestCase):
    async def test_get_loop_time(self):
        iterations = 20
        tolerance = milliseconds(25)
        times: list[Time] = [get_loop_time()]

        while len(times) < iterations:
            another = get_loop_time()
            if another != times[-1]:
                times.append(another)

        self.assertGreaterEqual(len(times), iterations)
        last = times.pop(0)
        for t in times:
            delta = t.subtract_time(last)
            self.assertTrue(delta.__lt__(tolerance))
            self.assertTrue(delta.__gt__(zero()))
            last = t

    async def test_sleep_until(self):
        iterations = 10
        delay = milliseconds(75)
        times = []
        results = []

        for i in range(0, iterations):
            start = get_loop_time()
            target = start.add(delay)
            results.append(await sleep_until(target))
            end = get_loop_time()
            times.append(end.subtract_time(start))

        self.assertEqual(len(times), iterations)

        for i, d in enumerate(times):
            self.assertTrue(d.__ge__(delay.scale(0.5)), f"times[{i}]")
            self.assertTrue(d.__le__(delay.scale(2)), f"times[{i}]")

        for i, b in enumerate(results):
            self.assertTrue(b, f"results[{i}]")

    async def test_sleep_until_cancellable(self):
        iterations = 10
        delay = milliseconds(75)
        times = []
        results = []

        for i in range(0, iterations):
            cancellation_event = Event()
            start = get_loop_time()
            target = start.add(delay)
            results.append(await sleep_until(target, cancellation_event))
            end = get_loop_time()
            times.append(end.subtract_time(start))

        self.assertEqual(len(times), iterations)

        for i, d in enumerate(times):
            self.assertTrue(d.__ge__(zero()), f"times[{i}]")
            self.assertTrue(d.__le__(delay.scale(2)), f"times[{i}]")

        for i, b in enumerate(results):
            self.assertTrue(b, f"results[{i}]")

    async def test_sleep_until_cancellable_cancelled(self):
        iterations = 10
        delay = milliseconds(75 * 2)
        cancel_delay = delay.scale(0.5)
        times = []
        results = []

        for i in range(0, iterations):
            cancellation_event = Event()
            start = get_loop_time()
            target = start.add(delay)
            cancel_target = start.add(cancel_delay)

            async def cancel():
                await sleep_until(cancel_target)
                cancellation_event.set()
            sleep_coro = create_task(
                sleep_until(target, cancellation_event))
            create_task(cancel())
            results.append(await sleep_coro)
            end = get_loop_time()
            times.append(end.subtract_time(start))

        self.assertEqual(len(times), iterations)

        for i, d in enumerate(times):
            self.assertTrue(d.__ge__(zero()), f"times[{i}]")
            self.assertTrue(d.__le__(delay.scale(2)), f"times[{i}]")

        for i, b in enumerate(results):
            self.assertFalse(b, f"results[{i}]")

    async def test_queue(self):
        iterations = 10
        delay = milliseconds(25)
        queue = Queue()
        put_times: list[Time] = []
        get_times: list[Time] = []
        results: list[bool] = []

        async def putter():
            for i in range(0, iterations):
                await sleep_for(delay)
                put_times.append(get_loop_time())
                queue.put(i)

        async def getter():
            for i in range(0, iterations):
                results.append(await queue.get())
                get_times.append(get_loop_time())

        get_task = create_task(getter())
        await sleep_for(delay)
        self.assertEqual(0, len(results))
        await sleep_for(delay)
        self.assertEqual(0, len(results))
        create_task(putter())
        await get_task
        self.assertEqual(iterations, len(results))

        self.assertEqual(iterations, len(put_times))
        self.assertEqual(iterations, len(get_times))
        for i in range(0, iterations):
            self.assertEqual(i, results[i])
            put_time = put_times[i]
            get_time = get_times[i]
            delay = get_time.subtract_time(put_time)
            self.assertTrue(delay.__ge__(zero()), f"iteration {i}")


if __name__ == '__main__':
    main()
