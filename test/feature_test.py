from unittest import TestCase, main

from bezoar import reset
from bezoar.library.features import feature
from bezoar.model import description, find_data


class FeatureTest(TestCase):
    def setUp(self):
        reset()

    def tearDown(self):
        reset()

    def test_it_all(self):
        expected_id = "a"
        expected_situ = "fsitu"
        expected_quick = "fquick"
        expected_detail = "fdetail"
        feat = feature(id=expected_id,
                       description=description(
                           quick=expected_quick,
                           situ=expected_situ,
                           detail=expected_detail,
                       ))
        self.assertIsNotNone(feat)
        self.assertEqual(expected_id, feat.id)
        self.assertEqual(expected_quick, feat.description.quick)
        self.assertEqual(expected_situ, feat.description.situ)
        self.assertEqual(expected_detail, feat.description.detail)
        self.assertEqual(feat, find_data(expected_id))

    def test_example(self):
        mailbox = feature(
            id="mailbox",
            adjectives=[
                "small",
                "mail",
            ],
            description=description(
                detail="""
                    You see a small, traditional-looking mailbox lying on the ground, next to
                    a desiccated wooden post that was once its home.
                    """,
            ),
            aliases=[
                "mailbox",
                "box",
            ],
            properties={
                "custom_1": "foo",
            }
        )
        self.assertIsNotNone(mailbox)
        self.assertIsNotNone(mailbox.id)
        self.assertIsNotNone(mailbox.description)
        self.assertIsNotNone(mailbox.description.detail)
        self.assertIsNotNone(mailbox.properties)
        self.assertTrue("custom_1" in mailbox.properties)
        self.assertEqual(mailbox.properties["custom_1"], "foo")
        self.assertEqual(mailbox, find_data("mailbox"))


if __name__ == '__main__':
    main()
