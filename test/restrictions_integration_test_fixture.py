from typing import Iterable

import bezoar.library.default_actions
from bezoar._model.entities import (all_entities, entity, entity_filter,
                                    entity_search, set_player_data)
from bezoar.library.actors import actor
from bezoar.library.items import item
from bezoar.library.locations import location
from bezoar.model import Entity, ParseResult, action, description, version


def _all_entities() -> Iterable[Entity]:
    entities_by_type = all_entities()
    entities: list[Entity] = []
    for entity_type_list in entities_by_type:
        for entity in entity_type_list:
            entities.append(entity)
    return entities


def _s1_search(source: Entity) -> Iterable[Entity]:
    return _all_entities()


def _s2_search(source: Entity) -> Iterable[Entity]:
    return _all_entities()


def _s3_search(source: Entity) -> Iterable[Entity]:
    return _all_entities()


def _fruit_filter(source: Entity, target: Entity) -> bool:
    return target.matches(None, "fruit")


def _vegetable_filter(source: Entity, target: Entity) -> bool:
    return target.matches(None, "vegetable")


def _f3_filter(source: Entity, target: Entity) -> bool:
    return True


entity_search("s1", _s1_search)
entity_search("s2", _s2_search)
entity_search("s3", _s3_search)
entity_filter("fruit", _fruit_filter)
entity_filter("vegetable", _vegetable_filter)
entity_filter("f3", _f3_filter)


def _action_test(self: Entity, result: ParseResult) -> bool:
    directs = result.objects["direct"]
    if not directs:
        result.actor.tell("Test what?")
        return False

    for d in directs:
        result.actor.tell(f"{d.quick}")

    return True


test1_action = action(id="test1",
                      syntaxes=[
                          "test1 {direct+:here and not have and fruit and s1 and s2}",
                      ],
                      handler=_action_test)

test2_action = action(id="test2",
                      syntaxes=[
                          "test2 {direct+:(here and not have and fruit) or (have and vegetable)}",
                      ],
                      handler=_action_test)

test3_action = action(id="test3",
                      syntaxes=[
                          "test3 {direct+:(have or fruit) and (here and not have or vegetable)}",
                      ],
                      handler=_action_test)

test4_action = action(id="test4",
                      syntaxes=[
                          "test4 {direct+:(vegetable or fruit) and (here and not have)}",
                      ],
                      handler=_action_test)

version(
    app_name="Restrictions Integration Text Fixture",
    version="0",
)

apple = item(
    id="apple",
    aliases=[
        "apple",
        "fruit",
    ],
)

pear = item(
    id="pear",
    aliases=[
        "pear",
        "fruit",
    ],
)

orange = item(
    id="orange",
    aliases=[
        "orange",
        "fruit",
    ],
)

carrot = item(
    id="carrot",
    aliases=[
        "carrot",
        "vegetable",
    ],
)

potato = item(
    id="potato",
    aliases=[
        "potato",
        "vegetable",
    ],
)

celery = item(
    id="celery",
    aliases=[
        "celery",
        "vegetable",
    ],
)

rutabaga = item(
    id="rutabaga",
    aliases=[
        "rutabaga",
        "vegetable",
    ],
)

tomato = item(
    id="tomato",
    aliases=[
        "tomato",
        "fruit",
        "vegetable",
    ],
)

rock = item(
    id="rock",
    aliases=[
        "rock",
        "mineral",
    ],
)


stone = item(
    id="stone",
    aliases=[
        "stone",
        "mineral",
    ],
)

the_player = entity(
    id="the_player",
    description=description(
        detail="""You quickly assess yourself. Everything seems fine?"""
    ),
    aliases=["you", "yourself", "self", "myself"],
    mixins=[actor],
    relationships={
        "contents": [
            rutabaga,
        ],
    },
)

set_player_data(the_player)


start_room = location(
    id="start_room",
    description=description(
        quick="Start Room",
        detail="""This is the room you start in.""",
    ),

    exits={
        "s": "other_room",
    },
    relationships={
        "contents": [
            the_player,
            apple,
            pear,
            celery,
            rock,
        ],
    },
)


start_room = location(
    id="other_room",
    description=description(
        quick="Other Room",
        detail="""This is not the room you start in, it's another room.""",
    ),
    exits={
        "n": "start_room",
    },
    relationships={
        "contents": [
            carrot,
            potato,
            orange,
            tomato,
            stone,
        ],
    },
)
