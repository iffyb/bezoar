from test.integration_test_base import IntegrationTestBase
from types import ModuleType
from typing import Optional, Union
from unittest import main

from bezoar._model.entities import find_entity
from bezoar.mddom import AnsiTerminalRenderer
from bezoar.model import Entity, EntityData
from bezoar.runtime.engine import submit_command_async


def _ed_to_e(ed: Union[Entity, EntityData]) -> Entity:
    if not ed:
        raise Exception()

    if isinstance(ed, Entity):
        return ed

    if isinstance(ed, EntityData):
        e = find_entity(ed.id)
        if not e:
            raise Exception()
        return e


class IntegrationTest(IntegrationTestBase):
    def __init__(self, methodName: str) -> None:
        super().__init__(methodName=methodName)

    def import_model(self) -> ModuleType:
        import bezoar_sample.model as the_model
        return the_model

    def test_move(self):
        def move(command: str, loc: Union[EntityData, Entity]) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            if loc:
                e = _ed_to_e(loc)
                self.assertRegex(self.log[0], str(e.quick))
            self.log.clear()

        def move_item(command: str, match: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            if match:
                self.assertRegex(self.log[0], match)
            self.log.clear()

        move("east", None)
        move_item("move rug", "the rug")
        move("west", None)

        move("north", self.model.house_north)
        move("east", self.model.house_east)
        move("south", self.model.house_south)
        move("west", self.model.house_west)

        move("northeast", self.model.house_north)
        move("southeast", self.model.house_east)
        move("southwest", self.model.house_south)
        move("northwest", self.model.house_west)

        move("n", self.model.house_north)
        move("e", self.model.house_east)
        move("s", self.model.house_south)
        move("w", self.model.house_west)

        move("ne", self.model.house_north)
        move("se", self.model.house_east)
        move("sw", self.model.house_south)
        move("nw", self.model.house_west)

        move("north", self.model.house_north)

    def test_get_drop_put(self):
        def get(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Taken.")
            self.log.clear()

        def drop(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Dropped.")
            self.log.clear()

        def put(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Done.")
            self.log.clear()

        def put_on(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertRegex(self.log[0], "No objects found")
            self.log.clear()

        get("get leaflet")
        drop("drop leaflet")
        get("get small leaflet")
        drop("drop leaflet")
        get("get paper brochure")
        drop("drop brochure")
        get("pick paper leaflet up")
        drop("put leaflet down")
        get("pick up leaflet")
        drop("put down leaflet")

        get("get leaflet")
        put("put leaflet in mailbox")
        get("get leaflet")
        put("set leaflet in mailbox")
        get("get leaflet")
        put("place leaflet in mailbox")

        get("get leaflet")
        put_on("place leaflet on mailbox")

    def test_inventory(self):
        def get(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Taken.")
            self.log.clear()

        def inventory(command: str, objname: str = "") -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertRegex(self.log[0], "^You ")
            if objname:
                self.assertRegex(self.log[0], r"\b" + objname + r"\b")
            self.log.clear()

        def drop(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Dropped.")
            self.log.clear()

        inventory("inventory")
        get("get leaflet")
        inventory("i", "leaflet")
        drop("drop leaflet")
        inventory("i")

    def test_look(self):
        def look(command: str, loc: Union[EntityData, Entity]) -> None:
            loc_entity = _ed_to_e(loc)
            self.loop.run_until_complete(submit_command_async(command))
            self.assertIn(str(loc_entity.quick), self.log[0])
            self.log.clear()

        look("look", self.model.house_west)
        look("l", self.model.house_west)
        look("look around", self.model.house_west)
        look("l around", self.model.house_west)

    def test_look_at(self):
        def look_at(command: str, ed: Union[EntityData, Entity]) -> None:
            e = _ed_to_e(ed)
            self.loop.run_until_complete(submit_command_async(command))
            renderer = AnsiTerminalRenderer()
            renderer.ansi = False
            renderer.process(e.detail)
            self.compare_log_to_text(
                self.log, renderer.buffer)
            self.log.clear()

        look_at("look at leaflet", self.model.leaflet)
        look_at("l at leaflet", self.model.leaflet)
        look_at("examine leaflet", self.model.leaflet)
        look_at("exa leaflet", self.model.leaflet)
        look_at("x leaflet", self.model.leaflet)

    def test_drop_interrupt(self):
        def get(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Taken.")
            self.log.clear()

        def drop(command: str, match:str = "") -> None:
            self.loop.run_until_complete(submit_command_async(command))
            if not match:
                self.assertEqual(self.log[0], "Dropped.")
            else:
                self.assertRegexpMatches(self.log[0], match)
            self.log.clear()

        def move(command: str, loc: Optional[Union[EntityData, Entity]] = None) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            if loc:
                e = _ed_to_e(loc)
                self.assertRegex(self.log[0], str(e.quick))
            self.log.clear()

        get("get leaflet")
        move("e")
        move("d")
        move("n")
        move("w")
        drop("drop leaflet", "You don't want")


if __name__ == '__main__':
    main()
