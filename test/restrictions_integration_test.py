from test.integration_test_base import IntegrationTestBase
from types import ModuleType
from unittest import main

from bezoar.runtime.engine import submit_command_async


class RestrictionsIntegrationTest(IntegrationTestBase):
    def import_model(self) -> ModuleType:
        import test.restrictions_integration_test_fixture as the_model
        return the_model

    def test_restrictions(self):
        def get(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Taken.")
            self.log.clear()

        def get_not_here(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertRegex(self.log[0], r"No objects found")
            self.log.clear()

        def drop(command: str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertEqual(self.log[0], "Dropped.")
            self.log.clear()

        def test(command: str, name:str) -> None:
            self.loop.run_until_complete(submit_command_async(command))
            self.assertRegex(self.log[0], name)
            self.log.clear()

        get("get apple")
        drop("drop apple")
        get_not_here("get orange")

        test("test1 apple", r"apple")
        get("get apple")
        test("test1 apple", r"No objects found")
        drop("drop apple")
        test("test1 apple", r"apple")
        test("test1 celery", r"No objects found")

        test("test2 apple", r"apple")
        test("test2 pear", r"pear")
        get("get apple")
        test("test2 apple", r"No objects found")
        test("test2 rutabaga", r"rutabaga")
        test("test2 celery", r"No objects found")

        test("test3 tomato", r"tomato")
        test("test3 pear", r"pear")
        test("test3 apple", r"No objects found")
        test("test3 orange", r"No objects found")
        test("test3 potato", r"No objects found")
        test("test3 rutabaga", r"rutabaga")
        test("test3 celery", r"No objects found")

        test("test4 rock", r"No objects found")
        test("test4 stone", r"No objects found")
        test("test4 carrot", r"No objects found")
        test("test4 orange", r"No objects found")
        test("test4 apple", r"No objects found")
        test("test4 pear", r"pear")
        test("test4 celery", r"celery")

if __name__ == '__main__':
    main()
