$VENV_PATH = "venv"

if (Test-Path -Path $VENV_PATH) {
    "Virtual environment '$VENV_PATH' already exists."
} else {
    "Generating $VENV_PATH..."
    python -m venv --system-site-packages $VENV_PATH
}

"Activating $VENV_PATH..."
& "$VENV_PATH\Scripts\activate.ps1"

python -m pip install --upgrade pip

"Installing dependencies..."
pip install -r requirements.txt
