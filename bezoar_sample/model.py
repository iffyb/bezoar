from __future__ import annotations

from typing import Optional, cast

import bezoar.library.default_actions
from bezoar._model.entities import set_player_data
from bezoar.base.union import union
from bezoar.library.actors import actor, is_actor
from bezoar.library.combat import COMBAT_STATISTICS_PROPERTY, CombatStatistics
from bezoar.library.containers import (EVENT_ENTERED, MoveEvent, container,
                                       get_contents, get_environment, move)
from bezoar.library.features import feature
from bezoar.library.here import find_first_here, find_here
from bezoar.library.items import item
from bezoar.library.locations import find_location, location
from bezoar.library.text import vref
from bezoar.library.visibles import visible
from bezoar.mddom import Block, Blockish, Span, Spanish, bl, h4, i, p, s, sp
from bezoar.model import (EVENT_ACTION_INTERRUPT, ActionInterruptEvent, Entity,
                          Event, ParseResult, Relationship, RelationshipData,
                          action, description, entity, find_entity, get_actor,
                          get_player, ref, relate_handlers, the, version)

version(
    app_name="Bezoar Sample",
    version="3",
)


def _action_help(self: Entity, result: ParseResult) -> bool:
    result.actor.tell("""
        Congratulations, you are playing a text adventure game! The game tells
        you, in words, what you see, hear, smell, taste, and feel. It may
        occassionally also tell you what your character thinks or knows. You
        speak to the game by typing imperatives (a.k.a. commands), and it will
        try to understand you, do what you say, and tell you the result.

        You are always in a "location" within the game, and that location has
        a description, may contain objects you can interact with, and probably
        has one or more exits that you can take to move to another location.

        For example, in this game the first location is called "West of House"
        and it has several exits, one of which is "north." To move north you
        type:

        → Go north.

        Or, you may type:

        → north

        Compass directions can be abbreviated to their first letter:

        → n

        You also may be able to go up (u), down (d), in, or out. Sometimes,
        multiple exits from a source location take you to the same destination.

        Similarly, to pick up the leaflet in the mailbox, you may type:

        → Pick the leaflet up.

        Or just:

        → get leaflet

        To get a list of objects you are carrying, type:

        → inventory

        Or:

        → i

        You can look at objects that are visible to you, and may be able to
        examine things described in the location description for more details.
        For example, while at "West of House," try:

        → Look at leaflet.

        → l at forest

        → examine tree

        → x house

        You can also look again at the location you are in, but it will
        automatically look for you when you change locations:

        → Look around.

        Or:

        → look

        Now go forth, and explore! (BTW, The next prompt "→" is a real one.)
        """)
    return True


def _action_move_item(self: Entity, result: ParseResult) -> bool:
    result.actor.tell("You can't move that.")
    return True


def _action_push(self: Entity, result: ParseResult) -> bool:
    result.actor.tell("You can't push that.")
    return True


def _action_pull(self: Entity, result: ParseResult) -> bool:
    result.actor.tell("You can't pull that.")
    return True


def _action_roll_up(self: Entity, result: ParseResult) -> bool:
    result.actor.tell("You can't roll that up.")
    return True


action(id="help", syntaxes=["help"], handler=_action_help)


action(id="move_item",
       syntaxes=[
           "move {direct:here}",
       ],
       handler=_action_move_item)


action(id="push",
       syntaxes=[
           "push {direct:here}",
       ],
       handler=_action_push)


action(id="pull",
       syntaxes=[
           "pull {direct:here}",
       ],
       handler=_action_pull)


action(id="roll_up",
       syntaxes=[
           "roll up {direct:here}",
           "roll {direct:here} up",
       ],
       handler=_action_roll_up)

the_player = entity(
    id="the_player",
    description=description(
        detail="""You quickly assess yourself. Everything seems fine?"""
    ),
    aliases=["you", "yourself", "self", "myself"],
    mixins=[actor],
)

set_player_data(the_player)

leaflet = item(
    id="leaflet",
    adjectives=[
        "small",
        "paper",
    ],
    aliases=[
        "leaflet",
        "brochure",
        "letter",
        "memo",
        "mailer",
        "paper",
    ],
    description=description(
        detail=bl(
            h4("WELCOME TO BEZOAR"),
            p("""
            Bezoar is a framework for building text adventure games, using the
            modern trappings of software technology that have emerged in the ~40
            years since ZORK and Colossal Cave Adventure.
            """),
            p("""
            Bezoar is open-source, licensed under the Apache 2.0 Open Source
            License.
            """),
            p(
                i(
                    s("Bezoar is Copyright (c) 2021 The Bezoar Contributors."),
                    s("ZORK was a Registered Trademark of Infocom, Inc."),
                ),
            ),
        ),
    ),
)


def mailbox_short(e: Entity) -> Spanish:
    return s("A small ", ref(e, "mailbox"), " lies here on the ground.")


mailbox = feature(
    id="mailbox",
    description=description(
        situ=mailbox_short,
        detail="""
            You see a small, traditional-looking mailbox lying on the ground,
            next to a desiccated wooden post that was once its home.
            """,
    ),
    aliases=[
        "mailbox",
        "box",
    ],
    mixins=[container],
    relationships={
        "contents": [
            "leaflet",
        ],
    },
    visible=True,
)

HOUSE_FEATURES = {
    "house|mansion|manse|colonial": """
        The house looks like it was once a beautiful colonial, and probably
        owned by someone very wealthy. But, inside and out, time has ravaged it
        mercilessly. It doesn't quite look like it is about to collapse, but it
        is close.
        """,
    "forest": """
        With no one apparently around to protest, the forest has eagerly
        reclaimed its territory from the property.
        """,
    "tree|trees": """
        There are many trees around, none of them particularly interesting.
        """,
}


def forest_impassible(e: Entity, d: str,
                      actor: Entity) -> Optional[Entity]:
    actor.tell(p("You try to find a way through the forest, but it's just too ",
                 " dense to make any progress."))
    return None


def squeeze_in(rel: Relationship,
               destination: Optional[Entity]) -> None:
    player = get_player()
    if not player or rel.to_entity is not player:
        return
    if destination is not find_location("living_room"):
        return
    player.tell(p("You carefully maneuver between the rotting boards."))


def squeeze_out(rel: Relationship,
                destination: Optional[Entity]) -> None:
    player = get_player()
    if not player or rel.to_entity is not player:
        return
    if destination is not find_location("house_west"):
        return
    player.tell(p("You carefully maneuver between the rotting boards."))


def house_west_description(e: Entity) -> Block:
    return bl(
        p("You are in front of a dilapidated ", vref(e, "house"), " to the ",
          "east, rotting away in the middle of a ", vref(
              e, "forest"), ". The ",
          "few flakes of paint that remain suggest that it was once painted ",
          "white, but the house now bares its deteriorating structure to the ",
          "elements."),
        p("A once boarded-up ", vref(e, "door"), " has largely disintegrated, ",
          "leaving a gap you think you could squeeze through.")
    )


house_west = location(
    id="house_west",
    description=description(
        quick="West of House",
        detail=house_west_description,
    ),

    exits={
        "n|ne": "house_north",
        "s|se": "house_south",
        "e|in": "living_room",
        "nw|w": forest_impassible,
    },
    relationships={
        "contents": [
            "mailbox",
            the_player,
        ],
    },
    relationship_handlers={
        "contents": relate_handlers(on_unrelate=squeeze_in),
    },
    features=union(HOUSE_FEATURES, {
        "planks|boards|plank|board": """
            Planks which once kept a door sealed up have rotten away, their
            remains littering the ground. A couple boards are still hanging onto
            their nails, but they are no longer serving their purpose.
            """,
        "door|doorway": """
            There isn't much of a door to speak of. It looks like it was boarded
            up at one point, but most of the planks have fallen down.
            """,
    }),
)

house_north = location(
    id="house_north",
    description=description(
        quick="North of House",
        detail="""
            You are facing the north side of a ruined house. There is no door
            here, and all the windows are boarded up.
            """,
    ),
    exits={
        "w|sw": "house_west",
        "e|se": "house_east",
        "nw|n|ne": forest_impassible,
    },
    features=HOUSE_FEATURES,
)

house_south = location(
    id="house_south",
    description=description(
        quick="South of House",
        detail="""
        You are facing the south side of a broken-down house. There is no door
        here, and all the windows are boarded up.
        """,
    ),
    exits={
        "w|nw": "house_west",
        "e|ne": "house_east",
        "sw|s|se": forest_impassible,
    },
    features=HOUSE_FEATURES,
)


def climb_in_window(rel: Relationship,
                    destination: Optional[Entity]) -> None:
    player = get_player()
    if not player or rel.to_entity is not player:
        return
    if destination is not find_location("kitchen"):
        return
    player.tell(p("You climb in through the unboarded window."))


def house_east_description(e: Entity) -> Block:
    return p(
        "You are behind the remains of an old, wooden ", vref(
            e, "house"), ". ",
        "In one corner of the house is a ", vref(e, "window frame"), " ",
        "that hasn't been boarded up.",
    )


house_east = location(
    id="house_east",
    description=description(
        quick="Behind House",
        detail=house_east_description,
    ),
    exits={
        "n|nw": "house_north",
        "s|sw": "house_south",
        "w|in": "kitchen",
        "ne|e|se": forest_impassible,
    },
    relationship_handlers={
        "contents": relate_handlers(on_unrelate=climb_in_window),
    },
    features=union(HOUSE_FEATURES, {
        "window|frame|window frame": """All the other windows have been boarded
            up, but this one somehow escaped that fate."""
    }),
)


def rug_short(rug: Entity) -> Spanish:
    if not rug.properties.get("shredded", False):
        return sp(s("A large ", ref(rug, "rug"), " covering the floor is in ",
                    "tatters.",
                    s("It noticeably sinks in the center of the room.")))
    return s("There are scraps of a ", ref(rug, "rug"), " lying about the room.")


def rug_long(rug: Entity) -> str:
    if not rug.properties.get("shredded", False):
        return """
            The tattered carpet appears to have been a well-balanced meal for
            many healthy generations of moths. What remains hints at a
            traditional red, flowery Oriental pattern. The rug conspicuously
            sags down in the middle of the room, as if there were no floor
            there.
            """
    return """
        Scattered patches of muted color are all that are left of what was
        possibly once a large, handsome Oriental rug.
        """


def rug_shred(rug: Entity, actor: Entity) -> bool:
    if rug.properties.get("shredded", False):
        actor.tell(
            p("There's really nothing left of the rug to do anything with."))
        return True

    actor.tell(p("""
        As soon as you touch the rug, the tenuous fibers lose their capacity to
        hold themselves together. The sunken area breaks apart and falls down
        the hole it was covering, revealing a rickety staircase descending into
        darkness.
        """))
    rug.properties.__setitem__("shredded", True)
    trapdoor = find_entity("trapdoor")
    if trapdoor is None:
        raise Exception("No 'trapdoor' instance.")
    env = get_environment(rug)
    if env is None:
        raise Exception("rug has no environment.")
    if not move(trapdoor, env):
        raise Exception()
    return True


def rug_shred_handler(rug: Entity, reference_name: str, result: ParseResult) -> bool:
    return rug_shred(rug, result.actor)


feature(
    id="rug",
    description=description(
        situ=rug_short,
        detail=rug_long,
    ),
    aliases=[
        "rug",
        "carpet",
    ],
    adjectives=[
        "tattered",
        "oriental",
        "red",
        "flowery",
    ],
    properties={
        "shredded": False
    },
    actions={
        "get": rug_shred_handler,
        "move_item": rug_shred_handler,
        "pull": rug_shred_handler,
        "push": rug_shred_handler,
        "roll_up": rug_shred_handler,
    },
    visible=True,
)


def trapdoor_short(e: Entity) -> Spanish:
    return s("A large ", ref(e, "hole"), " in the center of the floor ",
             "exposes a downward staircase.")


feature(
    id="trapdoor",
    description=description(
        situ=trapdoor_short,
        detail="""
            The hole looks like it used to host a trap door, given the hinges on the
            far side, but there's no door to be seen. The frame of the hole is made
            of some kind of crusty concrete -- aged, but holding firm. Inside the
            hole is a set of stairs that could at best be called "functional."
            """,
    ),
    aliases=[
        "hole",
        "trapdoor",
        "door",
    ],
    adjectives=[
        "large",
        "trap",
    ],
    visible=True,
)


def case_short_description(case: Entity) -> Spanish:
    result = s()
    if get_contents(case):
        result.children.append(sp("A ", ref(case, "trophy case"), " "))
    else:
        result.children.append(
            sp("An empty ", ref(case, "trophy case"), " "))
    result.children.append(sp(" is mounted to the wall, with shards of glass on the floor "
                              "around it."))
    return result


feature(
    id="trophy_case",
    description=description(
        situ=case_short_description,
        detail="""
            The trophy case is firmly mounted to the wall, helping both survive
            the effects of time better than their surroundings.
            """,
    ),
    aliases=[
        "case",
    ],
    adjectives=[
        "trophy",
    ],
    mixins=[container],
)


def living_room_down(living_room: Entity,
                     direction: str,
                     actor: Entity) -> Optional[Entity]:
    if not find_here(living_room, None, "hole"):
        rug = find_first_here(living_room, None, "rug")
        if rug:
            rug_shred(rug, actor)
    return find_location("cellar")


def living_room_description(living_room: Entity) -> Blockish:
    case = find_first_here(living_room, None, "case")
    if case:
        case_desc: Optional[Span] = case.situ
    else:
        case_desc = None
    return p("You are in what used to be a living room. Many of the ",
             "floorboards are cracked, broken, or just missing. ",
             case_desc,
             "A doorway leads east to another room.")


location(
    id="living_room",
    description=description(
        quick="Living Room",
        detail=living_room_description,
    ),
    exits={
        "w": "house_west",
        "e": "kitchen",
        "d|in": living_room_down
    },
    relationships={
        "contents": [
            "trophy_case",
            "rug",
        ],
    },
    relationship_handlers={
        "contents": relate_handlers(on_unrelate=squeeze_out),
    },
),


def climb_out_window(rel: Relationship,
                     destination: Optional[Entity]) -> None:
    player = get_player()
    if not player or rel.to_entity is not player:
        return
    if destination is not find_location("house_east"):
        return
    player.tell(p("You clambor out through the window frame."))


def kitchen_description(e: Entity) -> Block:
    return p(
        "You are in the kitchen of the old house. A ", vref(e, "table"),
        " seems to have survived the ages, still standing in the center of ",
        "the room. An ", vref(e, "open doorway"), " leads to the west and to ",
        "the east is a small window. A former ", vref(e, "staircase"), " has ",
        "completely collapsed in a corner, underneath a ",
        vref(e, "dark hole"), " in the ceiling.",
    )


location(
    id="kitchen",
    description=description(
        quick="Kitchen",
        detail=kitchen_description,
    ),
    exits={
        "w": "living_room",
        "e": "house_east",
    },
    relationship_handlers={
        "contents": relate_handlers(on_unrelate=climb_out_window),
    },
    features={
        "table": """The table is still sturdy, but it does wobble a bit on the
            uneven floor.""",
        "staircase|stairs|stairway": """You are pretty sure that this pile of
            rotten wood was once stairs, though you could be convinced
            otherwise.""",
        "hole|dark hole|attic": """The hole looks like access to an attic above the
            kitchen, but you suspect that it wouldn't support your weight,
            even if you found a way up there.""",
        "doorway|open doorway|door": """There's no door, and it leads west.""",
    },
)


def rat_ant_entered(rat_ant: Entity, ev: Event) -> bool:
    move_event = cast(MoveEvent, ev)
    mover = move_event.source
    if not is_actor(mover):
        return False
    mover.tell(p(s(the(rat_ant), " spins around to face you warily.")))
    return True


entity(
    id="rat_ant",
    description=description(prefers_situ=True),
    aliases=[
        "rat-ant",
        "rat",
        "ant",
    ],
    event_handlers={
        EVENT_ENTERED: rat_ant_entered,
    },
    properties={
        COMBAT_STATISTICS_PROPERTY: CombatStatistics(20, 40, 15, 10, 10, 15, 30),
    },
    mixins=[visible, actor],
)


def cellar_south(e: Entity, d: str, actor: Entity) -> Optional[Entity]:
    actor.tell(p("This is one of those stubbornly impassible cave-ins."))
    return None


def cellar_west(e: Entity, d: str, actor: Entity) -> Optional[Entity]:
    actor.tell(p("You attempt to climb the unclimbable ramp. ",
                 "It doesn't go well."))
    return None


location(
    id="cellar",
    description=description(
        quick="Cellar",
        detail="""
            You are in a dark and damp cellar with a narrow passageway leading
            north, and a cave-in to the south. On the west is the bottom of a steep
            metal ramp, which is unclimbable.
            """,
    ),
    exits={
        "u|out": "living_room",
        "n": "troll_room",
        "s": cellar_south,
        "w": cellar_west,
    },
    relationships={
        "contents": [
            "rat_ant",
        ],
    },
    features={
        "ramp|metal ramp": """It's a very steep ramp. You are quite sure you
            could not climb it.""",
        "cave-in|rubble|crawlway": """The small crawlway to the south has
            completely filled in with rubble.""",
        "passageway|passage": """The wall to the north has an opening that leads
            to a short hallway, but you can't make out what lies beyond.""",
    },
)


def maze_reset_format(room: Entity, rel_data: RelationshipData,
                      user: Optional[Entity]):
    if user is None or user is not get_actor():
        return

    user.properties["format_index"] = 0


location(
    id="troll_room",
    description=description(
        quick="Violence Room",
        detail="""
            This is a small room with a passage to the south, a cave-in to the east,
            and a forbidding hole to the west. Bloodstains and deep scratches
            (perhaps made by an axe) mar the walls.

            There's no evidence of what committed all this gruesome savagery. It
            seems the act of some vicious monster, like a troll or something... But,
            of course, you know that monsters don't exist.
            """,
    ),
    exits={
        "s": "cellar",
        "w": "maze_0",
    },
    relationship_handlers={
        "contents": relate_handlers(on_relate=maze_reset_format),
    },
)


def _maze_action_interrupt(self: Entity, ev: Event) -> bool:
    e = cast(ActionInterruptEvent, ev)
    if not e:
        return False
    if e.result.action.id != "drop":
        return False
    e.result.actor.tell("You don't want to drop anything in this muddy cave.")
    return True


MAZE_FORMATS = [
    ("This is part of a {maze} of {twisty}, {little} {passages}, {allalike}."),
    ("You are in a {maze} of {twisty}, {little} {passages}, {allalike}."),
    ("You are in a part of a {maze} of {twisty}, {little} {passages}, "
        "{allalike}."),
    ("This is part of a {maze} of {little}, {twisty} {passages}, {allalike}."),
    ("This is part of a {little} {maze} of {twisty} {passages}, {allalike}."),
    ("This is part of a {twisty} {maze} of {little} {passages}, {allalike}."),
    ("This is a {little} part of a {twisty} {maze} of {passages} that are "
        "{allalike}."),
    ("This is a portion of a {maze} of {twisty}, {little} {passages}, "
        "{allalike}."),
    "You are lost in a {maze} of {little}, {twisty} {passages}, {allalike}.",
    ("This is part of a {maze} of {passages}, both {twisty} and {little}, "
        "{allalike}."),
    ("This is one of many {passages} in a {maze}, {allalike}. These {passages} "
        "are really {little} and super {twisty}."),
    "You are still in a {maze} of {twisty}, {little} {passages}, {allalike}.",
    ("You are yet still in a {maze} of {twisty}, {little} {passages}, "
        "{allalike}."),
    ("This is another part of a {maze} of {twisty}, {little} {passages}, "
     "{allalike}. Or, wait... is this the same part as before?"),
    ("It's {maze}! It's {maze}! It's {twisty}, it's {little}, the {passages} "
        "are {allalike}!"),
    ("This is part of a {maze} of {twisty}, {little} {passages}, {allalike}. "
        "There are exits in many directions."),
]


MAZE_ALL_ALIKES = [
    "all alike",
    "almost all alike",
    "mostly all alike",
    "significantly alike",
    "generally alike",
    "all somewhat alike",
    "largely all similar",
    "all the same",
    "all the same, but different",
    "all slightly alike",
    "all somewhat different",
    "all different",
    "all vastly different",
    "all not alike",
    "all unalike",
]


def make_maze_description(format: int, maze: str, twisty: str, little: str,
                          passages: str, all_alike_index: int) -> str:
    return MAZE_FORMATS[format].format(
        maze=maze,
        twisty=twisty,
        little=little,
        passages=passages,
        allalike=MAZE_ALL_ALIKES[all_alike_index],
    )


def maze_description(self: Entity) -> str:
    maze = self.properties.get("maze", "maze")
    twisty = self.properties.get("twisty", "twisty")
    little = self.properties.get("little", "little")
    passages = self.properties.get("passages", "passages")
    all_alike_index = 0
    format_index = 0
    actor = get_actor()
    if actor is not None:
        all_alike_index = actor.properties.get("all_alike_index", 0)
        format_index = actor.properties.get("format_index", 0)
    return make_maze_description(format_index, maze, twisty,
                                 little, passages,
                                 all_alike_index)


def maze_increment_all_alike(room: Entity, rel_data: RelationshipData,
                             user: Optional[Entity]):
    if user is None or user is not get_actor():
        return

    visited = room.properties.get("visited", False)
    if not visited:
        room.properties.__setitem__("visited", True)
        all_alike_index = user.properties.get("all_alike_index", -1)
        user.properties.__setitem__("all_alike_index", (
            all_alike_index + 1) % len(MAZE_ALL_ALIKES))


def maze_increment_format(rel: Relationship, next_entity: Optional[Entity]) -> None:
    user = rel.to_entity
    if user is None or user is not get_actor():
        return

    if next_entity is not None and next_entity.id.startswith("maze"):
        format_index = user.properties.get("format_index", -1)
        index = (format_index + 1) % len(MAZE_FORMATS)
        user.properties.__setitem__("format_index", index)


entity(
    id="maze_mixin",
    description=description(
        quick="Maze",
        detail=maze_description,
    ),
    relationship_handlers={
        "contents": relate_handlers(on_relate=maze_increment_all_alike,
                                    on_unrelate=maze_increment_format),
    },
    event_handlers={
        EVENT_ACTION_INTERRUPT: _maze_action_interrupt,
    },
)

location(
    id="maze_0",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_1",
        "e": "troll_room",
        "w": "maze_0",
        "s": "maze_0",
        "ne": "maze_2",
    },
)


location(
    id="maze_1",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_1",
        "e": "maze_2",
        "w": "maze_0",
        "s": "maze_2",
    },
    properties={
        "twisty": "twisting",
    }
)


location(
    id="maze_2",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_2",
        "e": "dead_end_0",
        "w": "maze_3",
        "s": "maze_3",
        "sw": "maze_0",
    },
    properties={
        "twisty": "turning",
    }
)


location(
    id="maze_3",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_3",
        "e": "maze_1",
        "w": "maze_3",
        "s": "maze_4",
    },
    properties={
        "passages": "passageways",
    }
)


location(
    id="dead_end_0",
    description=description(
        quick="Dead End",
        detail="""
            You have come to a dead end in the maze.
            """,
    ),
    exits={
        "w": "maze_2",
    },
)


location(
    id="maze_4",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_3",
        "e": "maze_10",
        "w": "dead_end_1",
        "u": "maze_5",
    },
    properties={
        "maze": "tangle",
        "twisty": "turning",
        "little": "tiny",
        "passages": "tunnels",
    }
)


location(
    id="dead_end_1",
    description=description(
        quick="Dead End",
        detail="""
            You have come to a dead end in the maze.
            """,
    ),
    exits={
        "e": "maze_4",
    },
)


location(
    id="maze_5",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_8",
        "d": "maze_4",
        "nw": "maze_8",
        "se": "maze_6",
    },
    properties={
        "little": "teensy",
        "passages": "tunnels",
    }
)


location(
    id="maze_6",
    mixins=["maze_mixin"],
    exits={
        "nw": "maze_8",
        "sw": "maze_5",
        "ne": "maze_7",
        "s": "dead_end_3",
    },
    properties={
        "little": "teeny",
        "passages": "tunnels",
    }
)


location(
    id="maze_7",
    mixins=["maze_mixin"],
    exits={
        "nw": "maze_8",
        "se": "maze_6",
        "s": "maze_6",
    },
    properties={
        "little": "itty-bitty",
        "passages": "tunnels",
    }
)


location(
    id="maze_8",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_9",
        "ne": "maze_7",
        "se": "maze_6",
        "sw": "maze_5",
    },
    properties={
        "little": "teeny-weeny",
        "passages": "tunnels",
    }
)


location(
    id="maze_9",
    mixins=["maze_mixin"],
    exits={
        "s": "maze_7",
        "e": "dead_end_2",
    },
    properties={
        "little": "wee",
        "passages": "tunnels",
    }
)


def skeleton_short(e: Entity) -> Spanish:
    return s("A ", ref(e, "weathered skeleton"), " lies here.")


feature(
    id="maze_skeleton",
    description=description(
        situ=skeleton_short,
        detail="""
            The ancient skeleton of a luckless adventure leans up against the wall.
            """,
    ),
    mixins=["container"],
    aliases=[
        "skeleton",
        "bones",
    ],
    adjectives=[
        "weathered",
        "ancient",
    ],
    relationships={
        "contents": [
            "maze_dagger",
        ],
    },
    visible=True,
)


item(
    id="maze_dagger",
    description=description(
        detail="""
            This dagger is surprisingly well-kept, considering the context in which
            you found it. It must be made of an advanced metal to keep its shape and
            sharpness over the years.
            """,
    ),
    aliases=["dagger"],
    adjectives=["sleek"],
)

location(
    id="dead_end_2",
    description=description(
        quick="Dead End",
        detail="""
            You have come to a dead end in the maze.
            """,
    ),
    exits={
        "w": "maze_9",
    },
    relationships={
        "contents": [
            "maze_skeleton",
        ],
    },
)


location(
    id="maze_10",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_10",
        "e": "maze_10",
        "w": "maze_4",
        "s": "maze_10",
        "nw": "maze_13",
        "ne": "maze_11",
        "sw": "maze_12",
    },
    properties={
        "little": "tiny",
        "passages": "passageways",
    }
)


location(
    id="maze_11",
    mixins=["maze_mixin"],
    exits={
        "n": "dead_end_3",
        "e": "maze_11",
        "w": "maze_11",
        "s": "maze_11",
        "ne": "maze_10",
    },
    properties={
        "little": "tiny",
        "passages": "paths",
    }
)


location(
    id="maze_12",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_12",
        "e": "maze_12",
        "w": "maze_12",
        "s": "maze_12",
        "sw": "maze_10",
        "se": "maze_13",
    },
    properties={
        "little": "tiny",
        "passages": "pathways",
    }
)


location(
    id="maze_13",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_13",
        "e": "maze_13",
        "w": "maze_13",
        "s": "maze_13",
        "ne": "maze_14",
        "nw": "maze_10",
        "se": "maze_12",
        "sw": "maze_14",
    },
    properties={
        "little": "tiny",
        "passages": "crawlways",
    }
)


location(
    id="maze_14",
    mixins=["maze_mixin"],
    exits={
        "n": "maze_14",
        "e": "maze_14",
        "w": "maze_14",
        "s": "maze_14",
        "ne": "maze_13",
        "se": "cyclops_room",
        "sw": "maze_13",
    },
    properties={
        "maze": "labyrinth",
        "twisty": "byzantine",
        "little": "infinitesimal",
        "passages": "passageways",
    }
)


location(
    id="dead_end_3",
    description=description(
        quick="Dead End",
        detail="""
            You have come to a dead end in the maze.
            """,
    ),
    exits={
        "s": "maze_11",
    },
)


location(
    id="cyclops_room",
    description=description(
        quick="Bloody Room",
        detail="""
            This room has an exit on the northwest, and a staircase leading up. From
            the blackened bloodstains on the walls, you gather that unspeakable
            horrors occurred here, long ago.
            """,
    ),
    exits={
        "nw": "maze_14",
        "u": "treasure_room",
    },
)


item(
    id="barrow_map",
    description=description(
        detail="""
            The map shows a forest with three clearings. The largest clearing
            contains a house. Three paths leave the large clearing. One of these
            paths, leading southwest, is marked "To Stone Barrow."
            """,
    ),
    aliases=[
        "map",
        "parchment",
    ],
    adjectives=[
        "ancient",
        "antique",
        "old",
    ],
)


location(
    id="treasure_room",
    description=description(
        quick="Treasure Room",
        detail="""
            This is a large room, whose east wall is solid granite. A number of
            discarded bags, which crumble at your touch, are scattered about on the
            floor. There is an exit down a staircase.
            """,
    ),
    exits={
        "d": "cyclops_room",
    },
    relationships={
        "contents": [
            "barrow_map",
        ],
    },
)


location(
    id="void",
    description=description(
        quick="Void",
    ),
    relationships={
        "contents": [
            "trapdoor",
        ],
    },
    exits={}
)
