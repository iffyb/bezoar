import shutil
import subprocess
import sys
from pathlib import Path

_OUT_PATH = Path("out").absolute()
_WORK_ROOT_PATH = _OUT_PATH.joinpath("work")
_RELEASE_PATH = _OUT_PATH.joinpath("release")
_DEBUG_PATH = _OUT_PATH.joinpath("debug")
_TEST_PATH = _OUT_PATH.joinpath("test")
_SETUP_PY_TEMPLATE = """from setuptools import setup, find_packages

packages = find_packages(include=["{module}", "{module}.*"])

setup(name="{name}",
      version="{version}",
      python_requires=">3.9",
      packages=packages,
      include_package_data=True,
      )
"""

_SETUP_CFG_TEMPLATE = """[build]
build_base = {work_dir}

[egg_info]
egg_base = {work_dir}
"""


def _get_wheel_name(name: str, version: str) -> str:
    return "{}-{}-py3-none-any.whl".format(name, version)


def _get_src_wheel_name(name: str, version: str) -> str:
    return "{}-{}-py3-none-any-src.whl".format(name, version)


def _mkdir(path: Path) -> None:
    if path.is_dir():
        return
    print("[MKDIR]", path)
    path.mkdir(parents=True, exist_ok=True)

def _copy(src_file: Path, dest_file: Path) -> None:
    print("[COPY ]", src_file, "->", dest_file)
    shutil.copy(src_file, dest_file)


def _copy_to_dir(src_file: Path, dest_dir: Path) -> None:
    dest_file = dest_dir.joinpath(src_file.name)
    _copy(src_file, dest_file)


def _build_wheel(name: str, module: str, version: str = "0") -> Path:
    wheel_name = _get_wheel_name(name, version)
    src_wheel_name = _get_src_wheel_name(name, version)

    work_dir = _WORK_ROOT_PATH.joinpath(name)
    _mkdir(work_dir)

    print("[WHEEL]", wheel_name, "in", work_dir)

    setup_py_path = Path("setup.py")
    setup_py_contents = _SETUP_PY_TEMPLATE.format(
        module=module, name=name, version=version)
    setup_py_path.write_text(setup_py_contents)

    setup_cfg_path = Path("setup.cfg")
    setup_cfg_contents = _SETUP_CFG_TEMPLATE.format(work_dir=work_dir)
    setup_cfg_path.write_text(setup_cfg_contents)

    subprocess.check_call([sys.executable, "-m", "build",
                           "--wheel", "--outdir", work_dir])
    setup_py_path.unlink()
    setup_cfg_path.unlink()
    wheel_path = work_dir.joinpath(wheel_name)
    src_wheel_path = work_dir.joinpath(src_wheel_name)
    _copy(wheel_path, src_wheel_path)

    print("[COMPL]", wheel_name)
    subprocess.check_call([sys.executable, "-m", "pyc_wheel", wheel_path])
    return wheel_path


bezoar_wheel = _build_wheel("bezoar", "bezoar")
bezoargame_wheel = _build_wheel("bezoargame", "bezoar_sample")
bezoar_test_wheel = _build_wheel("bezoartest", "test")

_mkdir(_RELEASE_PATH)
_copy_to_dir(bezoar_wheel, _RELEASE_PATH)
_copy_to_dir(bezoargame_wheel, _RELEASE_PATH)
_copy(Path("html").joinpath("sample.html"),
      _RELEASE_PATH.joinpath("index.html"))

_mkdir(_TEST_PATH)
_copy_to_dir(bezoar_wheel, _TEST_PATH)
_copy_to_dir(bezoargame_wheel, _TEST_PATH)
_copy_to_dir(bezoar_test_wheel, _TEST_PATH)
_copy(Path("html").joinpath("test.html"),
      _TEST_PATH.joinpath("index.html"))
