@echo off

set VENV_PATH=venv

if exist %VENV_PATH%\ (
    echo "Virtual environment '%VENV_PATH%\' already exists."
) else if exist %VENV_PATH% (
    echo "%VENV_PATH% is a file, removing."
    del %VENV_PATH%
) else (
    echo "Generating virtual environment '%VENV_PATH%'..."
    python -m venv --system-site-packages %VENV_PATH%
)

echo "Activating '%VENV_PATH%'..."
call %VENV_PATH%\Scripts\activate.bat

echo "Installing dependencies..."
pip -q install transcrypt pyc_wheel setuptools build
