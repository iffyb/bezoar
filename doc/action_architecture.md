# Action Architecture

## Background

Each game has to have some way of processing input, otherwise it is just a movie.

### Definitions

* *Action* - An Actor-triggered process that follows some set of rules (to be
  defined).
* *Actor* - Any object that can issue commands, and thus perform Actions.
* *Player* - An Actor that represents the live User.
* *NPC* - Non-Player Character. An AI controlled Actor.

### Simplest Model

In its simplest form, this would be a turn-based model, where every command is
an action that takes the same amount of time.

Every Actor gets exactly one turn, and then the cycle repeats. E.g. the Player
issues one command, all NPCs get to issue one command.

### Incremental Sophistication

In Planetfall, an early entry from Infocom, different actions took different
amounts of time. At least, walking between locations took time proportional to
distance, and the Player had a watch that told the time. There was only one real
NPC, Floyd, and he seemed to only really do one thing for every one thing the
player did. But, there are hunger and sleep effects over time, so the higher
resolution model did come into play.

### MMO Model

Setting aside movement for the moment, let's focus on skills, spells, and
attacks as Actions in the game.

MMOs are real time games, but they have timing constraints for Actions that
factor into gameplay and balance.

Actions in an MMO have roughly these states (whose names I made up):

1. **READY:** Actor is idle, and will start any triggered Action immediately.
2. **ACTIVATING:** Actor is performing the lead-up to this Action taking effect.
   Typically an animation.
3. **TRIGGERING:** Zero duration state where the effect is reconciled with
   ground conditions, and applied.
4. **DEACTIVATING:** Actor is performing the lead-out to this Action after it
   has taken effect, also typically an animation.
5. **COOLING_DOWN:** This Action cannot be performed, but the Actor may be able
   to perform a different Action. Sometimes multiple actions share a cooldown
   state.

### Fighting Game Model

Fighting games, such as ***Street Fighter*** or ***Smash Bros.***, are extremely
timing sensitive — even more so than FPSs. Each Actor has a state machine that
tracks the Actor's state as it moves though many subtle state transitions that
may have different reactions to an input event. Actions in this model live
encoded in a unified state machine for all the Actor's possible Actions. Each
Action itself has substates, representing windows where an Action can be
cancelled, interrupted, or chained to another Action, all tied to the specific
animation frames.

The burden of working with so complex and bespoke a system is almost certainly
too expensive to be worth it for Bezoar. It is not ultimately trying to compete
with Godot or Unity for every kind of game, just a specific family of games.

## Requirements

1. The model must be compatible with a typing only interface.

   **Use Cases:** Even though Bezoar provides (or will provide) methods for
   streamlining most, if not all, typed commands, it should not be an assumed
   requirement to use Bezoar. Some may want to use Bezoar to build a traditional
   experience, and that is a project goal.
2. The model must support simulated and real time operation.
3. The model must support Actions of varying duration, as well as Actions of
   uniform duration.

   **Use Cases:**
   * Some games will want an old-school feel where every action
   takes one turn, and Actors and effects all are measured in turns.
   * Some games will want a detailed simulation where the tradeoff between quick
     and more effective actions is part of the gameplay.
4. Some Actions must be able to be interrupted.

   **Examples:**
   * `wait` being interrupted by almost anything.
   * An `attack` interrupted by a `parry`.
5. Action costs must be able to be modified by properties of the Actor.

   **Examples:**
   * Faster vs slower characters.
   * Persistent effects (e.g. bless, curse, haste) that make Actions faster or
     slower.
6. Actions must have Trigger points that could take place at an arbitrary time.

   **Example:** Swinging a sword takes X activation time before it triggers, and
   Y deactivation time before another command can be issued.
7. Action Triggers must be able to handle scenarios where the situation has
   changed from when the command was issued.

   **Example:** Swing a sword at a monster, but the monster moves before the
   swing connects.
8. A single Action must be able to support multiple trigger points at different
   times.

   **Example:** Burst rifle fires 3 shots in quick succession, should have 3
   different trigger points.
9. In simulated time mode, must be able to combine simulated time with real time
   effects (like delays, animations).

   **Example:** An attack action associated with a swing animation, damage
   readout, and a fade out back to neutrality.

## Proposal

This proposal hews closest to the MMO model, with maybe a little more
flexibility in terms of Actions having multiple ACTIVATION-TRIGGER cycles.

### Operating Modes

1. *Simulated* - A simulated clock, where game time advances when the Player
   takes Actions, and is frozen otherwise. All Infocom games except Border Zone.
   All turn-based games.
2. *Real-time* - A real time clock, where NPCs or other Players can move and
   take action regardless of Player state. Most action games. Sierra AGI
   adventures. Border Zone. MUDs, MMOs.
3. *Hybrid*, aka *Real Time with Pause* - When the user is typing, or possibly
   for other reasons, the game clock is paused. Once the user submits the
   command and hasn't typed anything new, the clock continues in real time.
   Sierra SCI adventures (Quest for Glory 1 & 2), Infinity Engine (Baldur's
   Gate), Darklands.

### Actor-Action Phases

1. **IDLE:** The Actor is not committed to any Action, and a new command
   submission will instantly transition to the ISSUE phase.
2. **ISSUE:** The intent is calculated, like what the direct or indirect objects
   involved are (*player* hit **rat-ant** with ***knife***). This could cause
   the Action to be aborted as invalid, like trying to interact with an object
   that isn't present.

   If not aborted, the activation delay is calculated based on prevailing
   conditions (it may be zero), and the phase transitions to ACTIVATE.

   An aborted Action may go through the DEACTIVATE phase, or it may jump
   instantly back to IDLE.
3. **ACTIVATE:** The Actor is in the process of activating the Action, but it
   has not triggered yet. During this phase, external events may be able
   interrupt or counter this Action. Once the game clock advances by the
   activation delay, the phase transitions to TRIGGER.

   Audio/visual effects (AVFX) may be tied to this phase. In the case of a
   Simulated clock, the AVFX duration is arbitrary. For Real-time, it must be
   faster or equal to the activation duration. For Hybrid, the game clock can be
   paused or slewed to match the AVFX duration.
4. **TRIGGER:** The ground truth is instantaneously assessed against the objects
   selected in the ISSUE phase. Effects are applied. The Action can
   conditionally REISSUE and jump back to the ACTIVATE phase, or it can
   calculate the deactivation delay (it may be zero) and continue forward to the
   DEACTIVATE phase.

   AVFX can be tied to this phase here. For a Simulated clock, the AVFX can be
   arbitrarily long, and the phase transition can be deferred until the end of
   the AVFX. For Real-time, the AVFX can be triggered, but not waited for. For
   Hybrid, it can be waited for, but the game clock would have to be paused
   during this time, which may have desirable or undesirable ramifications.
5. **DEACTIVATE:** The Actor is recovering from the Action, and cannot issue any
   other command until the game clock advances by the deactivation delay. At
   that point, the phase transitions back to IDLE and is waiting for another
   command.

   AVFX constraints are the same as in the ACTIVATE phase.

   Cooldown accounting, if any, can be calculated in TRIGGER and enforced in
   ISSUE.

### Detailed Examples

    > fhqwhgads

Presumably a typo, or otherwise flailing against the keyboard, this would not be
associated with an Action; therefore no command would be ISSUED, and no time
would pass.

    > look

In the standard library, Bezoar considers `look` and `look at` to be free
actions. This means the Actor-Action Phases degenerate into no ACTIVATE phase,
no DEACTIVATE phase, and the ISSUE and TRIGGER phases are essentially the same
thing. In classic Infocom, this was not true, and you could be racing against a
clock while staring dumbly around you.

This would look a lot like the current Actions, that just call a single function
and are done.

    > get lamp

A slightly more complex use case. The Actor must, say, bend over to pick up the
lamp, that would be the activation delay. Then the Actor has the lamp (the
Trigger), but is still bending over. Then the Actor stands up and is ready to do
something else, which is modeled with the deactivation delay. All in all, this
is probably a 5 second action.

    > west

Locations can represent places that are arbitrarily far apart. Going `east`
might take an Actor from the Living Room into the Kitchen, or it might take them
from San Francisco to Yosemite. So, it makes sense that the Action time would
vary based on how far apart the two Locations are supposed to be.

One way to model this might be:

1. ISSUE: Calculate the total delay of the Action, possibly by getting a
   property on the `exit` relationship.
2. ACTIVATE: Have a short activation delay as the Actor leaves the current
   location.
3. TRIGGER: Remove the Actor from the location and put them in a limbo.
4. ACTIVATE 2: A second activation delay represents the bulk of the travel.
5. TRIGGER 2: Add the Actor to the new location, and force a `look`.
6. DEACTIVATE: Similarly short to the activation delay

You can imagine leaving a room or other location that if something happened in
that location while you were leaving, you might see it, and you might stop
(interrupt) to see what it is. The bulk of the travel time would be elided from
the narrative, but we wouldn't tell the user that they were in a Void Limbo
Nowhere during that time. Finally, as you enter a room, you would first get a
sense of where you were entering (via the forced `look`) and then you might see
something that is actively happening in that new location.

    > attack rat-ant (with knife)

This is closely analogous to an simple attack in an MMO or other Action RPG.

1. ISSUE: Calculate the activation delay of the Action, using the knife's speed,
   Actor's skill, Actor's speed, whatever else.
2. ACTIVATE: Wait for the activation delay on the game clock.
3. TRIGGER: Is the rat-ant still present? If not, print a failure message. If
   so, calculate attack result. Apply damage to rat-ant, if applicable. Display
   result information.
4. DEACTIVATE: Wait for the deactivation delay on the game clock.

