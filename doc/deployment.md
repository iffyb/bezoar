# Deployment Ideation

I finished up a maze in [`bezoar_sample`](../bezoar_sample) that I wanted to be
able to share around with folks. The most obvious answer to handing it out,
based on Googling it, is the [`pyinstaller`](https://www.pyinstaller.org/)
package. It's not an official Python facility, it's a community-developed
solution. It worked well enough, but came with some pretty serious limitations.

In the future, I'd like for someone who isn't writing open-source software to be
able to use Bezoar and deploy a real, professional-seeming application on a
variety of platforms. I don't think PyInstaller is going to get me there, but
that's just a gut feeling. To really know, we'll have to enumerate our
requirements, and then look at solutions.

## Requirements

Here's an ordered list of requirements, in descending importance. But, if all of
the MUSTs are not satisfied, we frankly don't have a Minimum Viable Product.

1. MUST be deployable on:
   1. Windows
   1. iOS
   1. macOS
   1. Android
1. MUST appear to be a normal, legitimate application from the perspective of
   the user, as appropriate for whatever platform it is being deployed on
1. MUST be "push-button" accessible to other Bezoar developers\
   *(if they ever exist)*
1. MUST be fully reusable between projects
1. MUST (eventually) be able to display graphics and utilize UI controls on all
   platforms
1. SHOULD be deployable on:
   1. Linux
1. SHOULD be cross-buildable and packageable from Linux\
   *(this may be near-impossible for iOS, and difficult for macOS and Windows)*

## Goals

Goals are things to keep in consideration, but don't have a hard pass/fail
success criteria.

1. **Minimize Binary Size**\
   While the internet is big, and users tolerate a lot, this is fundamentally a
   text adventure engine, and shouldn't require a Type I Kardashev civilization
   to run.
1. **Run Fast**\
   Similar to the binary size goal, above, I want developers to be able to do
   complex and reactive things without quickly hitting performance limits. Yes,
   even real-time things.

## Constraints

* [**iOS**](https://developer.apple.com/app-store/review/guidelines/#third-party-software)
  > Apps may contain or run code that is not embedded in the binary (e.g.
  HTML5-based games, bots, etc.), as long as code distribution isn’t the main
  purpose of the app, the code is not offered in a store or store-like
  interface, and provided that the software (1) is free or purchased using
  in-app purchase; (2) only uses capabilities available in a standard WebKit
  view (e.g. it must open and run natively in Safari without modifications or
  additional software); your app must use WebKit and JavaScript Core to run
  third-party software and should not attempt to extend or expose native
  platform APIs to third-party software; (3) is offered by developers that have
  joined the Apple Developer Program and signed the Apple Developer Program
  License Agreement; (4) does not provide access to real money gaming,
  lotteries, or charitable donations; (5) adheres to the terms of these App
  Review Guidelines (e.g. does not include objectionable content); and (6) does
  not offer digital goods or services for sale. Upon request, you must provide
  an index of software and metadata available in your app. It must include Apple
  Developer Program Team IDs for the providers of the software along with a URL
  which App Review can use to confirm that the software complies with the
  requirements above.

  My NON-PROFESSIONAL interpretation of this:

  * I can have an interpreter, but the code that is interpreted must be
    bundled with the app.
  * I can download interpreted code, but it must use a Safari WebKit View.
  * I can't bypass the App Store in any way (seems fair).
  * I could download packages for free as long as there's no cost to the
    end-user (i.e. I can't get around the app store), and there's no "store-like
    interface," which is pretty broad and vague. I think the idea is that I
    could download script updates, but not provide a list of sub-apps to
    acquire.

## Options

* [**PyInstaller**](https://www.pyinstaller.org/) - *Package*
  * Can obfuscate source with AES, but easily reversed, as key is in binary, and
    source code for how it is done is available
  * Can compress source
  * Does not cross-package, requires a host system for every target platform
    (including 32 vs 64 bit architectures)
  * Is detected as a trojan by many antivirus programs, including those employed
    by Google Drive
  * Includes entire CPython runtime (big: 7-16 MB)
  * Slow startup time
  * Slow performance with just interpreted Python
  * No solution for mobile?
* [**Transcrypt**](https://www.transcrypt.org/) - *Build*
  * Transpiles to ES6
  * Can obfuscate and type-check
  * Doesn't officially support Python 3.9 yet, but may "Just Work"
  * Could deploy as browser only (PWA SPA) solution
  * Could use V8 in a native container everywhere but iOS
  * Does not support full standard library (may not be an issue)
  * Does not include a packaging solution, but changes the problem to a
    JS-packaging solution, which is somewhat easier.
* [**Web Assembly**](https://webassembly.org) - *Build*
  * No OOB Python to raw WASM solution
  * No built-in GC support in WASM, meaning I would have to provide one that
    would get compiled into WASM.
  * [Iodide](https://alpha.iodide.io/) builds the Python runtime for WASM, but
    not the Python code itself.
  * [RustPython](https://github.com/RustPython/RustPython) is a Python
    re-implementation in Rust, which can compile to WASM. Still interprets
    Python at runtime.
  * Does not include a packaging solution.
* [**IronPython**](https://ironpython.net/) - *Build*
  * Seems to be well behind Python (3.4), but actively developed
  * [Xamarin](https://visualstudio.microsoft.com/xamarin/) for mobile?
* [**Electron**](https://www.electronjs.org/) - *Package*
  * Would require either WASM or Transcrypt.
  * No OOB mobile support, but Apache Cordova, Ionic Framework, PhoneGap, or
    others may provide similar support for mobile.
* [**Unity**](https://unity.com/) - *Package*
  * Unity seems largely based on Mono/.NET/C#. So, seems like IronPython could
    generate CLR DLLs that Unity then deploys.
  * Would require IronPython or similar.
* [**Godot**](https://https://godotengine.org/) - *Package*
  * Similar to Unity, there is a custom script language, but also supports
    Mono/C#.
  * Would require IronPython or similar.
* [**Cython**](https://cython.org/) - *Build*
  * Compiles Python to C, using static typing for optimizations.
  * Probably the best performing option.
  * May be more finnicky and difficult for Bezoar game developers.
  * Embedded Cython is not the typical use case, but there is a [small
    example](https://github.com/cython/cython/wiki/EmbeddingCython).
  * Includes the Python runtime in the form of libpython, but portions may
    be able to be culled at link time
* **Custom**
  * Can possibly compile to WASM using typed-ast Python package
    * This is essentially what Transcrypt does, but to ES6
  * can optimize string dict lookups to integer indexes
    * If doing any build step, could also do this anyway
  * would have to provide standard library somehow
  * would be fun, but a ***lot*** of work

## Conclusions

I think it's a toss-up between **Cython + custom glue** and **Transcrypt +
Electron**.

ES6 is well-optimized and distributable on all key platforms, but the mobile
distribution problem will require some more work. If Cordova "Just Works"
everywhere, we might be in business. This will still leave some (significant)
performance on the table, for when we want to include more real-time and
interactive facilties driven by Python.

Cython will provide the absolute best performance, but performance isn't
currently the top concern. It also avoids iOS deployment restrictions by being
fully compiled ahead of time. It does include the Python runtime, which may
set off various virus, trojan, app store alarms.
