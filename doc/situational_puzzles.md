# Situational Puzzles: Conventions and Classifications

A situational puzzle is one where the puzzle isn't explicit, like a rebus,
riddle, or cryptogram, but instead is implied by the context. A locked door is a
normal thing to exist in the world. But, it implies a situational puzzle: How
can I get the door open?

Situational puzzles are most often employed in Adventure Games, and most
effectively in Text-Entry Adventure Games. Text Adventures are one of the purest
expressions of situational puzzles because the interface does not require
exposing a menu of choices. So the player must conceive of how to solve the
puzzle given the full range of the language.

Adventures where the input is not text entry (e.g. point-and-click) are forced
to prompt the user, either with a list of verbs, or by just using a single vague
one, like "use." This generally means there needs to be an item that represents
a specific action that solves a puzzle, and it gives the whole thing away.
Players can just try every item on every problem and farm into a solution.

## Puzzle Complexity

Here is a proposed classification of puzzle complexity. More complex puzzles
aren't necessarily harder, but they would tend in that direction.

### Complexity Zero - Exploration & Revelation

The simplest "puzzles" are still valuable and interesting. They may even make up
the bulk of a game. This represents just baseline interactivity: moving around,
looking at objects and places in detail, moving things in the environment to
reveal more, etc...

The first pass of an Adventure Game is to explore the environment; find the
boundaries; identify the top-level puzzles. This includes finding things that
are hidden.

#### C0 Examples

- Walking between rooms
- Opening containers, closets, cupboards, etc...
- Looking in pockets
- Looking under and behind beds, tables, chairs, etc...
- Looking inside drawers, closets, cabinets, etc...
- Looking for caves behind waterfalls (there is always a cave)
- Searching clear pools
- Looking under the rug to find a trap door (Zork)

### Complexity One - Traditional Applications

There are many puzzles where you just need to use things the way they were
obviously intended.

#### C1 Examples

- Give honey/fish to a bear
- Give bone/meat to dog
- Unlock a door with a key
- Put a gem in a gem-shaped eye socket
- Pulling a lever to open a door
- Cast Telekenesis on an unreachable item
- Pry open a rusted grate with a crowbar
- Bringing light to a dark area

### Complexity Two - Non-traditional Applications

Some puzzles put the player in a situation of limited means. Here they have to
get creative, and find a way to use what they have to solve the problem.

#### C2 Examples

- Use a butterknife to unscrew a screw (Murder Mystery House)
- Use an umbrella as a parachute (Beyond Zork)
- Look through a red wine bottle to read text on an amulet with red swirls
  (Beyond Zork)
- Smash a wall with a hammer weapon (Lands of Lore)
- Cast Freeze on a slime pit to walk over it (Lands of Lore)
- Take a lantern used as a sign and use it as a real lantern (Beyond Zork)
- Take a shovel used as a sign and use it as a real shovel (Monkey Island)
- Use psychic power to see through Agent's eyes as he enters a passcode
  (Psychonauts)

### Complexity Three - Composition

Composite puzzles involve multiple solutions to work together to solve a greater
puzzle. Ideally, the player doesn't just stumble into solutions, but is able to
proactively decompose the problem, find solutions to the smaller problems, and
then put them together to achieve the original goal.

#### C3 Examples

- Pick up rug from the tavern, carry it to the tower, Walk across it to generate
  static electricity, then touch dust bunny (Beyond Zork)
- Gnomon screw and sundial hole threads are reversed, go through Klein bottle to
  make world (or screw) reversed, then screw in. (Trinity)
- The long series of puzzles to solve to get the Babel Fish (H2G2)

### Other puzzles to consider

- gather pieces of a password
- break a code
  - anagram
  - substitution cypher
  - multiple layered cyphers‽
- manipulate emotions of an NPC that has something you want
  - figure out what they want and get it for them
- use tools
  - conventional
  - improvised
- find things
  - in dark corners
  - beneath, behind beds, tables, chairs, rugs, etc...
  - hidden in plain sight
- clues use uncommon or archaic language forms (a la crosswords)
- duplicate an object (e.g. key) from a mold

## Design Principles

These are derived from Graham Nelson's _Bill of Player's Rights_ below.

1. Death is Serious
   1. No walking death
   2. No sudden death
   3. Nothing that can only be learned by playing and restarting
2. Sensible Puzzles
   1. Clear clues
   2. No random solutions
   3. Understandable once solved
   4. Not too many red herrings
   5. Understandable red herrings
3. Keep it Interesting
   1. Limit excessive trekking
   2. Avoid big empty spaces, unless you aren't really supposed to go there
   3. Limit simple fetch quests
   4. No "kill 10" quests
4. Freedom of Action
   1. Features should be interactive if they look interactive
   2. Systems should function even if not needed to beat game (e.g. bread baking
      in Ultima VII)
   3. Space should be big enough to provide a sense of exploration
5. Sense of Progression

## Appendix: Graham Nelson's Bill of Player's Rights

1. Not to be killed without warning. At its most basic level, this means that a
   room with three exits, two of which lead to instant death and the third to
   treasure, is unreasonable without some hint. Mention of which brings us to:

2. Not to be given horribly unclear hints. Many years ago, I played a game in
   which going north from a cave led to lethal pit. The hint was: there was a
   pride of lions carved above the doorway. Good hints can be skilfully hidden,
   or very brief (I think, for example, the hint in the moving-rocks plain
   problem in "Spellbreaker" is a masterpiece) but should not need explaining
   even after the event. A more sophisticated version of (1) leads us to:

3. To be able to win without experience of past lives. Suppose, for instance,
   there is a nuclear bomb buried under some anonymous floor somewhere, which
   must be disarmed. It is unreasonable to expect a player to dig up this floor
   purely because in previous games, the bomb blew up there. To take a more
   concrete example, in "The Lurking Horror" there is something which needs
   cooking for the right length of time. As far as I can tell, the only way to
   find out the right time is by trial and error. But you only get one trial per
   game. In principle a good player should be able to play the entire game out
   without doing anything illogical. In similar vein:

4. To be able to win without knowledge of future events. For example, the game
   opens near a shop. You have one coin and can buy a lamp, a magic carpet or a
   periscope. Five minutes later you are transported away without warning to a
   submarine, whereupon you need a periscope. If you bought the carpet, bad
   luck.

5. Not to have the game closed off without warning. Closed off meaning that it
   would become impossible to proceed at some later date. If there is a
   papier-mache wall which you can walk through at the very beginning of the
   game, it is extremely annoying to find that a puzzle at the very end requires
   it to still be intact, because every one of your saved games will be useless.
   Similarly it is quite common to have a room which can only be visited once
   per game. If there are two different things to be accomplished there, this
   should be hinted at.

6. Not to need to do unlikely things. For example, a game which depends on
   asking a policeman about something he could not reasonably know about. (Less
   extremely, the problem of the hacker's keys in "The Lurking Horror".) Another
   unlikely thing is waiting in uninteresting places. If you have a junction
   such that after five turns an elf turns up and gives you a magic ring, a
   player may well never spend five turns there and never solve what you
   intended to be straightforward. On the other hand, if you were to put
   something which demanded investigation in the junction, it might be fair
   enough. ("Zork III" is especially poor in this respect.)

7. Not to need to do boring things for the sake of it. In the bad old days many
   games would make life difficult by putting objects needed to solve a problem
   miles away from where the problem was, despite all logic - say, putting a
   boat in the middle of a desert. Or, for example, it might be fun to have a
   four-discs tower of Hanoi puzzle in a game. But not an eight-discs one.

8. Not to have to type exactly the right verb. For instance, looking inside a
   box finds nothing, but searching it does.

9. To be allowed reasonable synonyms. In the same room in "Sorcerer" is a "woven
   wall hanging" which can instead be called "tapestry" (though not "curtain").
   This is not a luxury, it's an essential.

10. To have a decent parser. This goes without saying. At the very least it
    should provide for taking and dropping multiple objects.

11. To have reasonable freedom of action. Being locked up in a long sequence of
    prisons, with only brief escapes between them, is not all that entertaining.
    After a while the player begins to feel that the designer has tied him to a
    chair in order to shout the plot at him.

12. Not to depend much on luck. Small chance variations add to the fun, but only
    small ones. The thief in "Zork I" seems to me to be just about right in this
    respect, and similarly the spinning room in "Zork II". But a ten-ton weight
    which fell down and killed you at a certain point in half of all games is
    just annoying.

13. To be able to understand a problem once it is solved. This may sound odd,
    but many problems are solved by accident or trial and error. A guard-post
    which can be passed only if you are carrying a spear, for instance, ought to
    have some indication that this is why you're allowed past. (The most extreme
    example must be the notorious Bank of Zork.)

14. Not to be given too many red herrings. A few red herrings make a game more
    interesting. A very nice feature of "Zork I", "II" and "III" is that they
    each contain red herrings explained in the others (in one case, explained in
    "Sorcerer"). But difficult puzzles tend to be solved last, and the main
    technique players use is to look at their maps and see what's left that they
    don't understand. This is frustrated when there are many insoluble puzzles
    and useless objects. So you can expect players to lose interest if you
    aren't careful. My personal view is that red herrings ought to have some
    clue provided (even only much later): for instance, if there is a useless
    coconut near the beginning, then perhaps much later an absent-minded
    botanist could be found who wandered about dropping them. The coconut should
    at least have some rationale. The very worst game I've played for red
    herrings is "Sorcerer", which by my reckoning has 10.

15. To have a good reason why something is impossible. Unless it's also funny, a
    very contrived reason why something is impossible just irritates. (The
    reason one can't walk on the grass in "Trinity" is only just funny enough, I
    think.)

16. Not to need to be American to understand hints. The diamond maze in "Zork
    II" being a case in point. Similarly, it's polite to allow the player to
    type English or American spellings or idiom. For instance "Trinity" endears
    itself to English players in that the soccer ball can be called "football" -
    soccer is a word almost never used in England.

17. To know how the game is getting on. In other words, when the end is
    approaching, or how the plot is developing. Once upon a time, score was the
    only measure of this, but hopefully not any more.
