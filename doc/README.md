# Bezoar Documentation

This is really going to be more of a development blog, but organized by topic rather than by date.

## Index

- [Building Blocks of the Bezoar Model](building_blocks.md)
- [Parser Background and Requirements](parser.md)
- [Situational Puzzles: Conventions and Classifications](situational_puzzles.md)
- [Deployment Ideation](deployment.md)
