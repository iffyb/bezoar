# Building Blocks of the Bezoar Model

Infocom had a brilliant idea -- build an engine with coarse primitives that
could express a wide variety of concepts. Those primitives can then be optimized
for each platform, while providing as much control as possible to the portable
game development language.  Bezoar keeps this paradigm, but chooses to draw the
line in a somewhat different way.

## Important Context

Bezoar is split up into four major components:

1. The Model
2. The Engine (including the Parser)
3. The Standard Library
4. The Game

### The Model

The Model is definitionally these exact building blocks being described here. As
with Infocom's system, the Model is not customizable between games. Even more so
than Infocom's system, the goal is that the Model should not **need** to be
changed to achieve any particular game feature.

### The Engine

The Engine is really everything else that makes the application go. The Model
and the Parser need to interact intimately, so these two components are tied
closely together. Again, the goal is that the Engine should not need to be
changed to implement any particular game feature.

In a hypothetical future, both the Engine and Model would likely be implemented
natively for each platform, so all these primitive building blocks would have
maximal efficiency. This is probably not necessary for a typical text adventure,
but Bezoar has aspirations to support Text Adventures Improved, with graphics
and mobile-friendly controls. (When Infocom expanded their Z-Machine engine to
support graphics, it was pretty clunky...)

### The Standard Library

The Standard Library would be a bunch of definitions that could be used by any
Game that wanted such mechanics. Each Game could decide to include or not
include portions of the Standard Library, even to the full extremes of including
all or absolutely none of it.

The goal of the Standard Library would be to simplify constructs that are
endemic to Text Adventures, but not so fundamental that they need to be built
into the Model.

### The Game

The Game is what you expect, it's a specific game using Bezoar as a framework.
When adding a feature, the thought experiment should be to think about how
specific it is. Is it very specific to the game? Is it actual game content? Then
it should be in the Game. Is it a generic facility that other Games might use?
Perhaps it should be in the Standard Library. Does it *require* some
modification to either the Model or Engine to work? Then it **must** go in the
Model or Engine, as appropriate.

## Static Data vs Runtime

The vast majority of the data in a Bezoar model is completely static. Or, if
dynamic, is dynamic in a static way. So, Bezoar chooses to separate its model
into an immutable Static layer and dynamic Runtime layer. The Runtime layer only
shadows the Static, and applies deltas to the Static Data to produce the current
runtime state of the simulation. Incidentally, these deltas are all that need to
be persisted when saving a game.

The convention chosen here is that the Static layer classes all have the `Data`
suffix (e.g. `EntityData`). We don't spend the effort to make these classes
actually immutable, but they are immutable by convention once the runtime has
started.

Each runtime object refers back to a static `Data` object of the corresponding
type that defines all of its initial values, as well as provides functions that
implement runtime behavior. The runtime objects only interact directly with
their own static `Data` class, and otherwise interact purely with the runtime
layer.

## Entities

The most fundamental building block in Bezoar is an `Entity`. It represents a
game runtime object of any kind: item, location, player, concept. This is not
really any different from an object in ZIL. An `Entity` has some universal
properties, the things that every object in the game would need.

The core properties of an `Entity` are:

* unique id
* nouns
* adjectives
* event handlers

Some ubiquitous `Entity` properties:

* short description, when in a list, or the title of a room
* long description, when looked at, or entered as a room
* initial short description, when still in its initial position, if any
* whether it should be listed in lists
* size, if any
* weight, if any

These properties are so universal, they are represented "natively" within the
`Entity` implementation. Any other properties can be added to a generic
`property` bag attached to each `Entity`.

## Relationships

Where Bezoar starts to diverge from Infocom's model is how objects relate to
each other. Because text adventures are so naturally expressive, the primitives
should, as much as possible, avoid limiting the expressiveness of the model.
They should, in fact, support expressiveness in ways that haven't yet been
considered.

Bezoar first implemented things like rooms and containment as special cases. And
there is a real value for syntactic sugar for often-used constructs. But,
objects may have a variety of relationships to each other, and the model
supports creating new and different kinds of relationships.

If you have a desk, you can put a note *on top* of the desk, *inside* the desk
drawer, or *underneath* the desk. You don't want have to code this behavior into
every desk, bed, chair, or similar, so maybe you just avoid the matter by never
putting things under other things.

A door should be in multiple rooms at the same time. Either you keep two objects
in sync, or you do some other thwarting of the model to achieve the effect. Or,
you avoid the matter by never having doors.

A `Relationship` is meant to be a better way. The Bezoar model supports defining
different relationships, and the mechanics therein. Instead of an object having
a hard-coded `contents` property and a `container` property, the Library can
instead define a `contents-container` `Relationship`. Instead of moving an
object, you break the existing `contents-container` `Relationship` of the
content object, and form a new `contents-container` `Relationship` with the new
container.

This sounds like more work. It is. But, this can be pasted over with
syntactic candy, while providing flexibility for a richer simulation that is
more generically coded.

### Perspective: Like a State Machine

A `Relationship` in this case is like a state in a state machine. It likely has
one or more invariants associated with it. The handlers are called in a formal
sequence so that they can maintain any invariants the `Relationship` is meant to
represent.

For example, perhaps the `contents-container` `Relationship` maintains a
subservient `Relationship` of `visible-to-actor`. Then, any time the game wanted
to get the list of `Entity` instances visible to the player, they could just
get all the `visible-to-actor` `Relationship` instances for the player `Entity`.

### Case Study: Moving an Entity

We want to be sure Relationships can support at least the same functionality as
the original contents-container/moving case, so let's examine that without the
Relationship lens.

When moving an object from a source container to a destination container, the
model would go through something like this sequence:

1. Ask object if it `can_move` to the destination container.
2. Ask source container if the object `can_exit` to the destination.
3. Ask destination if the object `can_enter` from the source.
4. -- **The point of no return:** -- Move the object.
5. Tell the source the object has left -- `on_exit`.
6. Tell the destination the object has entered -- `on_enter`.
7. Tell the object it has been moved -- `on_move`.

Many of these would often be no-ops, but it is important to have each of the
objects able to impose an invariant on the position of an `Entity`.

Going back to thinking about `Relationship`s, we want to them to preserve this
mechanism of an atomic exchange of one container for another. This way, the
object and containers can express opinions not only about what is leaving, but
where it is coming from, and going to. Also, we don't want to leave an `Entity`
in an partial state, so we have to first verify that the action will completely
succeed, and only then mutate anything.

### Relationship Sequence

#### Form

This is the sequence for when a `Relationship` is formed between two `Entity`
objects, but not as part of an atomic exchange.

1. Ask object if it `can_relate` to the destination.
2. Ask destination if `can_relate` to the object.
3. -- **The point of no return:** -- Create the `Relationship` between object
   and destination, also running the `Relationship`'s `on_form` handler.
4. Run the object `on_form` handler.
5. Run the destination `on_form` handler.

#### Break

This is the sequence for when an established `Relationship` is broken between
two `Entity` objects, but not as part of an atomic exchange.

1. Ask object if it `can_unrelate` to the source.
2. Ask source if `can_unrelate` to the object.
3. -- **The point of no return:** -- Break the `Relationship` between object and
   source, also running the `Relationship`'s `on_break` handler.
4. Run the object `on_break` handler.
5. Run the source `on_break` handler.

#### Swap

This is the sequence for an atomic exchange of a `Relationship` between an
`Entity` object from a source `Entity` to a destination `Entity`.

1. Ask object if it `can_unrelate` from the source to the destination.
2. Ask source if `can_unrelate` from the object to the destination.
3. Ask object if it `can_relate` to the destination from the source.
4. Ask destination if `can_relate` to the object from the source.
5. -- **The point of no return:** -- Break the `Relationship` between object and
   source, also running the `Relationship`'s `on_break` handler.
6. Create the `Relationship` between object and destination, also running the
   `Relationship`'s `on_form` handler.
7. Run the object `on_break` handler.
8. Run the source `on_break` handler.
9. Run the object `on_form` handler.
10. Run the destination `on_form` handler.

### Potential Use Cases

* content-container
* under-over
* on top-supporting
* next to (symmetric)
* tied to (symmetric)
* wearing-worn
* wielding-wielded
* engaged in combat with (symmetric)

### Open Questions

* Should `Relationships` have a defined `Arity`?
  * 1 - 1
  * 1 - many
  * many - 1
  * many - many
* How to represent the directionality of a `Relationship` from the perspective
  of one of the `Entity` objects involved?
  * **For example:** An `Entity` instance can be a `content` of one `Entity` but
    the `container` for another `Entity`. When the handlers get called,
    shouldn't it be clearly indicated which side the `Entity` that defined the
    handler is on?
  * **Corollary:** Should there be a `Relationship.reverse()` method that
    returns a view that represents the same `Relationship` from the reverse
    perspective?
