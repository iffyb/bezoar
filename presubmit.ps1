$ErrorActionPreference = "Stop"
& .\env.ps1
python -m mypy -p test -p bezoar_sample -p bezoar

if ($LastExitCode -ne 0) {
    throw $LastExitCode
}

python -m test

if ($LastExitCode -ne 0) {
    throw $LastExitCode
}
