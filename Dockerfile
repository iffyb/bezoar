FROM python:3-buster

COPY . /bezoar

WORKDIR /bezoar

RUN python -m pip install -r requirements.txt

RUN python -m buildtools.make_wheel

CMD ["python3", "-m", "bezoar_sample"]
